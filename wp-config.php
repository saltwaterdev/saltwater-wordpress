<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

if (strpos($_SERVER['SERVER_NAME'],'stage') !== false) {
	define('DB_NAME', 'sw_wp_stage');
	define('DB_USER', 'swstage_usr');
	define('DB_PASSWORD', 'kT_s359b');
} else if (strpos($_SERVER['SERVER_NAME'],'site') !== false) {
	define('DB_NAME', 'sw_wp');
	define('DB_USER', 'root');
	define('DB_PASSWORD', 'root');
} else {
	define('DB_NAME', 'sw_wp');
	define('DB_USER', 'sw_wp_usr');
	define('DB_PASSWORD', 'kT_s359b');
}

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

if (strpos($_SERVER['SERVER_NAME'],'stage') !== false) {
	define('WP_HOME','https://stage.saltwaterco.com');
	define('WP_SITEURL','https://stage.saltwaterco.com');
} else if (strpos($_SERVER['SERVER_NAME'],'site') !== false) {
	define('WP_HOME','http://saltwaterco.site:8888');
	define('WP_SITEURL','http://saltwaterco.site:8888');
} else {
	define('WP_HOME','https://www.saltwaterco.com');
	define('WP_SITEURL','https://www.saltwaterco.com');

}
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'J0g.+=9k9X)2zO4{8pq%GG;2l3{*bO _L9?m#:I&we2_#(${qj{>Z>npOU%@n3x7');
define('SECURE_AUTH_KEY',  'prse1# AhqJ4KpLq`A.O@f(.UUYO[/QA9Qpd8ss;DtbBDGOIoNF61{ [GRTorr[j');
define('LOGGED_IN_KEY',    'Z<tD&+Q:CM,`W S-zY#9~tauC9PBvhCo@rxoKQNIh&d54Jf<inIG7f}gb;amm)$e');
define('NONCE_KEY',        'x^#Q{b_9I3-NcLy2)#{.#YTg/3kXLaCIW,_]J``0$^/MqU =h.5XiPo(PHNMGrEs');
define('AUTH_SALT',        '#HS Y9XAY<[EBK>VlDPMu=f{F*OnP-cw61%?f$MAB<,8NIAp5<$B%A5ssd=,0zR{');
define('SECURE_AUTH_SALT', 'mrM3Jz+avJ-uZNsnVnV]ureDE$g)CsssusZr+P4C&mkF5ZUFJJ.v`%pCO_U]&v-f');
define('LOGGED_IN_SALT',   'nEtd9j,eK$jzqqE,vY423~aCZB8#4[kD#sB,sxm_)1/l{F!zJ&ij(Q=t{Oz^<J{L');
define('NONCE_SALT',       '>Q_Z^&*J~dV.=/&:8YE56@7vLV>EADWZH}* EK6>yF@*1364Lk[M=#Pg5eC?Bhq!');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
if (strpos($_SERVER['SERVER_NAME'],'stage') !== false) {
	define('WP_DEBUG', true);
} else if (strpos($_SERVER['SERVER_NAME'],'site') !== false) {
	define('WP_DEBUG', true);
} else {
	define('WP_DEBUG', false); 
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
