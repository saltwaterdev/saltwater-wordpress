<?php
	
	function sw_archive_redirect()
	{
	    if ( is_category( 'uncategorized' ) ) {
	        $url = site_url( '/blog' );
	        wp_safe_redirect( $url, 301 );
	        exit();
	    }
	    
	    if ( is_category( 'marketing-strategy' ) ) {
	        $url = site_url( '/blog?category=Marketing%20Strategy' );
	        wp_safe_redirect( $url, 301 );
	        exit();
	    }
	    
	    if ( is_category( 'agency-culture' ) ) {
	        $url = site_url( '/blog?category=Agency%20Culture' );
	        wp_safe_redirect( $url, 301 );
	        exit();
	    }
	    
	    if ( is_category( 'branding' ) ) {
	        $url = site_url( '/blog?category=Branding' );
	        wp_safe_redirect( $url, 301 );
	        exit();
	    }
	    
	    if ( is_category( 'content-marketing' ) ) {
	        $url = site_url( '/blog?category=Content%20Marketing' );
	        wp_safe_redirect( $url, 301 );
	        exit();
	    }
	    
	    if ( is_category( 'marketing-analytics' ) ) {
	        $url = site_url( '/blog?category=Marketing%20Analytics' );
	        wp_safe_redirect( $url, 301 );
	        exit();
	    }
	    
	    if ( is_category( 'marketing-automation' ) ) {
	        $url = site_url( '/blog?category=Marketing%20Automation' );
	        wp_safe_redirect( $url, 301 );
	        exit();
	    }
	    
	    if ( is_category( 'marketing-trends' ) ) {
	        $url = site_url( '/blog?category=Marketing%20Trends' );
	        wp_safe_redirect( $url, 301 );
	        exit();
	    }
	    
	    if ( is_category( 'media-planning-and-buying' ) ) {
	        $url = site_url( '/blog?category=Media%20Planning%20and%20Buying' );
	        wp_safe_redirect( $url, 301 );
	        exit();
	    }
	    
	    if ( is_category( 'ppc' ) ) {
	        $url = site_url( '/blog?category=PPC' );
	        wp_safe_redirect( $url, 301 );
	        exit();
	    }
	    
	    if ( is_category( 'seo' ) ) {
	        $url = site_url( '/blog?category=SEO' );
	        wp_safe_redirect( $url, 301 );
	        exit();
	    }
	    
	    if ( is_category( 'social-media-marketing' ) ) {
	        $url = site_url( '/blog?category=Social%20Media%20Marketing' );
	        wp_safe_redirect( $url, 301 );
	        exit();
	    }
	    
	    if ( is_category( 'video-production' ) ) {
	        $url = site_url( '/blog?category=Video%20Production' );
	        wp_safe_redirect( $url, 301 );
	        exit();
	    }
	    
	    if ( is_category( 'website-design' ) ) {
	        $url = site_url( '/blog?category=Website%20Design' );
	        wp_safe_redirect( $url, 301 );
	        exit();
	    }
	}
	add_action( 'template_redirect', 'sw_archive_redirect' );
	
	// wp menus
	add_theme_support( 'menus' );
	add_theme_support( 'post-thumbnails' );
	
	register_nav_menus(
		array(
			'main_nav' => __('Main Nav','saltwater'),
			'footer_nav' => __('Footer Nav','saltwater'),
		)	
	);
	
	add_theme_support( 'custom-logo' );
	
	function cc_mime_types($mimes) {
	 $mimes['svg'] = 'image/svg+xml';
	 return $mimes;
	}
	add_filter('upload_mimes', 'cc_mime_types');
	
	function prefix_nav_description( $item_output, $item, $depth, $args ) {
	    if ( !empty( $item->description ) ) {
	        $item_output = str_replace( $args->link_after . '</a>', '<span class="menu-item-description">' . $item->description . '</span>' . $args->link_after . '</a>', $item_output );
	    }
	 
	    return $item_output;
	}
	add_filter( 'walker_nav_menu_start_el', 'prefix_nav_description', 10, 4 );
	
	function saltwater_widget() {
		
		register_sidebar( array(
			'name'          => 'Navigation Social Links',
			'id'            => 'nav_social',
			'before_widget' => '<div>',
			'after_widget'  => '</div>',
		) );
		
	}
	add_action( 'widgets_init', 'saltwater_widget' );
	
	function wpb_mce_buttons_2($buttons) {
	    array_unshift($buttons, 'styleselect');
	    return $buttons;
	}
	add_filter('mce_buttons_2', 'wpb_mce_buttons_2');
	
	/*
	* Callback function to filter the MCE settings
	*/
	 
	function my_mce_before_init_insert_formats( $init_array ) {  
	 
	// Define the style_formats array
	 
	    $style_formats = array(  
	/*
	* Each array child is a format with it's own settings
	* Notice that each array has title, block, classes, and wrapper arguments
	* Title is the label which will be visible in Formats menu
	* Block defines whether it is a span, div, selector, or inline style
	* Classes allows you to define CSS classes
	* Wrapper whether or not to add a new block-level element around any selected elements
	*/
	        array(  
	            'title' => 'Right Arrow',  
	            'block' => 'div',  
	            'classes' => 'right-arrow',
	            'wrapper' => false,
	             
	        ),
	        array(  
	            'title' => 'Quote',  
	            'block' => 'p',  
	            'classes' => 'quote_text',
	            'wrapper' => false,
	             
	        ),
	    );  
	    // Insert the array, JSON ENCODED, into 'style_formats'
	    $init_array['style_formats'] = json_encode( $style_formats );  
	     
	    return $init_array;  
	   
	} 
	// Attach callback to 'tiny_mce_before_init' 
	add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' ); 
	
	function create_post_type() {
  register_post_type( 'career',
    array(
      'labels' => array(
        'name' => __( 'Careers' ),
        'singular_name' => __( 'Career' )
      ),
      'public' => true,
      'has_archive' => false,
      'supports' => array( 'title', 'editor', 'thumbnail' ),
      'menu_icon' => 'dashicons-laptop',
      'rewrite' => array('slug' => 'careers'),
    )
  );
  register_post_type( 'portfolio',
    array(
      'labels' => array(
        'name' => __( 'Work' ),
        'singular_name' => __( 'Work' )
      ),
      'public' => true,
      'has_archive' => true,
      'supports' => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
      'menu_icon' => 'dashicons-laptop',
      'taxonomies' => array( 'post_tag','services'),
      'rewrite' => array('slug' => 'work'),
    )
  );
}
add_action( 'init', 'create_post_type' );

function prfx_image_enqueue() {
	global $post;
        wp_enqueue_media();
 
        // Registers and enqueues the required javascript.
        wp_register_script( 'meta-box-image', get_template_directory_uri() . '/js/meta-box-image.js', array( 'jquery' ) );
        wp_localize_script( 'meta-box-image', 'meta_image',
            array(
                'title' => __( 'Choose or Upload an Image', 'careers_thingtitle' ),
                'button' => __( 'Use this image', 'careers_thingtitle' ),
            )
        );
        wp_enqueue_script( 'meta-box-image' );
        wp_enqueue_script("jquery-ui",'//code.jquery.com/ui/1.11.4/jquery-ui.js', array('jquery'), '1.11.4');
        wp_enqueue_script("jquery-ui");
}
add_action( 'admin_enqueue_scripts', 'prfx_image_enqueue' );

//START CUSTOM TAXONOMY
function themes_taxonomy() {  
    register_taxonomy(  
        'services',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces). 
        'portfolio',        //post type name
        array(  
            'hierarchical' => true,  
            'label' => 'Services Performed',  //Display name
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'work', // This controls the base slug that will display before each term
                'with_front' => false // Don't display the category base before 
            )
        )  
    );  
}  
add_action( 'init', 'themes_taxonomy');

function prfx_admin_styles(){
    global $post;
        wp_enqueue_style( 'prfx_meta_box_styles', get_template_directory_uri() . '/css/metabox.css' );
}
add_action( 'admin_print_styles', 'prfx_admin_styles' );

function filter_post_type_link($link, $post)
{
    if ($post->post_type != 'portfolio')
        return $link;

    if ($cats = get_the_terms($post->ID, 'services'))
        $link = str_replace('%services%', array_pop($cats)->slug, $link);
    return $link;
}
add_filter('post_type_link', 'filter_post_type_link', 10, 2);

//END CUSTOM TAXONOMY


function sm_custom_meta() {
    add_meta_box( 'sm_meta', __( 'Featured Posts', 'sm-textdomain' ), 'sm_meta_callback', 'post' );
    add_meta_box( 'projects_meta', __( 'Project Information', 'project-domain' ), 'project_callback', 'portfolio' );
    add_meta_box( 'career_meta', __( 'Career Description', 'career-desc' ), 'career_desc', 'career' );
}
function sm_meta_callback( $post ) {
    $featured = get_post_meta( $post->ID );
    ?>
 
	<p>
    <div class="sm-row-content">
        <label for="meta-checkbox">
            <input type="checkbox" name="meta-checkbox" id="meta-checkbox" value="yes" <?php if ( isset ( $featured['meta-checkbox'] ) ) checked( $featured['meta-checkbox'][0], 'yes' ); ?> />
            <?php _e( 'Feature this post', 'sm-textdomain' )?>
        </label>
        
    </div>
</p>
<p>
    <div class="sm-row-content">
        <label for="meta-checkbox">
            <?php _e( 'Schema Markup', 'sm-textdomain' )?>
            <textarea name="schemamarkup" style="width: 100%; height: 150px;"><?php if ( isset ( $featured['schemamarkup'] ) ) echo $featured['schemamarkup'][0]; ?></textarea>
        </label>
        
    </div>
</p>
 
    <?php
}
function project_callback( $post ) {
    $featured = get_post_meta( $post->ID );
    ?>
 
	<p>
    <div class="sm-row-content">
        <label for="meta-checkbox">
            <input type="checkbox" name="meta-checkbox" id="meta-checkbox" value="yes" <?php if ( isset ( $featured['meta-checkbox'] ) ) checked( $featured['meta-checkbox'][0], 'yes' ); ?> />
            <?php _e( 'Featured this Project', 'project-domain' )?>
        </label>
        
    </div>
</p>
<p>
	
	<input type="text" name="portfoliobox_img" id="portfoliobox_img" value="<?php if ( isset ( $featured['portfoliobox_img'] ) ) echo $featured['portfoliobox_img'][0]; ?>" class="picval"/>
<!-- 				<a href="<?php if ( isset ( $prfx_stored_meta['portfoliobox_img'] ) ) echo $prfx_stored_meta['portfoliobox_img'][0]; ?>" class="picview" target="_blank" style="display: block;"><?php if ( isset ( $prfx_stored_meta['portfoliobox_img'] ) ) echo $prfx_stored_meta['portfoliobox_img'][0]; ?></a> -->
 				<img src="<?php if ( isset ( $featured['portfoliobox_img'] ) ) echo $featured['portfoliobox_img'][0]; ?>" class="pic_show" id="portfoliobox_img-img">
				<input type="button" class="picbtn button" id="portfoliobox_img-button" class="button picbtn" value="<?php _e( 'Choose an Optional Project Thumbnail', 'myplugin_textdomain' )?>" />
				<input type="button" class="removepic button" id="removepic" value="Remove Portfolio Thumbnail">
</p>

<input type="text" name="portfoliobox_logo" id="portfoliobox_logo" value="<?php if ( isset ( $featured['portfoliobox_logo'] ) ) echo $featured['portfoliobox_logo'][0]; ?>" class="picval"/>
<!-- 				<a href="<?php if ( isset ( $prfx_stored_meta['portfoliobox_logo'] ) ) echo $prfx_stored_meta['portfoliobox_logo'][0]; ?>" class="picview" target="_blank" style="display: block;"><?php if ( isset ( $prfx_stored_meta['portfoliobox_logo'] ) ) echo $prfx_stored_meta['portfoliobox_logo'][0]; ?></a> -->
 				<img src="<?php if ( isset ( $featured['portfoliobox_logo'] ) ) echo $featured['portfoliobox_logo'][0]; ?>" class="pic_show" id="portfoliobox_logo-img">
				<input type="button" class="picbtn button" id="portfoliobox_logo-button" class="button picbtn" value="<?php _e( 'Choose an Optional Project Logo', 'myplugin_textdomain' )?>" />
				<input type="button" class="removepic button" id="removepic" value="Remove Portfolio Logo">
</p>

 
    <?php
}
function career_desc( $post ) {
    $prfx_stored_meta = get_post_meta( $post->ID );
    $descstable = "";
    if ($prfx_stored_meta){
	    $descstable = $prfx_stored_meta['desc'][0];
    }
    ?>
	
    <div class="sm-row-content">
        <textarea name="desc" id="desc" style="width: 100%; height: 100px;"><?php echo $descstable; ?></textarea>
    </div>
 
    <?php
}
add_action( 'add_meta_boxes', 'sm_custom_meta' );

function sm_meta_save( $post_id ) {
 
    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'sm_nonce' ] ) && wp_verify_nonce( $_POST[ 'sm_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
 
    // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }
 
 // Checks for input and saves
if( isset( $_POST[ 'meta-checkbox' ] ) ) {
    update_post_meta( $post_id, 'meta-checkbox', 'yes' );
} else {
    update_post_meta( $post_id, 'meta-checkbox', '' );
}

if( isset( $_POST[ 'desc' ] ) ) { 
	$desc = sanitize_text_field($_POST['desc']);
    update_post_meta($post_id,'desc',$desc); 
}

if( isset( $_POST[ 'portfoliobox_img' ] ) ) { 
	$portfoliobox_img = sanitize_text_field($_POST['portfoliobox_img']);
    update_post_meta($post_id,'portfoliobox_img',$portfoliobox_img); 
}

if( isset( $_POST[ 'portfoliobox_logo' ] ) ) { 
	$portfoliobox_logo = sanitize_text_field($_POST['portfoliobox_logo']);
    update_post_meta($post_id,'portfoliobox_logo',$portfoliobox_logo); 
}

if( isset( $_POST[ 'schemamarkup' ] ) ) { 
	$schemamarkup = sanitize_text_field($_POST['schemamarkup']);
    update_post_meta($post_id,'schemamarkup',$schemamarkup); 
}
 
}
add_action( 'save_post', 'sm_meta_save' );

function prefix_estimated_reading_time( $content = '', $wpm = 300 ) {
	$clean_content = strip_shortcodes( $content );
	$clean_content = strip_tags( $clean_content );
	$word_count = str_word_count( $clean_content );
	$time = ceil( $word_count / $wpm );
	return $time;
}


?>