<?php get_header(); ?>
<div class="entry-content-page tostick">
<?php
		$metacheck = true;		  
		$query = new WP_Query(array(
			'post_type' => 'portfolio',
			'post_status' => 'publish',
			'numberposts' => -1
			// 'order'    => 'ASC'
		));
		$firstOne = true;
		while ($query->have_posts()) {
		$query->the_post();
		$portfoliometa = get_post_meta( $post->ID );
		$term_list = wp_get_post_terms($post->ID, 'services', array("fields" => "names"));
		if ( isset($portfoliometa['meta-checkbox']) && $metacheck == true){
			
		
	?>
	<div id="hero" class="portfolio" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'full'); ?>);">
		<div class="container">
			<h2>Featured Project</h2>
			<h3><?php echo the_title(); ?></h3>
			<ul>
				<?php foreach($term_list as $term){ ?>
				<li><?php echo $term; ?></li>
				<?php } ?>
			</ul>
			<a href="<?php echo get_permalink(get_the_ID()); ?>">View</a>
		</div>
	</div>
		<?php }
			
		if ( $metacheck == false){
		if ( isset ( $portfoliometa['portfoliobox_img'] ) ){
			$picthumb = $portfoliometa['portfoliobox_img'][0];
		} else {
			$picthumb = get_the_post_thumbnail_url(get_the_ID(),'full');
		}
		?>
		<?php if($firstOne){
			$firstOne = false;
		?>
		<a data-fancybox href="https://vimeo.com/509940266" class="project_block page" style="background-image: url(https://www.saltwaterco.com/wp-content/uploads/2021/03/portfoliohero.jpg)">
			<div class="cover"></div>
			<p class="title">View our Production Reel</p>
			<p class="view">Watch Now</p>
		</a>
		<?php } ?>
		<a href="<?php echo get_permalink(get_the_ID()); ?>" class="project_block page" style="background-image: url(<?php echo $picthumb; ?>)">
			<div class="cover"></div>
			<p class="title"><?php echo the_title(); ?></p>
			<p class="desc"><?php echo implode(', ', $term_list); ?></p>
			<p class="view">View Project</p>
		</a>
		<?php } $metacheck = false; } wp_reset_query(); ?>
<?php  get_footer(); ?>
</div>