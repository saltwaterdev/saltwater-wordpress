<?php
	/*
* Template Name: Careers Page
*/
get_header(); 
$currentid = $post->ID;
?>
<div class="tostick">
<div id="hero" class="shorter">
	<h1>Interested in working with us?</h1>
</div>
</div>
<div class="careers_list single">
	    <h4>CURRENT OPENINGS</h4>
		   	<ul>
			   	<?php
				  
				$query = new WP_Query(array(
				  'post_type' => 'career',
				  'post_status' => 'publish',
				  'numberposts' => -1
				  // 'order'    => 'ASC'
				));
				while ($query->have_posts()) {
				    $query->the_post();
				    if (get_the_ID() !== $currentid){
				?>
			   	<li><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?><span class="link">See Details</span></a></li>
			    <?php } } wp_reset_query(); ?>
		    </ul>
	    </div>
	<?php
    while ( have_posts() ) : the_post(); ?>
        <div class="entry-content-page">
            <?php the_content(); ?>
        </div>

    <?php
    endwhile;
    wp_reset_query();
    ?>
<?php  get_footer(); ?>