<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-WD2BLM');</script>
	<!-- End Google Tag Manager -->
  	<meta charset="utf-8">

	<?php // force Internet Explorer to use the latest rendering engine available ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width; initial-scale = 1.0; maximum-scale=1.0; user-scalable=no" />
	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

	
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
	<script type="text/javascript" src="//fast.fonts.net/jsapi/e6422a3c-7741-4915-a367-b8a8f1399e77.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-TXfwrfuHVznxCssTxWoPZjhcss/hp38gEOH8UPZG/JcXonvBQ6SlsIF49wUzsGno" crossorigin="anonymous">
	
	<link href="https://fonts.googleapis.com/css?family=Barlow+Condensed:300,400,500,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Barlow+Semi+Condensed" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Barlow:100,200,300,400,500,700" rel="stylesheet">
	<link rel="stylesheet" href="https://use.typekit.net/avb3lwz.css">
	
	<script type='text/javascript'>
	(function (d, t) {
	  var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
	  bh.type = 'text/javascript';
	  bh.src = 'https://www.bugherd.com/sidebarv2.js?apikey=fhaqxdpsgescijqaj4bwdg';
	  s.parentNode.insertBefore(bh, s);
	  })(document, 'script');
	</script>
	
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.1/dist/jquery.fancybox.min.css" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/hamburgers.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/css/playbook.css">
	
	<?php wp_head(); ?>
	
	<?php
		global $post;
		if (!empty($post)){
			$post_slug=$post->post_name;
		}
    ?>
	
	<!--[if lt IE 9]>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
	<![endif]-->
	
</head>

<?php
	$loginclass = "";
	if ( !is_user_logged_in() ) {
		$loginclass = "user";
	}
?>

<body id="page_<?php echo $post_slug; ?>" class="<?php echo $loginclass; ?>" data-id="<?php echo get_the_id(); ?>">
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WD2BLM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	
	<header id="header">
		<div class="container">
			<?php $custom_logo_id = get_theme_mod( 'custom_logo' ); 
				$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
				$image = $image[0];
			?>
			<a href="/" class="logo"><img src="<?php echo $image; ?>" alt="Saltwater Collective"></a>
			<div class="btns">
				<a class="btn"><span>Tell us about your new branch</span><i class="fas fa-pencil"></i></a>
				<a href="https://www.saltwaterco.com/wp-content/uploads/2021/06/branch-opening-playbook.pdf" target="_blank" class="link"><span>Download this Playbook <img src="/wp-content/themes/saltwater/img/playbook/chevron_orange.svg" alt="Download" ></span><i class="fas fa-download"></i></a>
			</div>
		</div>
	</header>
	
	<div id="nav">
		<div class="legend">
			<span class="current">1</span>
			<span class="max">9</span>
		</div>
		<ul>
			<li><a data-slide="1" class="current"><div>Cover</div><span>1</span></a></li>
			<li><a data-slide="2"><div>Intro</div><span>2</span></a></li>
			<li><a data-slide="3"><div>Google Ads</div><span>3</span></a></li>
			<li><a data-slide="4"><div>Paid Social</div><span>4</span></a></li>
			<li><a data-slide="5"><div>Organic Social Media</div><span>5</span></a></li>
			<li><a data-slide="6"><div>SEO</div><span>6</span></a></li>
			<li><a data-slide="7"><div>Email</div><span>7</span></a></li>
			<li><a data-slide="8"><div>Influencers</div><span>8</span></a></li>
			<li><a data-slide="9"><div>Contact Us</div><span>9</span></a></li>
		</ul>
	</div>