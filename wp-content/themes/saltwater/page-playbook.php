<?php
	/*
* Template Name: Playbook
*/
require ("header-playbook.php");
?>

<div class="scenes">
	<div class="scene" id="scene1">
		<img src="/wp-content/themes/saltwater/img/playbook/scene1img1.png" class="img1" alt="Calendar" >
		<img src="/wp-content/themes/saltwater/img/playbook/scene1img2.png" class="img2" alt="Phone" >
		<img src="/wp-content/themes/saltwater/img/playbook/scene1img3.png" class="img3" alt="Branch Opening" >
		<div class="container">
			<h4>A SALTWATER PLAYBOOK</h4>
			<h1>Branch Opening</h1>
			<h2>Digital Marketing Campaign</h2>
			<p>Is your bank or credit union opening a new location? This playbook is for you.</p>
		</div>
	</div>
	<div class="scene" id="scene2">
		<div class="container">
			<div class="col-sm-30">
				<h2>Whenever your bank opens a new branch the goal is to create awareness and drive engagement in the local community.</h2>
			</div>
			<div class="col-sm-15">
				<img src="/wp-content/themes/saltwater/img/playbook/scene2img1.png" class="img1" alt="Bank Teller" >
			</div>
			<div class="col-sm-15">
				<p>Your messaging needs to garner consideration and get people to take action, whether that means visiting the new branch or attending a grand opening event. But no matter how strong your message is, if you don’t get it in front of the right people your branch opening will flop.</p>
				<p><b>This is a playbook for promoting a new branch opening.</b></p>
				<p>Designed for banks and credit unions, this playbook runs through the roles that digital advertising channels should play in your branch opening campaign and includes tactical recommendations to use in your next campaign.</p>
			</div>
		</div>
	</div>
	<div class="scene" id="scene3">
		<div class="tip_toggle">
			<a><img src="/wp-content/themes/saltwater/img/playbook/protip_icon.png" alt="protip_icon" width="56" height="62" /></a>
		</div>
		<div class="protip">
			<a class="toggle"></a>
			<div class="contents">
				<h3>PRO TIP</h3>
				<p>Don’t be afraid to test and learn. Google Ads give you close control over your spending and the advanced targeting options mean you can test new messaging or promotions to narrow audiences without spending a lot of budget.</p>
			</div>
		</div>
		<div class="container">
			<div class="col-sm-30">
				<h2>Google Ads</h2>
				<h3>Target specific audiences, locations, and interests.</h3>
			</div>
			<div class="col-sm-15">
				<h4>The Role</h4>
				<p>Promote your branch opening it to a highly targeted and qualified audience. Your ads will only show to people who search the keywords and terms you are targeting, and you only pay when people click on them.</p>
				<img src="/wp-content/themes/saltwater/img/playbook/scene3img1.png" class="img1" alt="Google Ads" >
			</div>
			<div class="col-sm-15">
				<h4>Tactical Plays</h4>
				<p><b>Keep thinking local.</b></p>
				<p>Google ads can be hyper-targeted to a 1 mile radius from an address. Identify the major employers and residential buildings in your city and target ads specifically at those audiences.</p>
				<p><b>Don't forget about your existing customers.</b></p>
				<p>Use remarketing campaigns to deliver ads to people who visit your website but may not be near your new branch.</p>
				<p><b>Try a competitive conquest campaign.</b></p>
				<p>Target the brand names of your competitors so your ads show up when someone searches for your competitors, especially if you have an incentive for people switching banks.</p>
			</div>
		</div>
	</div>
	<div class="scene" id="scene4">
		<div class="tip_toggle">
			<a><img src="/wp-content/themes/saltwater/img/playbook/protip_icon_dark.png" alt="protip_icon" width="56" height="62" /></a>
		</div>
		<div class="protip dark">
			<a class="toggle"></a>
			<div class="contents">
				<h3>PRO TIP</h3>
				<p>Give your event a catchy name and include a thorough description, the correct date and time, and be sure to include imagery to build interest.</p>
			</div>
		</div>
		<div class="container">
			<div class="col-sm-30">
				<h2>Paid Social</h2>
				<h3>The right place at the right time.</h3>
			</div>
			<div class="col-sm-30 smaller">
				<div class="col-sm-15">
					<h4>The Role</h4>
					<p>Ensure your branch opening stays in front of users while they browse their social feeds. Complex ad formats allow for effective visual storytelling that can garner a better response than a simple ad elsewhere.</p>
					<img src="/wp-content/themes/saltwater/img/playbook/scene4img1.png" class="img1" alt="Paid Social" >
				</div>
				<div class="col-sm-15">
					<h4>Tactical Plays</h4>
					<p><b>Visuals are key.</b></p>
					<p>Promote a video showing your new branch so locals will know what to expect before they arrive.</p>
					<p><b>Use event ads on Facebook.</b></p>
					<p>These ads allow users to RSVP that they are “interested” in an event like a grand opening and then share that event with their friends and family on Facebook.</p>
					<p><b>Find people like your current customers.</b></p>
					<p>Upload your customer email list to Facebook. It will then profile those customers and identify other people on Facebook who have similar digital footprints and serve your ads to them.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="scene" id="scene5">
		<div class="tip_toggle">
			<a><img src="/wp-content/themes/saltwater/img/playbook/protip_icon.png" alt="protip_icon" width="56" height="62" /></a>
		</div>
		<div class="protip">
			<a class="toggle"></a>
			<div class="contents">
				<h3>PRO TIP</h3>
				<p>Actively monitor your direct messages on Facebook and Instagram, and get back to them. A quick and friendly response to a question could be the difference between someone coming in the door or not.</p>
			</div>
		</div>
		<div class="container">
			<div class="col-sm-30">
				<h2>Organic Social Media Content</h2>
				<h3>Top of mind, all the time.</h3>
			</div>
			<div class="col-sm-30 smaller">
				<div class="col-sm-15">
					<h4>The Role</h4>
					<p>A free way to communicate with your audience, actively promoting your branch opening on social media gives customers a way to ask questions or start discussions if they need to.</p>
					<img src="/wp-content/themes/saltwater/img/playbook/scene5img1.png" class="img1" alt="Organic Social Media Content" >
				</div>
				<div class="col-sm-15">
					<h4>Tactical Plays</h4>
					<p><b>Introduce your branch manager.</b></p>
					<p>Post photos or videos of your staff on your brand’s social media channels and have them share something about themselves, especially if they live locally.</p>
					<p><b>Use Instagram stories.</b></p>
					<p>About half of Instagram’s engagement happens in stories. Post stories during branch opening events to show off everything going on, and save them to a branch-specific highlight.</p>
					<p><b>Add the new branch location to your Facebook page.</b></p>
					<p>Log in to your Facebook Business Manager and add a new location to your account. This will enable people to tag the location in their posts on Facebook and Instagram.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="scene" id="scene6">
		<div class="container">
			<div class="col-sm-30">
				<h2>Search Engine Optimization (SEO)</h2>
				<h3>The thing you're most likely to skip.</h3>
			</div>
			<div class="col-sm-30 smaller">
				<div class="col-sm-15">
					<h4>The Role</h4>
					<p>Increase your branch’s visibility on<br> search engines and maps. Ensuring<br> the branch’s phone number,<br> address, and operating hours are<br> correct will go a long way<br> towards customers finding<br> their way into the<br> new location.</p>
					<img src="/wp-content/themes/saltwater/img/playbook/scene6img1.png" class="img1" alt="Search Engine Optimization" >
					<img src="/wp-content/themes/saltwater/img/playbook/scene6img2.png" class="img2" alt="Search" >
				</div>
				<div class="col-sm-15">
					<h4>Tactical Plays</h4>
					<p><b>Talk about your new branch on your website.</b></p>
					<p>Publish a blog post on your website, and include the address and local keywords like the neighborhood, town, or region where your branch is located.</p>
					<p><b>Think local.</b></p>
					<p>Put keywords aside and focus on helping people easily find the branch’s address and contact info online: set up a Google My Business listing, optimize directory listings, and make the branch’s contact info accessible on your website.</p>
					<p><b>Optimize your <span>Google My Business</span> listing.</b></p>
					<p>A GMB listing ensures info about your business will be on maps and in the detailed knowledge graph section of Google results pages. Optimize by adding photos, videos, and posts.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="scene" id="scene7">
		<div class="container">
			<div class="col-sm-30">
				<h2>Email</h2>
				<h3>Cut through the noise.</h3>
			</div>
			<div class="col-sm-30 smaller">
				<div class="col-sm-15">
					<h4>The Role</h4>
					<p>Advertising channels get cluttered and busy, but contacting your customers via email removes the competition for their attention. Plus, email tracking capabilities mean your marketing will get smarter every time.</p>
					<img src="/wp-content/themes/saltwater/img/playbook/scene7img1.png" class="img1" alt="Email" >
				</div>
				<div class="col-sm-15">
					<h4>Tactical Plays</h4>
					<p><b>Use progressive messaging.</b></p>
					<p>No one opens every email you send them. If you send one email about a promotion, follow it up with an email about the branch grand opening.</p>
					<p><b>Use an email marketing platform to track engagement</b></p>
					<p>Email marketing platforms provide analytics on who opened specific emails, who clicked through to your website, and what they looked at once there. This enables you to keep sending updates and information that you know users are interested in.</p>
					<p><b>Send people what they're interested in.</b></p>
					<p>Use your email marketing platform to build email lists based on what people click on. This will allow you to keep sending them updates and information that you know they are interested in.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="scene" id="scene8">
		<div class="container">
			<div class="col-sm-30">
				<h2>Influencer Marketing</h2>
				<h3>Your brand at the center of the conversation.</h3>
			</div>
			<div class="col-sm-30 smaller">
				<div class="col-sm-15">
					<h4>The Role</h4>
					<p>In locations with larger populations, use local Influencers to spread the word about your opening. Influencers, having built a community online around their interests or lifestyle, have a direct line to similar residents and provide<br> social proof to<br> encourage locals<br> to visit the branch.</p>
					<img src="/wp-content/themes/saltwater/img/playbook/scene8img1.png" class="img1" alt="Influencer Marketing" >
				</div>
				<div class="col-sm-15">
					<h4>Tactical Plays</h4>
					<p><b>Invite local influencers to your grand opening.</b></p>
					<p>Look for diverse influencers to reach a wider audience, and invite them via their preferred social media platform.</p>
					<p><b>Have the branch manager connect with influencers.</b></p>
					<p>One-on-one or as part of a group, create an occasion where local influencers can meet your branch manager, and get to know them and the bank. Inviting them to the branch prior to the grand opening, or meeting for a coffee are good options.</p>
					<p><b>Work with influencers to tell your story</b></p>
					<p>By engaging influencers in a campaign, your brand can work with them to share specific messaging or promotions to their audiences.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="scene" id="scene9">
		<div class="container">
			<div class="col-sm-30 smaller">
				<div class="col-sm-15">
					<h2>Like what you see?</h2>
					<p>With over a decade of experience working with banks and credit unions, we know what works.</p>
					<p>Talk to one of our Strategists today and see how these plays can help your new branch.</p>
					<?php echo do_shortcode('[contact-form-7 id="2400" title="Branch Opening Langing Page Form"]'); ?>
				</div>
				<div class="col-sm-15 socialcol">
					<div class="angle"></div>
					<div class="content">
						<p>Share this<br>digital playbook.</p>
						<ul>
							<li><a href="#"><img src="/wp-content/themes/saltwater/img/playbook/LinkedIn.png" alt="LinkedIn" width="32" height="30" /></a></li>
							<li><a href="#"><img src="/wp-content/themes/saltwater/img/playbook/Facebook.png" alt="Facebook" width="16" height="35" /></a></li>
							<li><a href="#"><img src="/wp-content/themes/saltwater/img/playbook/Twitter.png" alt="Twitter" width="41" height="33" /></a></li>
						</ul>
						<p><span>&mdash; OR &mdash;</span></p>
						<a href="https://www.saltwaterco.com/wp-content/uploads/2021/06/branch-opening-playbook.pdf" target="_blank" class="btn">Download</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	
<?php
	require ("footer-playbook.php");
?>