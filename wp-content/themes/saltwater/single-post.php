<?php
get_header(); 
$prfx_stored_meta = get_post_meta( $post->ID );
while ( have_posts() ) : the_post();
if (!get_avatar_url(get_the_author_meta('ID'))){
	$authorurl = get_stylesheet_directory_uri().'/img/default_person.png';
} else {
// 	$authorurl = get_avatar_url(get_the_author_meta('ID'));
	$authorurl = get_stylesheet_directory_uri().'/img/default_person.png';
}
?>

<div class="tostick">
<div id="hero" class="blog" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'full') ?>)">
	<div class="container">
		<div class="info">
			<span class="date"><?php the_time('n.j.Y') ?></span>
			<span class="sep">&bull;</span>
			<span class="cat"><?php the_category(); ?></span>
<!-- 			<span class="sep">&bull;</span> -->
		</div>
		<h1><?php echo the_title(); ?></h1>
		<div class="author">
			<div class="img" style="background-image: url(<?php echo $authorurl; ?>)"></div>
			<p class="name"><?php echo get_the_author_meta('first_name'); ?> <?php echo get_the_author_meta('last_name'); ?></p>
			<p class="title"><?php echo the_author_meta('description'); ?></p>
		</div>
	</div>
</div>
</div>
        <div class="entry-content-post">
	        
	        <div class="social_bar">
		        <p class="share">Share:</p>
		        <ul class="share">
			         <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" class="fb" target="_blank"><i class="fab fa-facebook-square"></i></a></li>
			        <li><a href="https://twitter.com/home?status=<?php echo get_the_title(); ?>%20-%20Via%20%40SaltwaterCo%20<?php echo get_permalink(); ?>" class="tw" target="_blank"><i class="fab fa-twitter-square"></i></a></li>
			        <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_permalink(); ?>&title=<?php echo get_the_title(); ?>&summary=&source=" class="li" target="_blank"><i class="fab fa-linkedin"></i></a></li>
			        <li><a href="mailto:?&subject=<?php echo get_the_title(); ?> - From Saltwater Co.&body=Check%20this%20out%3A%20<?php echo get_permalink(); ?>" class="em" target="_blank"><i class="far fa-envelope"></i></a></li>
		        </ul>
<!--
		        <?php
					$posts = $wpdb->get_results("SELECT * FROM likes WHERE pageid = '".get_the_id()."'");
					$ifliked = $wpdb->get_results("SELECT * FROM likes WHERE pageid = '".get_the_id()."' AND ip = ");
				?> 
-->
		        <a href="#" class="likes"><span>0</span></a>
		        <p class="like">Likes:</p>
	        </div>
			<div class="content">
            	<?php the_content(); ?>
            	<hr>
            	<?php 
				$tags = wp_get_post_tags( $post->ID );
				if (count($tags) > 0){
					echo '<h3>Tagged in:</h3>';
					echo '<ul class="tags">';
					foreach ($tags as $tag) {
					  echo '<li><a href="/blog?tag='.$tag->slug.'">' . $tag->name . '</a></li>';
					}
					echo '</ul>';
				}
				?>
	       </div>
	       <div class="social_bar">
		        <p class="share">Share:</p>
		        <ul class="share">
			        <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" class="fb" target="_blank"><i class="fab fa-facebook-square"></i></a></li>
			        <li><a href="https://twitter.com/home?status=<?php echo get_the_title(); ?>%20-%20Via%20%40SaltwaterCo%20<?php echo get_permalink(); ?>" class="tw" target="_blank"><i class="fab fa-twitter-square"></i></a></li>
			        <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_permalink(); ?>&title=<?php echo get_the_title(); ?>&summary=&source=" class="li" target="_blank"><i class="fab fa-linkedin"></i></a></li>
			        <li><a href="mailto:?&subject=<?php echo get_the_title(); ?> - From Saltwater Co.&body=Check%20this%20out%3A%20<?php echo get_permalink(); ?>" class="em" target="_blank"><i class="far fa-envelope"></i></a></li>
		        </ul>
		        <a href="#" class="likes"><span>0</span></a>
		        <p class="like">Likes:</p>
	        </div>
	        <hr>
	        <?php
		        $previous = get_previous_post();
				if ($previous){
					echo '<a href="'.get_the_permalink($previous).'" class="button back"><i class="fas fa-angle-left"></i> Back</a>';
				} ?>
<!-- 	        <?php previous_post_link('%link', 'Previous in category', TRUE, '13'); ?>  -->
        </div><!-- .entry-content-page -->
        
        <div class="row">
        	<hr>
        </div>
        
        <div class="related">
	        <div class="post active">
		        <div class="container">
			        <div class="info"><span class="date">5.13.2015</span><span class="sep">&bull;</span><ul class="cat"><li><a href="#">Lorem Ipsum</a></li></ul></div>
			        <p class="title">Lorem Ipsum Dolor Set Amet Consectetur ut Odio Nulu Massa Velit Augue Erat Mattis</p>
			        <a href="#" class="read">Read</a>
		        </div>
	        </div>
			<div class="post next">
		        <div class="container">
			        <div class="info"><span class="date">5.13.2015</span><span class="sep">&bull;</span><ul class="cat"><li><a href="#">Lorem Ipsum</a></li></ul></div>
			        <p class="title">Lorem Ipsum Dolor Set Amet Consectetur ut Odio Nulu Massa Velit Augue Erat Mattis</p>
			        <a href="#" class="read">Read</a>
		        </div>
	        </div>
	        <a href="#" class="next"></a>
        </div>
        
        <div class="entry-content-page">
	        <div class="waves blog">
		        <div class="content">
					<h5>Sign up to receive the latest on Industry insights</h5>
					<form action="https://success.saltwaterco.com/l/514801/2018-10-03/6fzkcb" method="post">
						<input type="text" name="email" placeholder="Email">
						<input type="submit" value="Send It!">
					</form>
		        </div>
	        </div>
        </div>
        
        <div style="display: none !important">
    <?php
    endwhile; //resetting the page loop
    wp_reset_query(); //resetting the page query
    ?>
        </div>

<?php get_footer(); ?>