<footer>
	<?php if ( is_front_page() || get_the_id() == 71 || get_the_id() == 42){ ?>
		  <div class="column col-sm-15">
			<div class="container">
				<a href="/careers/" class="careers_header">CAREERS AT SALTWATER</a>
				<p>Want to get in our wheelhouse? We're always interested in local talent. Here's what we're currently looking for:</p>
				<ul>
					<?php
				  
					$query = new WP_Query(array(
					  'post_type' => 'career',
					  'post_status' => 'publish',
					  'numberposts' => -1
					  // 'order'    => 'ASC'
					));
					while ($query->have_posts()) {
					    $query->the_post();
					?>
				   	<li><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?> <i class="far fa-chevron-right"></i></a></li>
				    <?php } wp_reset_query(); ?>
				</ul>
			</div>
		</div>
		<div class="column col-sm-15">
			<div class="container">
				<?php $custom_logo_id = get_theme_mod( 'custom_logo' ); 
					$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
					$image = $image[0];
				?>
				<div class="holder">
					<a href="/" class="logo"><img src="<?php echo $image; ?>" alt="Saltwater Collective"></a>
					<p>40 Congress Street, Fl. 5</p>
					<p>Portsmouth, NH 03801, USA</p>
					<p>T. 603.964.1100</p>
				</div>
				
<!-- 				MAP START	 -->
<div id="map"></div>
    <script>
// Initialize and add the map
function initMap() {
  // The location of Uluru
  var uluru = {lat: 43.076353, lng: -70.758683};
  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 15, center: uluru, styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]});
  // The marker, positioned at Uluru
  var marker = new google.maps.Marker({position: uluru, icon: window.location.origin+'/wp-content/themes/saltwater/img/smart.png', map: map});
}
    </script>
    <!--Load the API from the specified URL
    * The async attribute allows the browser to render the page while the API loads
    * The key parameter will contain your own API key (which is not needed for this tutorial)
    * The callback parameter executes the initMap() function
    -->
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDarwlfSpAZV5USDoA-FC-2YfAyOW4ZxNc&callback=initMap">
    </script>
<!-- 					MAP END -->
			
				<div class="bottom">
					<ul class="terms">
						
					</ul>
					<span>&copy; <?php echo date('Y');?> Saltwater Collective LLC.</span>
				</div>
			</div>
		</div>
	<?php } else { ?>
		<div class="row">
			<div class="left">
				<a href="/" class="logo small"></a>
				<span class="addr">40 Congress Street, Fl. 5</span>
				<span class="sep">&bull;</span>
				<span class="addr">Portsmouth, NH 03801, USA</span>
				<span class="sep">&bull;</span>
				<span class="addr">603.964.1100</span>
			</div>
			<div class="right">
				<?php wp_nav_menu(array(
				    'container' => false,                           // remove nav container
				   	'container_class' => 'menu cf',                 // class of container (should you choose to use it)
				   	'menu' => __( 'Footer Navigation', 'saltwater' ),  // nav name
				   	'menu_class' => 'nav footer_nav cf',               // adding custom nav class
				   	'theme_location' => 'footer_nav',                 // where it's located in the theme
				   	'before' => '',                                 // before the menu
				   	'after' => '',                                  // after the menu
				  	'link_before' => '',                            // before each link
				    'link_after' => '',                             // after each link
				    'depth' => 0,                                   // limit the depth of the nav
				    'fallback_cb' => ''                             // fallback function (if there is one)
				)); ?>
				<span class="copyright">&copy; <?php echo date('Y');?> Saltwater Collective LLC. All Rights Reserved</span>
			</div>
		</div>
	<?php }?>
	
	<?php if (is_page(18)){ ?>
	<script type="application/ld+json">
		[
		{
			"@context": "http://schema.org",
			"@type": "Organization",
			"name": "Saltwater Collective",
			"url": "https://www.saltwaterco.com",
			"sameAs": [
				"https://www.facebook.com/Saltwater-Collective-155024917872103/",
				"https://twitter.com/saltwaterco",
				"https://www.instagram.com/saltwatercreative/",
				"https://www.linkedin.com/company/saltwater-creative---technology/"
			],
			"logo":"https://www.saltwaterco.com/wp-content/uploads/2018/10/saltwaterlogo.png",
			"description":"Saltwater is a full-service digital marketing & advertising agency located in Portsmouth, New Hampshire. We specialize in website design, content marketing, media planning & buying, SEO, PPC and video production.",
			"address":{
				"@type": "PostalAddress",
				"streetAddress": "40 Congress Street, Fl. 5",
				"addressLocality": "Portsmouth",
				"addressRegion": "NH",
				"postalCode": "03801",
				"addressCountry": "United States"
			},
			"contactPoint":{
				"@type": "ContactPoint",
				"telephone": "+1(603) 964-1100",
				"contactType": "Sales and Service"
			}
		},
		{
			"@context": "http://schema.org",
			"@type": "WebSite",
			"name": "Saltwater Collective",
			"url": "https://www.saltwaterco.com"
		}
		]
	</script>
	<?php } ?>
	<?php if (is_page(42) || is_page(42)){ ?>
	<script type="application/ld+json">
		[
		{
			"@context": "http://schema.org/BankOrCreditUnion",
			"name": "Saltwater Collective",
			"contactPoint": {
				"@type": "ContactPoint",
				"telephone": "+1(603) 964-1100",
				"email": "info@saltwaterco.com",
				"contactType": ""
			},
			"specialOpeningHoursSpecification": [
				{
					"@type": "specialOpeningHoursSpecification",
					"closes": "17:00:00",
					"dayOfWeek": "http://schema.org/Monday",
					"opens": "08:00:00"
				},
				{
					"@type": "specialOpeningHoursSpecification",
					"closes": "17:00:00",
					"dayOfWeek": "http://schema.org/Tuesday",
					"opens": "08:00:00"
				},
				{
					"@type": "specialOpeningHoursSpecification",
					"closes": "17:00:00",
					"dayOfWeek": "http://schema.org/Wednesday",
					"opens": "08:00:00"
				},
				{
					"@type": "specialOpeningHoursSpecification",
					"closes": "17:00:00",
					"dayOfWeek": "http://schema.org/Thursday",
					"opens": "08:00:00"
				},
				{
					"@type": "specialOpeningHoursSpecification",
					"closes": "17:00:00",
					"dayOfWeek": "http://schema.org/Friday",
					"opens": "08:00:00"
				}
			]
		}
		]
	</script>
	<?php } ?>
	<?php if( get_post_type() == 'post' ) { 
		$author_id = get_post_field ('post_author', get_the_id());
		$display_name = get_the_author_meta( 'display_name' , $author_id ); 
	?>
	<script type="application/ld+json">
		[{
			"@context": "http://schema.org",
		    "@type": "BlogPosting",
		    "mainEntityOfPage": {
		        "@type": "WebPage",
		        "@id": "<?php echo get_permalink(get_the_ID()); ?>"
		    },
		    "headline": "<?php echo get_the_title(get_the_ID()); ?>",
		    "author": {
		        "@type": "Person",
		        "name": "<?php echo $display_name; ?>"
		    },
		    "datePublished": "<?php echo get_the_time('Y-m-d',get_the_ID()) ?>T<?php echo get_the_time('H:i:s+00:00',get_the_ID()) ?>",
		    "dateModified": "<?php echo get_the_time('Y-m-d',get_the_ID()) ?>T<?php echo get_the_time('H:i:s+00:00',get_the_ID()) ?>",
		    "url": "<?php echo get_permalink(get_the_ID()); ?>",
		    "image": [
		        {
		            "@type": "ImageObject",
		            "url": "<?php echo get_the_post_thumbnail_url(get_the_ID(),'full') ?>",
		            "width": 1600,
		            "height": 400
		        }
		    ],
		    "publisher": {
		        "@type": "Organization",
		        "name": "Saltwater Collective",
		        "logo": [
		            {
		                "@type": "ImageObject",
		                "url": "https://www.saltwaterco.com/wp-content/uploads/2018/10/saltwaterlogo.png",
		                "width": 421,
		                "height": 60
		            }
		        ]
		    }
		}]
	</script>
	<?php } ?>
	
</footer>

<?php wp_footer(); ?>
<script src="https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.1/dist/jquery.fancybox.min.js"></script>
<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/headroom.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/animations.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenLite.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.js"></script>

<script src="https://cdn.jsdelivr.net/parallax.js/1.4.2/parallax.min.js"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/rellax.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js"></script>

</body>
</html>