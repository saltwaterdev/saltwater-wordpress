<?php
	/*
* Template Name: Blog Page
*/
get_header(); 

	$args = array(
        'posts_per_page' => 1,
        'meta_key' => 'meta-checkbox',
        'meta_value' => 'yes',
        'post_type'        => 'post'
    );
    $featured = new WP_Query($args);
    if (!empty($featured->have_posts())) {
		$featuredid = $featured->posts[0]->ID;
	}

?>
	<div class="tostick">
		<?php if (!empty($featured->have_posts())) { ?>
			<div id="hero" class="blog page" style="background-image: url(<?php echo get_the_post_thumbnail_url($featuredid,'full') ?>)">
				<div class="container">
					<div class="info">
						<span class="date"><?php echo get_the_date('n.j.Y',$featuredid); ?></span>
						<span class="sep">&bull;</span>
						<span class="cat">
						<ul class="post-categories">
						<?php $categories = get_the_category( $featuredid); 
							$uncatCheck = false;
						foreach( $categories as $category ) {
							if ($category->name !== "Uncategorized"){
								echo "<li><a href='".$category->slug."'>".$category->name."</a></li>";
							} else {
								$uncatCheck = true;
							}
						} ?></span>
			 			<?php if ($uncatCheck == false){ ?><span class="sep">&bull;</span><?php } ?>
			 			<span class="time"><i class="fal fa-clock"></i> 
							<?php 
								$content_post = get_post($featuredid);
								$content = $content_post->post_content;
								$content = apply_filters('the_content', $content);
								$content = str_replace(']]>', ']]&gt;', $content);
								echo prefix_estimated_reading_time( $content ); 
							?> min
						</span>
					</div>
					<h2><?php echo get_the_title($featuredid); ?></h2>
					<a href="<?php echo get_permalink($featuredid); ?>" class="button">Read</a>
				</div>
			</div>
		<?php } ?>
		
		 <div class="entry-content-page">
	        <div class="waves blog">
		        <div class="content">
					<h5>Sign up to receive the latest on Industry insights</h5>
					<form action="https://success.saltwaterco.com/l/514801/2018-10-03/6fzkcb" method="post">
						<input type="text" name="email" placeholder="Email">
						<input type="submit" value="Submit">
					</form>
			    </div>
		    </div>
	    </div>
    </div>
    
    <div class="row">
	     <div class="filters">
		    <form id="search_posts">
		    	<input type="text" placeholder="Search Posts" name="search_posts">
		    	<input type="submit" value="">
		    </form>
		    <form id="categories">
			    <select name="category" id="category">
				    <option value="0">Category</option>
				    <?php $terms = get_terms('category');
					    if ( $terms && !is_wp_error( $terms ) ) :
					    	foreach ( $terms as $term ) {
						    	echo '<option value="'.$term->name.'">'.$term->name.'</option>';	
						    }
						endif;
					?>
			    </select>
		    </form>
		    <p class="label">Filter by:</p>
	    </div>
   		<hr>
   		<h2>Recent Posts:</h2>
    </div>
    
    <div class="posts">
	    <?php
		$args = array(
		 	'posts_per_page'   => -1,
		    'post_type'        => 'post',
		);
    	$allposts = new WP_Query($args);  
    	if( $allposts->have_posts()): 

		while( $allposts->have_posts()): $allposts->the_post();

		{ 
			
			if (!empty(get_the_post_thumbnail_url(get_the_ID(),'full'))){
		    	$postclass = "bgimg";
		    	$postimg = "style='background-image: url(".get_the_post_thumbnail_url(get_the_ID(),'full').")'";
	    	} else {
		    	$postclass = "";
		    	$postimg = "";
	    	}
			$term_list = wp_get_post_terms($post->ID, 'category', array("fields" => "names"));
		?>
     	
     	<div class="post <?php echo $postclass; ?>" <?php echo $postimg; ?>>
		    <a href="<?php echo get_permalink(get_the_ID()); ?>" class="cover"></a>
		    <div class="container">
			    <div class="info"><span class="date"><?php the_time('n.j.Y') ?></span>
			    <?php if (implode(', ', $term_list) !== "Uncategorized"){ ?>
			    <span class="sep">&bull;</span>
			    <span class="cats"><?php echo implode(', ', $term_list); ?></span>
			    <?php } ?>
			    </div>
			    <p class="date"><i class="fal fa-clock"></i> <?php echo prefix_estimated_reading_time( get_the_content() ); ?> min</p>
				<p class="title"><?php echo the_title(); ?></p>
				<a href="<?php echo get_permalink(get_the_ID()); ?>" class="read">Read <i class="fas fa-angle-right"></i></a>
		    </div>
	    </div>
     	
     	<?php }

	 	endwhile; 
	 	else:
	 	endif;
    	?>
	    
    </div>
    
<?php  get_footer(); ?>