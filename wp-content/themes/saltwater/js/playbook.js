jQuery(document).ready(function($) {
	currentScene = 1;
	canTrans = true;
	function nextScene(current,next){
		canTrans = false;
		$("#scene"+current).addClass("trans").addClass("off");
		$("#scene"+next).addClass("trans").addClass("active");
		$(".protip, #nav").removeClass("active");
		currentScene = next;
		transScene(currentScene);
		setTimeout(function(){ 
			$(".scene").removeClass("trans");
			if(next-current>1){
				for(x=current;x<next;x++){
					$("#scene"+x).addClass("active off");
				}
			}
			canTrans = true;
		}, 600);
		$(".legend span.current").text(currentScene);
		$("#nav ul li a").removeClass("current");
		$("#nav ul li a[data-slide='"+currentScene+"']").addClass("current");
		if(next == 9){
			$("footer a.scroll").fadeOut("fast");
		} else {
			$("footer a.scroll").fadeIn("fast");
		}
	}
	function prevScene(current,next){
		canTrans = false;
		$("#scene"+current).addClass("trans").removeClass("active");
		$("#scene"+next).addClass("trans").removeClass("off");
		$(".protip, #nav").removeClass("active");
		currentScene = next;
		transScene(currentScene);
		setTimeout(function(){ 
			$(".scene").removeClass("trans");
			if(current-next>1){
				for(x=(next+1);x<current;x++){
					$("#scene"+x).removeClass("active off");
				}
			}
			canTrans = true;
		}, 600);
		$(".legend span.current").text(currentScene);
		$("#nav ul li a").removeClass("current");
		$("#nav ul li a[data-slide='"+currentScene+"']").addClass("current");
		if(next == 9){
			$("footer a.scroll").fadeOut("fast");
		} else {
			$("footer a.scroll").fadeIn("fast");
		}
	}
	$("body").on("click","a.scroll",function(e) {
		e.preventDefault();
		if(canTrans){
			nextScene(currentScene,currentScene+1);
		}
	});
	$("body").on("click",".tip_toggle a, .protip a.toggle",function(e) {
		e.preventDefault();
		$(this).parents(".scene").children(".protip").toggleClass("active");
	});
	startScene1();
	function startScene1(){
		setTimeout(function(){ 
			$("#scene1").addClass("active");
			$("#scene1 .img1").addClass("active");
		}, 250);
		setTimeout(function(){ 
			$("#scene1 .img2").addClass("active");
		}, 500);
		setTimeout(function(){ 
			$("#scene1 .img3").addClass("active");
		}, 750);
		setTimeout(function(){ 
			$("#scene1 h4").addClass("active");
		}, 100);
		setTimeout(function(){ 
			$("#scene1 h1").addClass("active");
		}, 300);
		setTimeout(function(){ 
			$("#scene1 h2").addClass("active");
		}, 500);
		setTimeout(function(){ 
			$("#scene1 p").addClass("active");
		}, 700);
	}
	function transScene(currentScene){
		if(currentScene !== 1){
			if(currentScene % 2 == 0) {
				$("a.scroll, #nav, .legend").addClass("dark");
			} else {
				$("a.scroll, #nav, .legend").removeClass("dark");
			}
			setTimeout(function(){ 
				$("#scene"+currentScene+" .col-sm-30").addClass("active");
			}, 300);
			setTimeout(function(){ 
				$("#scene"+currentScene+" .col-sm-15").eq(0).addClass("active");
			}, 800);
			setTimeout(function(){ 
				$("#scene"+currentScene+" .col-sm-15").eq(1).addClass("active");
			}, 1300);
		}
	}
	$("body").on("click","header#header .btns a.btn",function(e) {
		e.preventDefault();
		if ($(window).width() >= 960) {
			if(canTrans && currentScene !== 9){
				nextScene(currentScene,9);
			}
		} else {
			$('html, body').animate({
		        scrollTop: $("#scene9").offset().top
		    }, 500);
		}
		
	});
	$("body").on("click","#nav ul li a",function(e) {
		e.preventDefault();
		navSelected = parseInt($(this).data("slide"));
		if(currentScene !== navSelected){
			if(canTrans){
				if(currentScene < navSelected){
					nextScene(currentScene,navSelected);
				}
				if(currentScene > navSelected){
					prevScene(currentScene,navSelected);
				}
			}
		}
	});
	$( "#nav" ).hover(
	  function() {
	  	$("#nav").addClass("active");
	  	$(".legend").addClass("inactive");
	  }, function() {
	      $("#nav").removeClass("active");
	  	  $(".legend").removeClass("inactive");
	  }
	);
	
	if ($(window).width() < 1024) {
// 		$("footer a.scroll span").text("NEXT");
	}
	
	
	//SCROLL
	up = 0;
	down = 0;
	direction = 0;
	activeScene = false;

	var event = 'onwheel' in document ? 'wheel' : 'onmousewheel' in document ? 'mousewheel' : 'DOMMouseScroll';
	window.addEventListener(event, callback);
	
	menuOpen = false;	
	function report(ammout) {
		if ($(window).width() >= 1024) {
			if (ammout == 0.25 || ammout == -0.25){
				up = 0;
				down = 0;
				direction = 0;
			}
			if (ammout < 0){
				down++;
				direction = direction - 1;
				//console.log("Down:" + down);
			}
			if (ammout > 0){
				up++;
				direction = direction + 1;
				//console.log(up);
			}
// 			console.log(direction);
		}
	}
	function callback(event) {
	    var normalized;
	    if (event.wheelDelta) {
	        normalized = (event.wheelDelta % 120 - 0) == -0 ? event.wheelDelta / 120 : event.wheelDelta / 12;
	    } else {
	        var rawAmmount = event.deltaY ? event.deltaY : event.detail;
	        normalized = -(rawAmmount % 3 ? rawAmmount * 10 : rawAmmount / 3);
	    }
	    report(normalized);
	    if (direction == -5){
		    if(canTrans && currentScene !== 9){
			    nextScene(currentScene,currentScene+1);
			}
		}
		if (direction == 5){
			if(canTrans && currentScene !== 1){
			    prevScene(currentScene,currentScene-1);
			}
		}
	}
	
	//TOUCH
	document.addEventListener('touchstart', handleTouchStart, false);        
	document.addEventListener('touchmove', handleTouchMove, false);
	
	var xDown = null;                                                        
	var yDown = null;
	
	function getTouches(evt) {
	  return evt.touches ||             // browser API
	         evt.originalEvent.touches; // jQuery
	}                                                     
	
	function handleTouchStart(evt) {
	    const firstTouch = getTouches(evt)[0];                                      
	    xDown = firstTouch.clientX;                                      
	    yDown = firstTouch.clientY;                                  
	};  
	
	function handleTouchMove(evt) {
	    if ( ! xDown || ! yDown ) {
	        return;
	    }
	
	    var xUp = evt.touches[0].clientX;                                    
	    var yUp = evt.touches[0].clientY;
	
	    var xDiff = xDown - xUp;
	    var yDiff = yDown - yUp;  
	    if ( yDiff > 0 ) {
		 	if(canTrans && currentScene !== 9){
			    nextScene(currentScene,currentScene+1);
			}   
		} else {
			if(canTrans && currentScene !== 1){
			    prevScene(currentScene,currentScene-1);
			}
		}
	}
	
});