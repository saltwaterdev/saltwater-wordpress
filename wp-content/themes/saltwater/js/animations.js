$( document ).ready(function() {
	
	if ($("body").hasClass("user")){
	
		var controller = new ScrollMagic.Controller();
		
		if ($("#page_bearpaw-river-brewing-company")[0]){
			var cansAnimation1 = TweenMax.to('.can_slider .can1', 1, {
			  opacity: 1,
			  ease: Linear.easeNone
			});
			var cansAnimation1_scene = new ScrollMagic.Scene({
			  triggerElement: '.can_slider',
		// 	  triggerHook: "onEnter",
		 	  offset: -400,
			  duration: 350 // pin the element for a total of 400px
			})
			.setTween(cansAnimation1)
			.addTo(controller)
			.reverse(false);
			
			if ($(window).width() >= 960) {
				leftCan2 = "39.2%";
				leftCan3 = "55.5%";
				leftCan4 = "71.5%";
			} else {
				leftCan2 = "0";
				leftCan3 = "0";
				leftCan4 = "0";
			}
			
			var cansAnimation = TweenMax.to('.can_slider .can2', 1, {
			  left: leftCan2,
			  opacity: 1,
			  ease: Linear.easeNone
			});
			var cansAnimation_scene = new ScrollMagic.Scene({
			  triggerElement: '.can_slider',
		// 	  triggerHook: "onEnter",
		 	  offset: -400,
			  duration: 500 // pin the element for a total of 400px
			})
			.setTween(cansAnimation)
			.addTo(controller)
			.reverse(false);
			
			var cansAnimation2 = TweenMax.to('.can_slider .can3', 1, {
			  left: leftCan3,
			  opacity: 1,
			  ease: Linear.easeNone
			});
			var cansAnimation2_scene = new ScrollMagic.Scene({
			  triggerElement: '.can_slider',
		// 	  triggerHook: "onEnter",
		 	  offset: -400,
			  duration: 550 // pin the element for a total of 400px
			})
			.setTween(cansAnimation2)
			.addTo(controller)
			.reverse(false);
			
			var cansAnimation3 = TweenMax.to('.can_slider .can4', 1, {
			  left: leftCan4,
			  opacity: 1,
			  ease: Linear.easeNone
			});
			var cansAnimation3_scene = new ScrollMagic.Scene({
			  triggerElement: '.can_slider',
		// 	  triggerHook: "onEnter",
		 	  offset: -400,
			  duration: 600 // pin the element for a total of 400px
			})
			.setTween(cansAnimation3)
			.addTo(controller)
			.reverse(false);
			
			var ipadFadein = TweenMax.to('.bearpaw_ipad img', 1, {
			  opacity: 1,
			  ease: Linear.easeNone
			});
			var ipadFadein_scene = new ScrollMagic.Scene({
			  triggerElement: '.bearpaw_ipad',
		// 	  triggerHook: "onEnter",
		 	  offset: -300,
			  duration: 300 // pin the element for a total of 400px
			})
			.setTween(ipadFadein)
			.addTo(controller)
			.reverse(false);
		}
		
		if ($("#page_homepage")[0]){
/*
			var animation1Move = TweenMax.to('.animation1', 1, {
			  top: 0,
			  opacity: 1,
			  ease: Linear.easeNone
			});
			var animation2Move = TweenMax.to('.animation2', 1, {
			  top: 0,
			  opacity: 1,
			  ease: Linear.easeNone
			});
*/
			var animation3Move = TweenMax.to('.animation3', 1, {
			  top: 0,
			  opacity: 1,
			  ease: Linear.easeNone
			});
			var animation4Move = TweenMax.to('.animation4', 1, {
			  top: 0,
			  opacity: 1,
			  ease: Linear.easeNone
			});
			var animation5Move = TweenMax.to('.animation5', 1, {
			  top: 0,
			  opacity: 1,
			  ease: Linear.easeNone
			});
			if ($(".animation6")[0]){
				var animation6Move = TweenMax.to('.animation6', 1, {
				  top: 0,
				  opacity: 1,
				  ease: Linear.easeNone
				});
			}
			var animation7Move = TweenMax.to('.animation7', 1, {
			  top: 0,
			  opacity: 1,
			  ease: Linear.easeNone
			});
			var animation8Move = TweenMax.to('.animation8', 1, {
			  top: 0,
			  opacity: 1,
			  ease: Linear.easeNone
			});
			var animation9Move = TweenMax.to('.animation9', 1, {
			  top: 0,
			  opacity: 1,
			  ease: Linear.easeNone
			});
			var animation10Move = TweenMax.to('.animation10', 1, {
			  top: 0,
			  opacity: 1,
			  ease: Linear.easeNone
			});
			var animation11Move = TweenMax.to('.animation11', 1, {
			  top: 0,
			  opacity: 1,
			  ease: Linear.easeNone
			});
			var animation12Move = TweenMax.to('.animation12', 1, {
			  top: 0,
			  opacity: 1,
			  ease: Linear.easeNone
			});
			var animation13Move = TweenMax.to('.animation13', 1, {
			  top: 0,
			  opacity: 1,
			  ease: Linear.easeNone
			});
			var animation14Move = TweenMax.to('.animation14', 1, {
			  top: 0,
			  opacity: 1,
			  ease: Linear.easeNone
			});
			var animation15Move = TweenMax.to('.animation15', 1, {
			  top: 0,
			  opacity: 1,
			  ease: Linear.easeNone
			});
			var animation16Move = TweenMax.to('.animation16', 1, {
			  top: 0,
			  opacity: 1,
			  ease: Linear.easeNone
			});
			var footerMove = TweenMax.to('footer', 1, {
			  top: 0,
			  opacity: 1,
			  ease: Linear.easeNone
			});
			var animation3Move_scene = new ScrollMagic.Scene({
			  triggerElement: '.animation3',
		// 	  triggerHook: "onEnter",
		 	  offset: -400,
			  duration: 250 // pin the element for a total of 400px
			})
			.setTween(animation3Move)
			.addTo(controller)
			.reverse(false);
			var animation4Move_scene = new ScrollMagic.Scene({
			  triggerElement: '.animation4',
		// 	  triggerHook: "onEnter",
		 	  offset: -400,
			  duration: 250 // pin the element for a total of 400px
			})
			.setTween(animation4Move)
			.addTo(controller)
			.reverse(false);
			var animation5Move_scene = new ScrollMagic.Scene({
			  triggerElement: '.animation5',
		// 	  triggerHook: "onEnter",
		 	  offset: -400,
			  duration: 250 // pin the element for a total of 400px
			})
			.setTween(animation5Move)
			.addTo(controller)
			.reverse(false);
			if ($(".animation6")[0]){
				var animation6Move_scene = new ScrollMagic.Scene({
				  triggerElement: '.animation6',
			// 	  triggerHook: "onEnter",
			 	  offset: -400,
				  duration: 250 // pin the element for a total of 400px
				})
				.setTween(animation6Move)
				.addTo(controller)
				.reverse(false);
			}
			var animation7Move_scene = new ScrollMagic.Scene({
			  triggerElement: '.animation7',
		// 	  triggerHook: "onEnter",
		 	  offset: -400,
			  duration: 250 // pin the element for a total of 400px
			})
			.setTween(animation7Move)
			.addTo(controller)
			.reverse(false);
			var animation8Move_scene = new ScrollMagic.Scene({
			  triggerElement: '.animation8',
		// 	  triggerHook: "onEnter",
		 	  offset: -400,
			  duration: 250 // pin the element for a total of 400px
			})
			.setTween(animation8Move)
			.addTo(controller)
			.reverse(false);
			var animation9Move_scene = new ScrollMagic.Scene({
			  triggerElement: '.animation9',
		// 	  triggerHook: "onEnter",
		 	  offset: -390,
			  duration: 250 // pin the element for a total of 400px
			})
			.setTween(animation9Move)
			.addTo(controller)
			.reverse(false);
			var animation10Move_scene = new ScrollMagic.Scene({
			  triggerElement: '.animation10',
		// 	  triggerHook: "onEnter",
		 	  offset: -280,
			  duration: 250 // pin the element for a total of 400px
			})
			.setTween(animation10Move)
			.addTo(controller)
			.reverse(false);
			var animation11Move_scene = new ScrollMagic.Scene({
			  triggerElement: '.animation11',
		// 	  triggerHook: "onEnter",
		 	  offset: -370,
			  duration: 250 // pin the element for a total of 400px
			})
			.setTween(animation11Move)
			.addTo(controller)
			.reverse(false);
			var animation12Move_scene = new ScrollMagic.Scene({
			  triggerElement: '.animation12',
		// 	  triggerHook: "onEnter",
		 	  offset: -360,
			  duration: 250 // pin the element for a total of 400px
			})
			.setTween(animation12Move)
			.addTo(controller)
			.reverse(false);
			var animation13Move_scene = new ScrollMagic.Scene({
			  triggerElement: '.animation13',
		// 	  triggerHook: "onEnter",
		 	  offset: -350,
			  duration: 250 // pin the element for a total of 400px
			})
			.setTween(animation13Move)
			.addTo(controller)
			.reverse(false);
			var animation14Move_scene = new ScrollMagic.Scene({
			  triggerElement: '.animation14',
		// 	  triggerHook: "onEnter",
		 	  offset: -340,
			  duration: 250 // pin the element for a total of 400px
			})
			.setTween(animation14Move)
			.addTo(controller)
			.reverse(false);
			var animation15Move_scene = new ScrollMagic.Scene({
			  triggerElement: '.animation15',
		// 	  triggerHook: "onEnter",
		 	  offset: -330,
			  duration: 250 // pin the element for a total of 400px
			})
			.setTween(animation15Move)
			.addTo(controller)
			.reverse(false);
			var animation16Move_scene = new ScrollMagic.Scene({
			  triggerElement: '.animation16',
		// 	  triggerHook: "onEnter",
		 	  offset: -400,
			  duration: 250 // pin the element for a total of 400px
			})
			.setTween(animation16Move)
			.addTo(controller)
			.reverse(false);
			var footerMove_scene = new ScrollMagic.Scene({
			  triggerElement: 'footer',
		// 	  triggerHook: "onEnter",
		 	  offset: -550,
			  duration: 250 // pin the element for a total of 400px
			})
			.setTween(footerMove)
			.addTo(controller)
			.reverse(false);
		}
		
		if ($(".product_scroll")[0]){
			if ($(window).width() < 768) {
				redDesktop = "9%";
				blackDesktop = "37%";
				grayDesktop = "69%";
			} else if ($(window).width() < 1200) {
				redDesktop = "16%";
				blackDesktop = "42%";
				grayDesktop = "71.25%";
			} else {
				redDesktop = "16%";
				blackDesktop = "42%";
				grayDesktop = "68.25%";
			}
			
			var redProductmove = TweenMax.to('.red_product', 1, {
			  left: redDesktop,
			  ease: Linear.easeNone
			});
			var blackProductmove = TweenMax.to('.black_product', 1, {
			  left: blackDesktop,
			  ease: Linear.easeNone
			});
			var grayProductmove = TweenMax.to('.gray_product', 1, {
			  left: grayDesktop,
			  ease: Linear.easeNone
			});
			
			var redProductmove_scene = new ScrollMagic.Scene({
			  triggerElement: '.product_scroll',
		// 	  triggerHook: "onEnter",
		 	  offset: -150,
			  duration: 403 // pin the element for a total of 400px
			})
			.setTween(redProductmove)
			.addTo(controller)
			.reverse(false);
			
			var blackProductmove_scene = new ScrollMagic.Scene({
			  triggerElement: '.product_scroll',
		// 	  triggerHook: "onEnter",
		 	  offset: -200,
			  duration: 405 // pin the element for a total of 400px
			})
			.setTween(blackProductmove)
			.addTo(controller)
			.reverse(false);
			
			var grayProductmove_scene = new ScrollMagic.Scene({
			  triggerElement: '.product_scroll',
		// 	  triggerHook: "onEnter",
		 	  offset: -100,
			  duration: 410 // pin the element for a total of 400px
			})
			.setTween(grayProductmove)
			.addTo(controller)
			.reverse(false);
			
		}
		
		if ($(".wdh_product_scroll")[0]){
			if ($(window).width() < 768) {
				wdh_iphone = "-4.5%";
				wdh_tablet = "-1.5%";
				wdh_desktop = "7%";
				wdh_hero = "31%";
				wdh_laptop = "-9%";
				wdh_brochure1 = "11%";
				wdh_brochure2 = "25.5%";
			} else if ($(window).width() < 968) {
				wdh_iphone = "6.5%";
				wdh_tablet = "10.5%";
				wdh_desktop = "25%";
				wdh_hero = "27%";
				wdh_laptop = "1%";
				wdh_brochure1 = "33%";
				wdh_brochure2 = "40.5%";
			} else if ($(window).width() < 1200) {
				wdh_iphone = "6.5%";
				wdh_tablet = "10.5%";
				wdh_desktop = "29%";
				wdh_hero = "29%";
				wdh_laptop = "2%";
				wdh_brochure1 = "33%";
				wdh_brochure2 = "40.5%";	
			} else {
				wdh_iphone = "11.5%";
				wdh_tablet = "14.5%";
				wdh_desktop = "29%";
				wdh_hero = "21%";
				wdh_laptop = "-7%";
				wdh_brochure1 = "33%";
				wdh_brochure2 = "40.5%";
			}
			
			var wdh_iphoneMove = TweenMax.to('.wdh_iphone', 1, {
			  left: wdh_iphone,
			  ease: Linear.easeNone
			});
			var wdh_tabletMove = TweenMax.to('.wdh_tablet', 1, {
			  left: wdh_tablet,
			  ease: Linear.easeNone
			});
			var wdh_desktopMove = TweenMax.to('.wdh_desktop', 1, {
			  left: wdh_desktop,
			  ease: Linear.easeNone
			});
			var wdh_heroMove = TweenMax.to('.wdh_hero', 1, {
			  right: wdh_hero,
			  ease: Linear.easeNone
			});
			var wdh_laptopMove = TweenMax.to('.wdh_laptop', 1, {
			  right: wdh_laptop,
			  ease: Linear.easeNone
			});
			var wdh_brochure1Move = TweenMax.to('.wdh_brochure1', 1, {
			  left: wdh_brochure1,
			  ease: Linear.easeNone
			});
			var wdh_brochure2Move = TweenMax.to('.wdh_brochure2', 1, {
			  left: wdh_brochure2,
			  ease: Linear.easeNone
			});
			
			var wdh_iphoneMove_scene = new ScrollMagic.Scene({
			  triggerElement: '.wdh_product_scroll',
		// 	  triggerHook: "onEnter",
		 	  offset: -250,
			  duration: 400 // pin the element for a total of 400px
			})
			.setTween(wdh_iphoneMove)
			.addTo(controller)
			.reverse(false);
			
			var wdh_tabletMove_scene = new ScrollMagic.Scene({
			  triggerElement: '.wdh_product_scroll',
		// 	  triggerHook: "onEnter",
		 	  offset: -250,
			  duration: 400 // pin the element for a total of 400px
			})
			.setTween(wdh_tabletMove)
			.addTo(controller)
			.reverse(false);
			
			var wdh_desktopMove_scene = new ScrollMagic.Scene({
			  triggerElement: '.wdh_product_scroll',
		// 	  triggerHook: "onEnter",
		 	  offset: -250,
			  duration: 400 // pin the element for a total of 400px
			})
			.setTween(wdh_desktopMove)
			.addTo(controller)
			.reverse(false);
			
			var wdh_heroMove_scene = new ScrollMagic.Scene({
			  triggerElement: '.wdh_product_scroll',
		// 	  triggerHook: "onEnter",
		 	  offset: -250,
			  duration: 400 // pin the element for a total of 400px
			})
			.setTween(wdh_heroMove)
			.addTo(controller)
			.reverse(false);
			
			var wdh_laptopMove_scene = new ScrollMagic.Scene({
			  triggerElement: '.wdh_product_scroll',
		// 	  triggerHook: "onEnter",
		 	  offset: -250,
			  duration: 400 // pin the element for a total of 400px
			})
			.setTween(wdh_laptopMove)
			.addTo(controller)
			.reverse(false);
			
			var wdh_brochure1Move_scene = new ScrollMagic.Scene({
			  triggerElement: '.wdh_product_scroll',
		// 	  triggerHook: "onEnter",
		 	  offset: -250,
			  duration: 400 // pin the element for a total of 400px
			})
			.setTween(wdh_brochure1Move)
			.addTo(controller)
			.reverse(false);
			
			var wdh_brochure2Move_scene = new ScrollMagic.Scene({
			  triggerElement: '.wdh_product_scroll',
		// 	  triggerHook: "onEnter",
		 	  offset: -250,
			  duration: 400 // pin the element for a total of 400px
			})
			.setTween(wdh_brochure2Move)
			.addTo(controller)
			.reverse(false);
			
		}
	
	} else {
		var controller = new ScrollMagic.Controller();
		
		if ($("#page_bearpaw-river-brewing-company")[0]){
			var cansAnimation1 = TweenMax.to('.can_slider .can1', 1, {
			  opacity: 1,
			  ease: Linear.easeNone
			});
			var cansAnimation1_scene = new ScrollMagic.Scene({
			  triggerElement: '.can_slider',
		// 	  triggerHook: "onEnter",
		 	  offset: -400,
			  duration: 350 // pin the element for a total of 400px
			})
			.setTween(cansAnimation1)
			.addTo(controller)
			.reverse(false);
			
			if ($(window).width() >= 960) {
				leftCan2 = "39.2%";
				leftCan3 = "55.5%";
				leftCan4 = "71.5%";
			} else {
				leftCan2 = "0";
				leftCan3 = "0";
				leftCan4 = "0";
			}
			
			var cansAnimation = TweenMax.to('.can_slider .can2', 1, {
			  left: leftCan2,
			  opacity: 1,
			  ease: Linear.easeNone
			});
			var cansAnimation_scene = new ScrollMagic.Scene({
			  triggerElement: '.can_slider',
		// 	  triggerHook: "onEnter",
		 	  offset: -400,
			  duration: 500 // pin the element for a total of 400px
			})
			.setTween(cansAnimation)
			.addTo(controller)
			.reverse(false);
			
			var cansAnimation2 = TweenMax.to('.can_slider .can3', 1, {
			  left: leftCan3,
			  opacity: 1,
			  ease: Linear.easeNone
			});
			var cansAnimation2_scene = new ScrollMagic.Scene({
			  triggerElement: '.can_slider',
		// 	  triggerHook: "onEnter",
		 	  offset: -400,
			  duration: 550 // pin the element for a total of 400px
			})
			.setTween(cansAnimation2)
			.addTo(controller)
			.reverse(false);
			
			var cansAnimation3 = TweenMax.to('.can_slider .can4', 1, {
			  left: leftCan4,
			  opacity: 1,
			  ease: Linear.easeNone
			});
			var cansAnimation3_scene = new ScrollMagic.Scene({
			  triggerElement: '.can_slider',
		// 	  triggerHook: "onEnter",
		 	  offset: -400,
			  duration: 600 // pin the element for a total of 400px
			})
			.setTween(cansAnimation3)
			.addTo(controller)
			.reverse(false);
			
			var ipadFadein = TweenMax.to('.bearpaw_ipad img', 1, {
			  opacity: 1,
			  ease: Linear.easeNone
			});
			var ipadFadein_scene = new ScrollMagic.Scene({
			  triggerElement: '.bearpaw_ipad',
		// 	  triggerHook: "onEnter",
		 	  offset: -300,
			  duration: 300 // pin the element for a total of 400px
			})
			.setTween(ipadFadein)
			.addTo(controller)
			.reverse(false);
		}
	}
});