jQuery(document).ready(function($) {
	//TESTIMONIAL SLIDER
	if($(".testimonial")[0]){
		$( ".testimonial" ).each(function( index ) {
			getMaxTestimonial($(this));
		});
	}
	function getMaxTestimonial(elem){
		maxHeight = 0;
		elem.find(".slide").find(".contents").each(function( index ) {
			if($(this).outerHeight() > maxHeight){
				maxHeight = $(this).outerHeight();
			}
		});
		elem.css("height",maxHeight);
	}
	$( window ).resize(function() {
		if($(".testimonial")[0]){
			$( ".testimonial" ).each(function( index ) {
				getMaxTestimonial($(this));
			});
		}
	});
	if($(".testimonial")[0] && $(".testimonial").find(".slide").length > 1){
		var intTestCarousel = setInterval(intTestTimer, 8000);
	}
	function intTestTimer() {
		$( ".testimonial" ).each(function( index ) {
			intHero = $(this);
			currentSlide = intHero.find(".slide.active").data("slide");
			maxSlides = intHero.find(".slide").length;
			nextSlide = currentSlide + 1;
			if(nextSlide > maxSlides){
				nextSlide = 1;
			}
			transitionIntTest(intHero,currentSlide,nextSlide);
		});
	}
	function transitionIntTest(elem,current,next){
		if(!elem.hasClass("lock")){
			elem.addClass("lock");
			elem.find(".slide[data-slide='"+next+"']").removeClass("active left right");
			elem.find(".slide[data-slide='"+next+"']").addClass("transition");
			elem.find(".slide[data-slide='"+current+"']").addClass("transition");
 			elem.find(".slide[data-slide='"+next+"']").addClass("active");
			elem.find(".slide[data-slide='"+current+"']").addClass("left");
			elem.find("ul.indicators li a").removeClass("active");
			elem.find("ul.indicators li a[data-slide='"+next+"']").addClass("active");
			setTimeout(function(){ 
 				elem.find(".slide[data-slide='"+next+"']").removeClass("transition");
				elem.find(".slide[data-slide='"+current+"']").removeClass("transition active left right");
				elem.removeClass("lock");
			}, 255);
		}
	}
	$("body").on("click",".testimonial ul.indicators li a",function(e) {
		e.preventDefault();
		if (!$(this).hasClass("active")){
			clearInterval(intTestCarousel);
			intHero = $(this).parent().parent().parent(".testimonial");
			currentSlide = intHero.find(".slide.active").data("slide");
			maxSlides = intHero.find(".slide").length;
			nextSlide = $(this).data("slide");
			transitionIntTest(intHero,currentSlide,nextSlide);
			setTimeout(function(){ 
				var intTestCarousel = setInterval(intTestTimer, 8000);
			}, 255);
		}
	});
	$("body").on("click",".testimonial .prev, .testimonial .next",function(e) {
		e.preventDefault();
		clearInterval(intTestCarousel);
		intHero = $(this).parent(".testimonial");
		currentSlide = intHero.find(".slide.active").data("slide");
		maxSlides = intHero.find(".slide").length;
		if ($(this).hasClass("prev")){
			nextSlide = currentSlide - 1;
		}
		if ($(this).hasClass("next")){
			nextSlide = currentSlide + 1;
		}
		if (nextSlide < 1){
			nextSlide = maxSlides;
		}
		if(nextSlide > maxSlides){
			nextSlide = 1;
		}
		transitionIntTest(intHero,currentSlide,nextSlide);
		setTimeout(function(){ 
			var intTestCarousel = setInterval(intTestTimer, 8000);
		}, 255);
	});
	
	//INTERIOR PAGE HERO SLIDER
	if($(".int_hero")[0] && $(".int_hero").find(".slide").length > 1){
		var intHeroCarousel = setInterval(intHeroTimer, 8000);
	}
	function intHeroTimer() {
		$( ".int_hero" ).each(function( index ) {
			intHero = $(this);
			currentSlide = intHero.find(".slide.active").data("slide");
			maxSlides = intHero.find(".slide").length;
			nextSlide = currentSlide + 1;
			if(nextSlide > maxSlides){
				nextSlide = 1;
			}
			transitionIntHero(intHero,currentSlide,nextSlide);
		});
	}
	function transitionIntHero(elem,current,next){
		if(!elem.hasClass("lock")){
			elem.addClass("lock");
			elem.find(".slide[data-slide='"+next+"']").removeClass("active left right");
			elem.find(".slide[data-slide='"+next+"']").addClass("transition");
			elem.find(".slide[data-slide='"+current+"']").addClass("transition");
 			elem.find(".slide[data-slide='"+next+"']").addClass("active");
			elem.find(".slide[data-slide='"+current+"']").addClass("left");
			elem.find("ul.indicators li a").removeClass("active");
			elem.find("ul.indicators li a[data-slide='"+next+"']").addClass("active");
			setTimeout(function(){ 
 				elem.find(".slide[data-slide='"+next+"']").removeClass("transition");
				elem.find(".slide[data-slide='"+current+"']").removeClass("transition active left right");
				elem.removeClass("lock");
			}, 255);
		}
	}
	$("body").on("click",".int_hero ul.indicators li a",function(e) {
		if (!$(this).hasClass("active")){
			clearInterval(intHeroCarousel);
			intHero = $(this).parent().parent().parent(".int_hero");
			currentSlide = intHero.find(".slide.active").data("slide");
			maxSlides = intHero.find(".slide").length;
			nextSlide = $(this).data("slide");
			transitionIntHero(intHero,currentSlide,nextSlide);
			setTimeout(function(){ 
				var intHeroCarousel = setInterval(intHeroTimer, 8000);
			}, 255);
		}
	});

	
	//BEARPAW CAROUSELS START
	$("body").on("click",".bearpaw_can_carousel a.next_carousel",function(e) {
		e.preventDefault();
		elem = $(".bearpaw_can_carousel");
		currentSlide = elem.find(".slide.active").data("slide");
		nextSlide = currentSlide + 1;
		if (nextSlide > (elem.find(".slide").length - 1)){
			nextSlide = 1;
		}
		futureSlide = nextSlide + 1;
		if (futureSlide > (elem.find(".slide").length - 1)){
			futureSlide = 1;
		}
		upcomingSlide = nextSlide - 1;
		if (upcomingSlide < 1){
			upcomingSlide = (elem.find(".slide").length - 1);
		}
		elem.find(".slide.holder").show().addClass("noanim").addClass("stagefarleft").html(elem.find(".slide[data-slide='"+futureSlide+"']").html());
		elem.find(".slide[data-slide='"+currentSlide+"']").addClass("transition").addClass("moveright");
		elem.find(".slide[data-slide='"+nextSlide+"']").addClass("transition").addClass("movecenter");
		elem.find(".slide[data-slide='"+futureSlide+"']").addClass("transition").addClass("movefarright");
		setTimeout(function(){ 
			elem.find(".slide.holder").addClass("transition").addClass("moveleft");
		}, 1);
		setTimeout(function(){ 
			elem.find(".slide[data-slide='"+currentSlide+"']").removeClass("transition").removeClass("active").addClass("right").removeClass("moveright");
			elem.find(".slide[data-slide='"+nextSlide+"']").removeClass("transition").addClass("active").removeClass("left").removeClass("movecenter");
			elem.find(".slide[data-slide='"+futureSlide+"']").removeClass("transition").removeClass("right").removeClass("active").addClass("left").removeClass("movefarright");
 			elem.find(".slide.holder").hide().removeClass("transition").removeClass("moveleft").removeClass("noanim").removeClass("stagefarleft").html("");
		}, 501);
	});
	$("body").on("click",".bearpaw_poster_carousel a.next_carousel",function(e) {
		e.preventDefault();
		elem = $(".bearpaw_poster_carousel");
		currentSlide = elem.find(".slide.active").data("slide");
		nextSlide = currentSlide + 1;
		if (nextSlide > (elem.find(".slide").length - 1)){
			nextSlide = 1;
		}
		futureSlide = nextSlide + 1;
		if (futureSlide > (elem.find(".slide").length - 1)){
			futureSlide = 1;
		}
		upcomingSlide = nextSlide - 1;
		if (upcomingSlide < 1){
			upcomingSlide = (elem.find(".slide").length - 1);
		}
		elem.find(".slide.holder").show().addClass("noanim").addClass("stagefarleft").html(elem.find(".slide[data-slide='"+futureSlide+"']").html());
		elem.find(".slide[data-slide='"+currentSlide+"']").addClass("transition").addClass("moveright");
		elem.find(".slide[data-slide='"+nextSlide+"']").addClass("transition").addClass("movecenter");
		elem.find(".slide[data-slide='"+futureSlide+"']").addClass("transition").addClass("movefarright");
		setTimeout(function(){ 
			elem.find(".slide.holder").addClass("transition").addClass("moveleft");
		}, 1);
		setTimeout(function(){ 
			elem.find(".slide[data-slide='"+currentSlide+"']").removeClass("transition").removeClass("active").addClass("right").removeClass("moveright");
			elem.find(".slide[data-slide='"+nextSlide+"']").removeClass("transition").addClass("active").removeClass("left").removeClass("movecenter");
			elem.find(".slide[data-slide='"+futureSlide+"']").removeClass("transition").removeClass("right").removeClass("active").addClass("left").removeClass("movefarright");
 			elem.find(".slide.holder").hide().removeClass("transition").removeClass("moveleft").removeClass("noanim").removeClass("stagefarleft").html("");
		}, 501);
	});
	$("body").on("click",".bearpaw_can_carousel a.back_carousel",function(e) {
		e.preventDefault();
		elem = $(".bearpaw_can_carousel");
		currentSlide = elem.find(".slide.active").data("slide");
		nextSlide = currentSlide - 1;
		if (nextSlide < 1){
			nextSlide = (elem.find(".slide").length - 1);
		}
		futureSlide = nextSlide - 1;
		if (futureSlide < 1){
			futureSlide = (elem.find(".slide").length - 1);
		}
		upcomingSlide = nextSlide + 1;
		if (upcomingSlide > (elem.find(".slide").length - 1)){
			upcomingSlide = 1;
		}
		elem.find(".slide.holder").show().addClass("noanim").addClass("stagefarright").html(elem.find(".slide[data-slide='"+futureSlide+"']").html());
		elem.find(".slide[data-slide='"+currentSlide+"']").addClass("transition").addClass("moveleft");
		elem.find(".slide[data-slide='"+nextSlide+"']").addClass("transition").addClass("movecenter");
		elem.find(".slide[data-slide='"+futureSlide+"']").addClass("transition").addClass("movefarleft");
		setTimeout(function(){ 
			elem.find(".slide.holder").addClass("transition").addClass("moveright");
		}, 1);
		setTimeout(function(){ 
			elem.find(".slide[data-slide='"+currentSlide+"']").removeClass("transition").removeClass("active").addClass("left").removeClass("moveleft");
			elem.find(".slide[data-slide='"+nextSlide+"']").removeClass("transition").addClass("active").removeClass("right").removeClass("left").removeClass("movecenter");
			elem.find(".slide[data-slide='"+futureSlide+"']").removeClass("transition").removeClass("left").removeClass("active").addClass("right").removeClass("movefarleft");
 			elem.find(".slide.holder").hide().removeClass("transition").removeClass("moveright").removeClass("noanim").removeClass("stagefarright").html("");
		}, 501);
	});
	$("body").on("click",".bearpaw_poster_carousel a.back_carousel",function(e) {
		e.preventDefault();
		elem = $(".bearpaw_poster_carousel");
		currentSlide = elem.find(".slide.active").data("slide");
		nextSlide = currentSlide - 1;
		if (nextSlide < 1){
			nextSlide = (elem.find(".slide").length - 1);
		}
		futureSlide = nextSlide - 1;
		if (futureSlide < 1){
			futureSlide = (elem.find(".slide").length - 1);
		}
		upcomingSlide = nextSlide + 1;
		if (upcomingSlide > (elem.find(".slide").length - 1)){
			upcomingSlide = 1;
		}
		elem.find(".slide.holder").show().addClass("noanim").addClass("stagefarright").html(elem.find(".slide[data-slide='"+futureSlide+"']").html());
		elem.find(".slide[data-slide='"+currentSlide+"']").addClass("transition").addClass("moveleft");
		elem.find(".slide[data-slide='"+nextSlide+"']").addClass("transition").addClass("movecenter");
		elem.find(".slide[data-slide='"+futureSlide+"']").addClass("transition").addClass("movefarleft");
		setTimeout(function(){ 
			elem.find(".slide.holder").addClass("transition").addClass("moveright");
		}, 1);
		setTimeout(function(){ 
			elem.find(".slide[data-slide='"+currentSlide+"']").removeClass("transition").removeClass("active").addClass("left").removeClass("moveleft");
			elem.find(".slide[data-slide='"+nextSlide+"']").removeClass("transition").addClass("active").removeClass("right").removeClass("left").removeClass("movecenter");
			elem.find(".slide[data-slide='"+futureSlide+"']").removeClass("transition").removeClass("left").removeClass("active").addClass("right").removeClass("movefarleft");
 			elem.find(".slide.holder").hide().removeClass("transition").removeClass("moveright").removeClass("noanim").removeClass("stagefarright").html("");
		}, 501);
	});
	
	//BEARPAW CAROUSELS END
	
	
		//full size image Carousel START
		carouselPlay = true;
	$("body").on("click",".carousel.double.full .next",function(e) {
		e.preventDefault();
		if (carouselPlay == true){
			carouselPlay = false;
			thisCarousel = $(this).parent(".carousel");
			activeSlide = parseInt(thisCarousel.find(".active").data("slide"));
			nextSlide = activeSlide + 1;
			newSlide = nextSlide + 1;
			if ($(window).width() < 768) {
				changeVar = 0;
			} else {
				changeVar = 0;
			}
			leftTotal = ($( window ).width() - (changeVar*2)) * -1;
			widthSlide = $( window ).width() - (changeVar*2);
			if (newSlide > thisCarousel.children(".slide").length){
				newSlide = false;
				widthSlide = $( window ).width() - changeVar;
				$(this).hide();
				$(this).siblings(".back").show();
			}
			if (thisCarousel.hasClass("contact")){
				$(this).siblings(".back").show();
			}
			thisCarousel.find(".active").addClass("outgoing");
			thisCarousel.find("[data-slide='" + nextSlide + "']").addClass("stage");
			if (newSlide !== false){
				thisCarousel.find("[data-slide='" + newSlide + "']").addClass("incoming").css("left","100%");
			}
			thisCarousel.find(".outgoing").animate({
				left: leftTotal+"px"
			}, 200, function() {
				thisCarousel.find(".outgoing").css("left",leftTotal+"px").removeClass("active").removeClass("outgoing");
			});
			thisCarousel.find(".stage").animate({
				left: changeVar+"px"
			}, 200, function() {
				thisCarousel.find(".stage").addClass("active").removeClass("nextslide").removeClass("stage");
				carouselPlay = true;
			});
			if (newSlide !== false){
				incomingMove = $( window ).width() - changeVar;
				thisCarousel.find(".incoming").animate({
					left: incomingMove+"px"
				}, 200, function() {
					thisCarousel.find(".incoming").addClass("nextslide").removeClass("incoming");
				});
			}
		}
	});
	$("body").on("click",".carousel.double.full .back",function(e) {
		e.preventDefault();
		if (carouselPlay == true){
			carouselPlay = false;
			thisCarousel = $(this).parent(".carousel");
			activeSlide = parseInt(thisCarousel.find(".active").data("slide"));
			nextSlide = activeSlide - 1;
			newSlide = nextSlide - 1;
			slideoffSlide = activeSlide + 1;
			if ($(window).width() < 768) {
				changeVar = 0;
			} else {
				changeVar = 0;
			}
			leftTotal = changeVar;
			leftPos = ($( window ).width() - (changeVar*2)) * -1;
			widthSlide = $( window ).width() - (changeVar);
			if (newSlide < 1){
				newSlide = false;
				widthSlide = $( window ).width() - changeVar;
				$(this).hide();
				$(this).siblings(".next").show();
				leftTotal = 0;
			}
			if (thisCarousel.hasClass("contact")){
				$(this).siblings(".next").show();
			}
			thisCarousel.find(".active").addClass("outgoing");
			thisCarousel.find("[data-slide='" + nextSlide + "']").css("left",leftPos).addClass("stage");
			if (newSlide !== false){
				thisCarousel.find("[data-slide='" + newSlide + "']").addClass("incoming").css("left","-100%");
			}
			thisCarousel.find(".outgoing").animate({
				left: widthSlide+"px"
			}, 200, function() {
				thisCarousel.find(".outgoing").css("left",widthSlide+"px").removeClass("active").addClass("nextslide").removeClass("outgoing");
			});
			thisCarousel.find("[data-slide='" + slideoffSlide + "']").animate({
				left: "100%"
			}, 200, function() {
				thisCarousel.find(".nextslide").removeClass("nextslide");
			});
			thisCarousel.find(".stage").animate({
				left: leftTotal
			}, 200, function() {
				thisCarousel.find(".stage").css("left",leftTotal+"px").addClass("active").removeClass("nextslide").removeClass("stage");
				carouselPlay = true;
			});
			if (newSlide !== false){
				incomingMove = ($( window ).width() - (changeVar*2))*-1;
				thisCarousel.find(".incoming").animate({
					left: incomingMove+"px"
				}, 200, function() {
					thisCarousel.find(".incoming").removeClass("incoming");
				});
			}
		}
	});
	//full size image Carousel END
	
	
	
	
	//MENU FUNCTIONS
	$("body").on("click","header .menu, nav .close",function(e) {
		$(".hamburger").toggleClass("is-active");
 		e.preventDefault();
		toggleMenu();
	}); 
/*
	$("body").on("click",".rfp-attachment",function(e) {
 		e.preventDefault();
 		$(".rfp-attachment input[type='file']").click();
	});
*/
	if ($("#page_homepage")[0] && $(window).width() < 768){
		$("#hero").append('<a href="#" class="scroll_trigger"></a>');
	}
	if ($(".font_holder.wellpet")[0]){
		wellpetfontHeight();
	}
	function wellpetfontHeight(){
		totalHeight = $(".font_holder.cervoholder").outerHeight() + $(".font_holder.routerholder").outerHeight();
		colorHeight = totalHeight / 3;
		$(".colorbox").css("height","auto");
		if ($(window).width() < 768) {
			
		} else {
			$(".colorbox").css("height",colorHeight);
		}
	}
	$("body").on("click",".likes",function(e) {
		e.preventDefault();
		if ($(this).hasClass("liked")){
			jQuery.getJSON(
			"http://gd.geobytes.com/GetCityDetails?callback=?",function (data) 
			{
	// 			console.log(data.geobytesipaddress);
				ipAddress = data.geobytesipaddress;
				pageID = $("body").data("id");
				getLikes(ipAddress, pageID, 3);
			}
			);
		} else {
			jQuery.getJSON(
			"http://gd.geobytes.com/GetCityDetails?callback=?",function (data) 
			{
	// 			console.log(data.geobytesipaddress);
				ipAddress = data.geobytesipaddress;
				pageID = $("body").data("id");
				getLikes(ipAddress, pageID, 2);
			}
			);
		}
	});
	if ($(".entry-content-post")[0]){
		jQuery.getJSON(
		"http://gd.geobytes.com/GetCityDetails?callback=?",function (data) 
		{
// 			console.log(data.geobytesipaddress);
			ipAddress = data.geobytesipaddress;
			pageID = $("body").data("id");
			getLikes(ipAddress, pageID, 1);
		}
		);
	}
	function getLikes(ip,id,f){
			$.ajax({
	        url: "//"+window.location.hostname+"/tools/likes.php",
	        	type: 'post',
		        data: "f="+f+"&ip="+ip+"&id="+id,
		        dataType: 'html',
		        success: function(response) {
			       if (response == "liked"){
				       $(".likes").addClass("liked");
				       likesCount = parseInt($(".likes span").text()) + 1;
				       $(".likes span").text(likesCount);
			       } else if (response == "unliked"){
				       $(".likes").removeClass("liked");
				       likesCount = parseInt($(".likes span").text()) - 11;
				       $(".likes span").text(likesCount);
			       }else {
				       if (response > 0){
					       $(".likes").addClass("liked");
					       $(".likes span").text(response);
				       }
			       }
	// 	          	console.log(response);
		        },
	            error: function (textStatus, errorThrown) {
	
/*
	                console.log(textStatus);
	                console.log(errorThrown);
*/
	
	            }
		    });
	}
	
	menuMake();
	function menuMake(){
		elem = $("#menu-main-nav");
		for(x=0;x<elem.children("li").length;x++){
			elem.children("li").eq(x).addClass("bb");
			if (x == 0){
				elem.children("li").eq(x).addClass("bt");
			}
		}
		elem = $(".careers_list ul");
		for(x=0;x<elem.children("li").length;x++){
			if (x !== elem.children("li").length - 1 || $(".careers_list").hasClass("single")){
				elem.children("li").eq(x).addClass("bb");
			}
			if (x == 0){
				elem.children("li").eq(x).addClass("bt");
			}
		}
	}
	$( "nav ul#menu-main-nav li" ).hover(
	  function() {
		maxItems = $("#menu-main-nav").children("li").length;
	    thisItem = $(this).index();
	    prevItem = thisItem - 1;
	    $(this).addClass("bbon");
	    if (thisItem == 0){
		    $(this).addClass("bton");
	    }
	    if (prevItem >= 0){
		    $("#menu-main-nav").children("li").eq(prevItem).addClass("bbon");
	    }
	  }, function() {
	    maxItems = $("#menu-main-nav").children("li").length;
	    thisItem = $(this).index();
	    prevItem = thisItem - 1;
	    $(this).removeClass("bbon");
	    if (thisItem == 0){
		    $(this).removeClass("bton");
	    }
	    if (prevItem >= 0){
		    $("#menu-main-nav").children("li").eq(prevItem).removeClass("bbon");
	    }
	  }
	);
	$( "nav form #searchsubmit" ).hover(
	  function() {
		$("#searchform").addClass("hover");
	  }, function() {
	    $("#searchform").removeClass("hover");
	  }
	);
	$( "#search_posts input[type='submit']" ).hover(
	  function() {
		$("#search_posts").addClass("hover");
	  }, function() {
	    $("#search_posts").removeClass("hover");
	  }
	);
	$( ".careers_list ul li" ).hover(
	  function() {
		maxItems = $(".careers_list ul").children("li").length;
	    thisItem = $(this).index();
	    prevItem = thisItem - 1;
	    $(this).addClass("bbon");
	    if (thisItem == 0){
		    $(this).addClass("bton");
	    }
	    if (prevItem >= 0){
		    $(".careers_list ul").children("li").eq(prevItem).addClass("bbon");
	    }
	  }, function() {
	    maxItems = $(".careers_list ul").children("li").length;
	    thisItem = $(this).index();
	    prevItem = thisItem - 1;
	    $(this).removeClass("bbon");
	    if (thisItem == 0){
		    $(this).removeClass("bton");
	    }
	    if (prevItem >= 0){
		    $(".careers_list ul").children("li").eq(prevItem).removeClass("bbon");
	    }
	  }
	);
	function toggleMenu(){
		$("html,body").toggleClass("solid");
		$("header").toggleClass("open");
		if ($("header").hasClass("open")){
			$("nav").show();
			$( "nav" ).animate({
				opacity: "1"
			}, 150, function() {
			});
		} else {
			$( "nav" ).animate({
				opacity: "0"
			}, 150, function() {
				$("nav").hide();
			});
		}
	}
	//END MENU FUNCTIONS
	if ($(".color_inert")[0]){
		getcolorHeight();
	}
	if ($(".related")[0]){
		getmaxRelated();
	}
	if ($(".bars")[0]){
		getbarChart();
	}
	if ($("#page_blog .posts")[0]){
// 		getmaxPosts();
	}
	if ($("#about_carousel")[0]){
		getaboutHeight();
		getaboutCarousel();
	}
	if ($(".carousel.single, .carousel.double, .carousel.triple")[0]){
		getcarouselHeight();
	}
	if ($(".carousel.links")[0]){
		getcarouselLinks();
	}
	setTimeout(function(){ 
		if ($(".carousel.single, .carousel.double, .carousel.triple")[0]){
			getcarouselHeight();
		}
	}, 1000);
	$( ".colorbox.finder" ).each(function( index ) {
		colorMatch = $(this).find("p").text();
		$(this).css("background-color",colorMatch);
	});
	$( window ).resize(function() {
		if ($("#about_carousel")[0]){
			getaboutHeight();
			getaboutCarousel();
		}
		if ($(".carousel.single, .carousel.double, .carousel.triple")[0]){
			getcarouselHeight();
		}
		if ($(".color_inert")[0]){
			getcolorHeight();
		}
		if ($(".font_holder.wellpet")[0]){
			wellpetfontHeight();
		}
	});
	function getcolorHeight(){
		$( ".color_inert" ).each(function( index ) {
			if ($(window).width() < 768) {
				$(this).attr("style","");
				elem = $(this).find(".getheight");
				elemHeight = elem.outerHeight();
				$(this).css("height",elemHeight*2);
				$(this).find(".colorbox.full").css("height",elemHeight*0.5);
				$(this).find(".colorbox.ten").css("height",elemHeight*.1);
				$(this).find(".colorbox.twenty").css("height",elemHeight*.1);
				$(this).find(".colorbox.half").css("height",elemHeight*.1);
				$(this).find(".colorbox.full").find("p").css("line-height",elemHeight*0.5+"px");
				$(this).find(".colorbox.ten").find("p").css("line-height",elemHeight*.1+"px");
				$(this).find(".colorbox.twenty").find("p").css("line-height",elemHeight*.1+"px");
				$(this).find(".colorbox.half").find("p").css("line-height",elemHeight*.1+"px");
			} else {
				$(this).attr("style","");
				elem = $(this).find(".getheight");
				elemHeight = elem.outerHeight();
				$(this).css("height",elemHeight);
				$(this).find(".colorbox.full").css("height",elemHeight);
				$(this).find(".colorbox.ten").css("height",elemHeight*.1);
				$(this).find(".colorbox.twenty").css("height",elemHeight*.2);
				$(this).find(".colorbox.half").css("height",elemHeight*.5);
				$(this).find(".colorbox.full").find("p").css("line-height",elemHeight+"px");
				$(this).find(".colorbox.ten").find("p").css("line-height",elemHeight*.1+"px");
				$(this).find(".colorbox.twenty").find("p").css("line-height",elemHeight*.2+"px");
				$(this).find(".colorbox.half").find("p").css("line-height",elemHeight*.5+"px");
			}
		});
	}
	function getcarouselLinks(){
		$( ".carousel.links" ).each(function( index ) {
			counterCopy = "";
			firstBg = $(this).find(".slide.active").data("bg");
			numSlides = $(this).find(".slide").length;
			firstCopy = $(this).find(".slide.active").children(".copy").html();
			$(this).find(".bg.on").css("background-image","url("+firstBg+")");
			$(this).find(".copybox").children(".copy").html(firstCopy);
			if ($(this).hasClass("wentworth")){
				counterCopy = "1/"+numSlides;
			}
			if ($(this).hasClass("grain")){
				counterCopy = "01";
			}
			if ($(this).hasClass("wellpet")){
				counterCopy = "1/"+numSlides;
			}
			$(this).find(".copybox").children(".counter").text(counterCopy);
		});
	}
	function getaboutCarousel(){
		maxslideHtml = "";
		slideFirst = 'class="active"';
		$( "#about_carousel .slide" ).each(function( index ) {
			maxslideHtml += '<li '+slideFirst+'><a href="#"></a></li>';
			slideFirst = "";
		});
		$("#about_carousel").append('<hr><ul>'+maxslideHtml+'</ul>');
	}
	function getaboutHeight(){
		maxHeight = 0;
		$("#about_carousel, #about_carousel .slide").css("height","initial");
		$( "#about_carousel .slide" ).each(function( index ) {
			if ($(this).outerHeight() > maxHeight){
				maxHeight = $(this).outerHeight();
			}
		});
		$("#about_carousel .slide").css("height",maxHeight);
		if ($(window).width() < 768) {
			maxPad = 100;
		} else if ($(window).width() < 1200) {
			maxPad = 100;
		} else {
			maxPad = 100;
		}
		$("#about_carousel").css("height",maxHeight + maxPad);
	}
	function getmaxRelated(){
		maxHeight = 0;
		$(".related, .related .post, .related .next").attr("style","");
		$( ".related .post" ).each(function( index ) {
			if ($(this).outerHeight() > maxHeight){
				maxHeight = $(this).outerHeight();
			}
		});
		$(".related, .related .post, .related .next").css("height",maxHeight);
	}
	function getmaxRelated(){
		maxHeight = 0;
		$("#page_blog .posts .post").attr("style","");
		$( "#page_blog .posts .post" ).each(function( index ) {
			if ($(this).outerHeight() > maxHeight){
				maxHeight = $(this).outerHeight();
			}
		});
		$("#page_blog .posts .posts").css("height",maxHeight);
	}
	function getcarouselHeight(){
		$( ".carousel.single, .carousel.double, .carousel.triple").attr("style","");
		$(".carousel.double.projects .slide img").attr("style","");
		$( ".carousel.single, .carousel.triple" ).each(function( index ) {
			maxHeight = 0;
			$this = $(this);
			maxPad = 80;
                       
			if ($this.hasClass("nopad")){maxPad=0;};
			$this.find(".slide" ).each(function( index ) {
				if ($(this).outerHeight() > maxHeight){
					maxHeight = $(this).outerHeight();
				}
                                 if($(this).hasClass('shorter')){
                            maxHeight -= 7;
                        }
			});
			$this.css("height",maxHeight+maxPad);
			if ($this.hasClass("double")){$this.find(".slide" ).css("height",maxHeight+maxPad).addClass("on");}; 
		});
		$( ".carousel.double" ).each(function( index ) {
			maxHeight = 0;
			$this = $(this);
			maxPad = 0;
			$this.find(".slide" ).css("height","initial");
			if ($this.hasClass("nopad")){maxPad=0;};
			$this.find(".slide" ).each(function( index ) {
				if ($(this).outerHeight() > maxHeight){
					maxHeight = $(this).outerHeight();
				}
			});
			if ($(window).width() <= 768) {
				maxPad = 0;
			}
// 			console.log(maxHeight);
			$this.css("height",maxHeight+maxPad);
			if ($this.hasClass("double")){$this.find(".slide" ).css("height",maxHeight+maxPad).addClass("on");}; 
			if ($this.hasClass("projects")){
				$(this).find(".slide").children("img").css("height",maxHeight+maxPad).css("width","auto");
				activeSlide = $this.find(".active").data("slide");
				nextSlide = activeSlide + 1;
				newSlide = nextSlide + 1;
				newnewSlide = newSlide + 1;
				prevSlide = activeSlide - 1;
				prevprevSlide = prevSlide - 1;
				prevprevprevSlide = prevprevSlide - 1;
				if ($(window).width() < 768) {
					changeVar = 32;
					leftTotal = (($( window ).width())*-1) + (changeVar*2);
					widthSlide = ($( window ).width()) - (changeVar);
					leftPos = $( window ).width()-changeVar;
				} else {
					changeVar = 64;
					leftTotal = (($( window ).width()/2)*-1) + (changeVar*2);
					widthSlide = ($( window ).width()/2) - (changeVar);
					leftPos = $( window ).width()-changeVar;
				}
				if ($(window).width() < 768){
					halfWidth = ($( window ).width()) - (changeVar);
				} else {
					halfWidth = "50%";
				}
				if (activeSlide == 1){
					$(this).find("[data-slide='" + activeSlide + "']").css("width",halfWidth).css("left","0%");
					$(this).find("[data-slide='" + nextSlide + "']").css("width",widthSlide).css("left",halfWidth);
					$(this).find("[data-slide='" + newSlide + "']").css("width",widthSlide).css("left",leftPos);
					$(this).find("[data-slide='" + newnewSlide + "']").css("width",widthSlide).css("left","100%");
				} else if (activeSlide == $(this).find(".slide").length){
					$(this).find("[data-slide='" + activeSlide + "']").css("width",halfWidth).css("left",halfWidth);
					$(this).find("[data-slide='" + prevSlide + "']").css("width",widthSlide).css("left",changeVar);
					$(this).find("[data-slide='" + prevprevSlide + "']").css("width",widthSlide).css("left",leftPos*-1);
					$(this).find("[data-slide='" + prevprevprevSlide + "']").css("width",widthSlide).css("left","-100%");
				} else {
					$(this).find("[data-slide='" + activeSlide + "']").css("width",widthSlide).css("left",changeVar);
					$(this).find("[data-slide='" + nextSlide + "']").css("width",widthSlide).css("left",halfWidth);
					$(this).find("[data-slide='" + newSlide + "']").css("width",widthSlide).css("left",leftPos);
					$(this).find("[data-slide='" + newnewSlide + "']").css("width",widthSlide).css("left","100%");
					$(this).find("[data-slide='" + prevSlide + "']").css("width",widthSlide).css("left",(widthSlide*-1)+changeVar);
					$(this).find("[data-slide='" + prevprevSlide + "']").css("width",widthSlide).css("left","-100%");
				}
			}
		});
	}
	function getbarChart(){
		numBars = $(".bars .bar").length;
		maxCount = 100;
		valueAdd = 0;
		maxVal = 45;
		for(x=0;x<numBars;x++){
			possibleNumber = 100 - maxCount;
			if (maxCount < maxVal){
				maxVal = maxCount;
			}
			theNumber = Math.floor((Math.random() * maxVal) + 1);
// 			console.log(maxCount);
			if (x == numBars - 1){
				theNumber = maxCount;
				maxCount = 0;
			} else {
				maxCount = maxCount - theNumber;
				valueAdd = valueAdd + theNumber;
			}
// 			console.log(valueAdd);
			$(".bars .bar").eq(x).find(".elem span").text(theNumber+"%");
			$(".bars .bar").eq(x).find(".elem").animate({
				height: theNumber+"%"
			}, 500, function() {
			});
		}
		setTimeout(function(){ getbarChart(); }, 6000);
	}
	//DOUBLE ONESIDE START//
	carouselPlay = true;
	$("body").on("click",".carousel.double.oneside .next",function(e) {
		e.preventDefault();
		if (carouselPlay == true){
			carouselPlay = false;
			thisCarousel = $(this).parent(".carousel");
			activeSlide = parseInt(thisCarousel.find(".active").data("slide"));
			nextSlide = activeSlide + 1;
			newSlide = nextSlide + 1;
			if ($(window).width() < 768) {
				changeVar = 32;
			} else {
				changeVar = 64;
			}
			leftTotal = ($( window ).width() - (changeVar*2)) * -1;
			widthSlide = $( window ).width() - (changeVar*2);
			if (newSlide > thisCarousel.children(".slide").length){
				newSlide = false;
				widthSlide = $( window ).width() - changeVar;
				$(this).hide();
				$(this).siblings(".back").show();
			}
			if (thisCarousel.hasClass("contact")){
				$(this).siblings(".back").show();
			}
			thisCarousel.find(".active").addClass("outgoing");
			thisCarousel.find("[data-slide='" + nextSlide + "']").addClass("stage");
			if (newSlide !== false){
				thisCarousel.find("[data-slide='" + newSlide + "']").addClass("incoming").css("left","100%");
			}
			thisCarousel.find(".outgoing").animate({
				left: leftTotal+"px"
			}, 200, function() {
				thisCarousel.find(".outgoing").css("left",leftTotal+"px").removeClass("active").removeClass("outgoing");
			});
			thisCarousel.find(".stage").animate({
				left: changeVar+"px"
			}, 200, function() {
				thisCarousel.find(".stage").addClass("active").removeClass("nextslide").removeClass("stage");
				carouselPlay = true;
			});
			if (newSlide !== false){
				incomingMove = $( window ).width() - changeVar;
				thisCarousel.find(".incoming").animate({
					left: incomingMove+"px"
				}, 200, function() {
					thisCarousel.find(".incoming").addClass("nextslide").removeClass("incoming");
				});
			}
		}
	});
	$("body").on("click",".carousel.double.oneside .back",function(e) {
		e.preventDefault();
		if (carouselPlay == true){
			carouselPlay = false;
			thisCarousel = $(this).parent(".carousel");
			activeSlide = parseInt(thisCarousel.find(".active").data("slide"));
			nextSlide = activeSlide - 1;
			newSlide = nextSlide - 1;
			slideoffSlide = activeSlide + 1;
			if ($(window).width() < 768) {
				changeVar = 32;
			} else {
				changeVar = 64;
			}
			leftTotal = changeVar;
			leftPos = ($( window ).width() - (changeVar*2)) * -1;
			widthSlide = $( window ).width() - (changeVar);
			if (newSlide < 1){
				newSlide = false;
				widthSlide = $( window ).width() - changeVar;
				$(this).hide();
				$(this).siblings(".next").show();
				leftTotal = 0;
			}
			if (thisCarousel.hasClass("contact")){
				$(this).siblings(".next").show();
			}
			thisCarousel.find(".active").addClass("outgoing");
			thisCarousel.find("[data-slide='" + nextSlide + "']").css("left",leftPos).addClass("stage");
			if (newSlide !== false){
				thisCarousel.find("[data-slide='" + newSlide + "']").addClass("incoming").css("left","-100%");
			}
			thisCarousel.find(".outgoing").animate({
				left: widthSlide+"px"
			}, 200, function() {
				thisCarousel.find(".outgoing").css("left",widthSlide+"px").removeClass("active").addClass("nextslide").removeClass("outgoing");
			});
			thisCarousel.find("[data-slide='" + slideoffSlide + "']").animate({
				left: "100%"
			}, 200, function() {
				thisCarousel.find(".nextslide").removeClass("nextslide");
			});
			thisCarousel.find(".stage").animate({
				left: leftTotal
			}, 200, function() {
				thisCarousel.find(".stage").css("left",leftTotal+"px").addClass("active").removeClass("nextslide").removeClass("stage");
				carouselPlay = true;
			});
			if (newSlide !== false){
				incomingMove = ($( window ).width() - (changeVar*2))*-1;
				thisCarousel.find(".incoming").animate({
					left: incomingMove+"px"
				}, 200, function() {
					thisCarousel.find(".incoming").removeClass("incoming");
				});
			}
		}
	});
	//DOUBLE ONESIDE END//
	//DOUBLE TWOSIDE START//
	$("body").on("click",".carousel.double.twoside .next",function(e) {
		e.preventDefault();
		if (carouselPlay == true){
			carouselPlay = false;
			thisCarousel = $(this).parent(".carousel");
			activeSlide = parseInt(thisCarousel.find(".active").data("slide"));
			nextactiveSlide = activeSlide + 1;
			nextSlide = nextactiveSlide + 1;
			newSlide = nextSlide + 1;
			prevSlide = activeSlide-1;
			if ($(window).width() < 768) {
				changeVar = 32;
				stageVar = ($( window ).width()) - (changeVar);
				leftTotal = (($( window ).width())*-1) + (changeVar*2);
				widthSlide = ($( window ).width()) - (changeVar);
				if (nextactiveSlide == thisCarousel.children(".slide").length){
					newSlide = false;
					widthSlide = $( window ).width();
					$(this).hide();
					$(this).siblings(".next").hide();
				}
			} else {
				changeVar = 64;
				stageVar = ($( window ).width()/2);
				leftTotal = (($( window ).width()/2)*-1) + (changeVar*2);
				widthSlide = ($( window ).width()/2) - (changeVar);
				if (nextSlide == thisCarousel.children(".slide").length){
					newSlide = false;
					widthSlide = $( window ).width()/2;
					$(this).hide();
					$(this).siblings(".next").hide();
				}
			}
			$(this).siblings(".back").show();
			thisCarousel.find(".top").removeClass("top");
			thisCarousel.find(".active").addClass("outgoing");
			thisCarousel.find("[data-slide='" + nextactiveSlide + "']").css("left",stageVar).addClass("nextup");
			thisCarousel.find("[data-slide='" + nextSlide + "']").css("width",widthSlide).addClass("stage");
			if (newSlide !== false){
				thisCarousel.find("[data-slide='" + newSlide + "']").addClass("incoming").css("left","100%");
			}
			if (activeSlide !== 1){
				
			}
			thisCarousel.find(".outgoing").animate({
				left: leftTotal+"px"
			}, 200, function() {
				thisCarousel.find(".outgoing").removeClass("active").removeClass("outgoing");
			});
			thisCarousel.find(".nextup").animate({
				left: changeVar+"px"
			}, 200, function() {
				thisCarousel.find(".nextup").addClass("active").removeClass("nextslide").removeClass("nextup");
				carouselPlay = true;
			});
			thisCarousel.find(".stage").css("width",widthSlide).animate({
				left: stageVar
			}, 200, function() {
				thisCarousel.find(".stage").addClass("nextslide").removeClass("stage");
				carouselPlay = true;
			});
			if (prevSlide > 0){
				thisCarousel.find("[data-slide='" + prevSlide + "']").animate({
					left: "-100%"
				}, 200, function() {
					carouselPlay = true;
				});
			}
			if (newSlide !== false){
				if ($(window).width() < 768) {
					incomingMove = $( window ).width();
				} else {
					incomingMove = $( window ).width() - changeVar;
				}
				thisCarousel.find(".incoming").animate({
					left: incomingMove+"px"
				}, 200, function() {
					thisCarousel.find(".incoming").removeClass("nextslide").removeClass("incoming");
				});
			}
		}
	});
	$("body").on("click",".carousel.double.twoside .back",function(e) {
		e.preventDefault();
		if (carouselPlay == true){
			carouselPlay = false;
			thisCarousel = $(this).parent(".carousel");
			activeSlide = parseInt(thisCarousel.find(".active").data("slide"));
			nextactiveSlide = activeSlide - 1;
			nextSlide = nextactiveSlide - 1;
			newSlide = activeSlide + 1;
			offSlide = newSlide + 1;
			if ($(window).width() < 768) {
				changeVar = 32;
				stageVar = ($( window ).width()) - (changeVar);
				leftTotal = (($( window ).width())*-1) + (changeVar*2);
				widthSlide = ($( window ).width()) - (changeVar);
			} else {
				changeVar = 64;
				stageVar = ($( window ).width()/2);
				leftTotal = (($( window ).width()/2)*-1) + (changeVar*3);
				widthSlide = ($( window ).width()/2) - (changeVar);
			}
			$(this).siblings(".next").show();
			if (activeSlide == 1){
				newSlide = false;
				if ($(window).width() < 768) {
					widthSlide = $( window ).width();
				} else {
					widthSlide = $( window ).width()/2;
				}
				$(this).hide();
				$(this).siblings(".back").hide();
			}
			thisCarousel.find(".active").addClass("outgoing");
			thisCarousel.find("[data-slide='" + nextactiveSlide + "']").css("left",leftTotal).addClass("nextup");
			thisCarousel.find(".top").removeClass("top");
			thisCarousel.find("[data-slide='" + nextSlide + "']").css("width",widthSlide).addClass("stage");
			if (newSlide !== false){
				thisCarousel.find("[data-slide='" + newSlide + "']").addClass("incoming").css("left",stageVar).addClass("top");
			}
			if (nextactiveSlide == 1){
				thisCarousel.find("[data-slide='" + nextactiveSlide + "']").css("width",stageVar);
				$(this).hide();
				$(this).siblings(".back").hide();
				changeVar = 0;
			}
			thisCarousel.find(".outgoing").animate({
				left: stageVar
			}, 200, function() {
				thisCarousel.find(".outgoing").removeClass("active").removeClass("outgoing");
			});
			thisCarousel.find(".nextup").animate({
				left: changeVar+"px"
			}, 200, function() {
				thisCarousel.find(".nextup").addClass("active").removeClass("nextslide").removeClass("nextup");
				carouselPlay = true;
			});
			thisCarousel.find(".stage").css("width",widthSlide).animate({
				left: leftTotal
			}, 200, function() {
				thisCarousel.find(".stage").addClass("nextslide").removeClass("stage");
				carouselPlay = true;
			});
/*
			if (offSlide <= thisCarousel.find(".slide").length){
				thisCarousel.find("[data-slide='" + offSlide + "']").animate({
					left: "100%"
				}, 200, function() {
					carouselPlay = true;
				});
			}
*/
			if (newSlide !== false){
				if ($(window).width() < 768) {
					incomingMove = $( window ).width();
				} else {
					incomingMove = $( window ).width() - changeVar;
				}
				thisCarousel.find(".incoming").animate({
					left: incomingMove+"px"
				}, 200, function() {
					thisCarousel.find(".incoming").removeClass("nextslide").removeClass("incoming");
				});
			}
/*
			if (offSlide <= thisCarousel.find(".slide").length){
				thisCarousel.find("[data-slide='" + offSlide + "']").animate({
					left: "100%"
				}, 200, function() {
					carouselPlay = true;
				});
			}
*/
		}
	});
	//DOUBLE TWOSIDE END//
	$("body").on("mouseover",".carousel.triple.goodwill .next, carousel.triple.goodwill .next svg, carousel.triple.goodwill .next svg path",function(e) {
		thisCarousel = $(this).parent(".carousel");
		activeId = parseInt(thisCarousel.find(".active").data("slide")) + 1;
		if (activeId > $(".carousel.triple.goodwill .slide").length){
			activeId = 1;
		}
		thisCarousel.find("[data-slide='"+activeId+"']").children("img").animate({
			left: -20
		}, 100, function() {
		});
	});
	$("body").on("mouseleave",".carousel.triple.goodwill .next, carousel.triple.goodwill .next svg, carousel.triple.goodwill .next svg path",function(e) {
		thisCarousel = $(this).parent(".carousel");
		activeId = parseInt(thisCarousel.find(".active").data("slide")) + 1;
		if (activeId > $(".carousel.triple.goodwill .slide").length){
			activeId = 1;
		}
		thisCarousel.find("[data-slide='"+activeId+"']").children("img").animate({
			left: 0
		}, 100, function() {
		});
	});
	$("body").on("mouseover",".carousel.triple.goodwill .back, carousel.triple.goodwill .back svg, carousel.triple.goodwill .back svg path",function(e) {
		thisCarousel = $(this).parent(".carousel");
		activeId = parseInt(thisCarousel.find(".active").data("slide")) - 1;
		if (activeId < 1){
			activeId = $(".carousel.triple.goodwill .slide").length;
		}
		thisCarousel.find("[data-slide='"+activeId+"']").children("img").animate({
			left: 20
		}, 100, function() {
		});
	});
	$("body").on("mouseleave",".carousel.triple.goodwill .back, carousel.triple.goodwill .back svg, carousel.triple.goodwill .back svg path",function(e) {
		thisCarousel = $(this).parent(".carousel");
		activeId = parseInt(thisCarousel.find(".active").data("slide")) - 1;
		if (activeId < 1){
			activeId = $(".carousel.triple.goodwill .slide").length;
		}
		thisCarousel.find("[data-slide='"+activeId+"']").children("img").animate({
			left: 0
		}, 100, function() {
		});
	});
	
	//WELLPET HOVER
	$("body").on("mouseover",".carousel.triple.wellpet .next, carousel.triple.wellpet .next svg, carousel.triple.wellpet .next svg path",function(e) {
		thisCarousel = $(this).parent(".carousel");
		activeId = parseInt(thisCarousel.find(".active").data("slide")) + 1;
		if (activeId > $(".carousel.triple.wellpet .slide").length){
			activeId = 1;
		}
		thisCarousel.find("[data-slide='"+activeId+"']").children("img").animate({
			left: -20
		}, 100, function() {
		});
	});
	$("body").on("mouseleave",".carousel.triple.wellpet .next, carousel.triple.wellpet .next svg, carousel.triple.wellpet .next svg path",function(e) {
		thisCarousel = $(this).parent(".carousel");
		activeId = parseInt(thisCarousel.find(".active").data("slide")) + 1;
		if (activeId > $(".carousel.triple.wellpet .slide").length){
			activeId = 1;
		}
		thisCarousel.find("[data-slide='"+activeId+"']").children("img").animate({
			left: 0
		}, 100, function() {
		});
	});
	$("body").on("mouseover",".carousel.triple.wellpet .back, carousel.triple.wellpet .back svg, carousel.triple.wellpet .back svg path",function(e) {
		thisCarousel = $(this).parent(".carousel");
		activeId = parseInt(thisCarousel.find(".active").data("slide")) - 1;
		if (activeId < 1){
			activeId = $(".carousel.triple.wellpet .slide").length;
		}
		thisCarousel.find("[data-slide='"+activeId+"']").children("img").animate({
			left: 20
		}, 100, function() {
		});
	});
	$("body").on("mouseleave",".carousel.triple.wellpet .back, carousel.triple.wellpet .back svg, carousel.triple.wellpet .back svg path",function(e) {
		thisCarousel = $(this).parent(".carousel");
		activeId = parseInt(thisCarousel.find(".active").data("slide")) - 1;
		if (activeId < 1){
			activeId = $(".carousel.triple.wellpet .slide").length;
		}
		thisCarousel.find("[data-slide='"+activeId+"']").children("img").animate({
			left: 0
		}, 100, function() {
		});
	});
	
	$("body").on("mouseover",".carousel.triple.wentworth.withcopy .next, carousel.triple.wentworth.withcopy svg, carousel.triple.wentworth.withcopy svg path",function(e) {
		thisCarousel = $(this).parent(".carousel");
		activeId = parseInt(thisCarousel.find(".active").data("slide")) + 1;
		if (activeId > $(".carousel.triple.wentworth.withcopy").length){
			activeId = 1;
		}
		thisCarousel.find("[data-slide='2']").children("img").animate({
			left: -20
		}, 100, function() {
		});
	});
	$("body").on("mouseleave",".carousel.triple.wentworth.withcopy .next, carousel.triple.wentworth.withcopy .next svg, carousel.triple.wentworth.withcopy .next svg path",function(e) {
		thisCarousel = $(this).parent(".carousel");
		activeId = thisCarousel.find(".active").data("slide");
		if (activeId > $(".carousel.triple.wentworth.withcopy").length){
			activeId = 1;
		}
		thisCarousel.find("[data-slide='2']").children("img").animate({
			left: 0
		}, 100, function() {
		});
	});
	$("body").on("mouseover",".carousel.triple.wentworth.withcopy .back, carousel.triple.wentworth.withcopy .back svg, carousel.triple.wentworth.withcopy .back svg path",function(e) {
		thisCarousel = $(this).parent(".carousel");
		activeId = parseInt(thisCarousel.find(".active").data("slide")) + 1;
		if (activeId > $(".carousel.triple.wentworth.withcopy").length){
			activeId = 1;
		}
		if (activeId < 1){
			activeId = $(".carousel.triple.wentworth.withcopy").length;
		}
		thisCarousel.find("[data-slide='1']").children("img").animate({
			left: 20
		}, 100, function() {
		});
	});
	$("body").on("mouseleave",".carousel.triple.wentworth.withcopy .back, carousel.triple.wentworth.withcopy .back svg, carousel.triple.wentworth.withcopy .back svg path",function(e) {
		thisCarousel = $(this).parent(".carousel");
		activeId = thisCarousel.find(".active").data("slide");
		if (activeId > $(".carousel.triple.wentworth.withcopy").length){
			activeId = 1;
		}
		thisCarousel.find("[data-slide='1']").children("img").animate({
			left: 0
		}, 100, function() {
		});
	});
	$("body").on("mouseleave",".carousel.links.inert .next, .carousel.links.inert .next svg, .carousel.links.inert .next svg path, .carousel.links.wentworth .next, .carousel.links.wentworth .next svg, .carousel.links.wentworth .next svg path, .carousel.links.wellpet .next, .carousel.links.wellpet .next svg, .carousel.links.wellpet .next svg path",function(e) {
		thisCarousel = $(this).parent(".carousel");
		thisCarousel.find(".nextslide").children("img").animate({
			right: 0
		}, 100, function() {
		});
	});
	$("body").on("mouseover",".carousel.links.inert .next, .carousel.links.inert .next svg, .carousel.links.inert .next svg path, .carousel.links.wentworth .next, .carousel.links.wentworth .next svg, .carousel.links.wentworth .next svg path, .carousel.links.grain .next, .carousel.links.grain .next svg, .carousel.links.grain .next svg path, .carousel.links.wellpet .next, .carousel.links.wellpet .next svg, .carousel.links.wellpet .next svg path",function(e) {
		thisCarousel = $(this).parent(".carousel");
		thisCarousel.find(".nextslide").children("img").animate({
			right: 20
		}, 100, function() {
		});
	});
	$("body").on("mouseleave",".carousel.links.inert .next, .carousel.links.inert .next svg, .carousel.links.inert .next svg path, .carousel.links.wentworth .next, .carousel.links.wentworth .next svg, .carousel.links.wentworth .next svg path, .carousel.links.grain .next, .carousel.links.grain .next svg, .carousel.links.grain .next svg path, .carousel.links.wellpet .next, .carousel.links.wellpet .next svg, .carousel.links.wellpet .next svg path",function(e) {
		thisCarousel = $(this).parent(".carousel");
		thisCarousel.find(".nextslide").children("img").animate({
			right: 0
		}, 100, function() {
		});
	});
	$("body").on("mouseover",".carousel.links.inert .back, .carousel.links.inert .back svg, .carousel.links.inert .back svg path",function(e) {
		thisCarousel = $(this).parent(".carousel");
		thisCarousel.find(".offstage").children("img").animate({
			left: 20
		}, 100, function() {
		});
	});
	$("body").on("mouseleave",".carousel.links.inert .back, .carousel.links.inert .back svg, .carousel.links.inert .back svg path",function(e) {
		thisCarousel = $(this).parent(".carousel");
		thisCarousel.find(".offstage").children("img").animate({
			left: 0
		}, 100, function() {
		});
	});
	$("body").on("click",".person, .carousel.contact a",function(e) {
		e.preventDefault();
	});
	$("body").on("click",".carousel.links .next",function(e) {
		e.preventDefault();
		if (carouselPlay == true){
			carouselPlay = false;
			thisCarousel = $(this).parent(".carousel");
			activeSlide = thisCarousel.find(".slide.active").data("slide");
			nextSlide = thisCarousel.find(".slide.nextslide").data("slide");
			newSlide = nextSlide + 1;
			if (newSlide > thisCarousel.find(".slide").length){
				newSlide = 1;
				if (thisCarousel.hasClass("inert")){
					newSlide = 99999;	
				}
			}
			$(".carousel.links.inert").find(".back").show();
			newBg = thisCarousel.find(".slide.nextslide").data("bg");
			newCopy = thisCarousel.find(".slide.nextslide").find(".copy").html();
			thisCarousel.find(".bg.fade").addClass("bggo").css("background-image","url("+newBg+")").show();
			thisCarousel.find(".bg.on").addClass("bgoff");
			thisCarousel.find(".active").addClass("outgoing");
			thisCarousel.find(".nextslide").addClass("stage");
			farPos = 150;
			if (thisCarousel.hasClass("wentworth")){
				initialState = "200%";
				outgoingPos = "-50%";
				stagePos = "50%";
				incomingPos = "118%";
				if ($(window).width() < 768) {
					incomingPos = "140%";
				} else if ($(window).width() < 968) {
					incomingPos = "140%";
					stagePos = "60%";
				} else if ($(window).width() < 1200) {
					incomingPos = "140%";
					stagePos = "50%";
				} else if ($(window).width() < 1500) {
					incomingPos = "127%";
					stagePos = "55%";
				}
			}
			if (thisCarousel.hasClass("inert")){
				initialState = "143%";
				outgoingPos = "-165%";
				stagePos = "0%";
				incomingPos = "93%";
				farPos = -165;
				if ($(window).width() <= 768) {
					incomingPos = "140%";
				} else if ($(window).width() < 968) {
					incomingPos = "140%";
				}
				if (nextSlide == 4){
					thisCarousel.find(".next").hide();
				}
			}
			if (thisCarousel.hasClass("grain")){
				initialState = "200%";
				outgoingPos = "-50%";
				stagePos = "54%";
				incomingPos = "125%";
				if ($(window).width() <= 768) {
					incomingPos = "140%";
					stagePos = "50%";
				} else if ($(window).width() < 1200) {
					incomingPos = "120%";
				}
			}
			if (thisCarousel.hasClass("wellpet")){
				initialState = "50%";
				outgoingPos = "-50%";
				stagePos = "50%";
				incomingPos = "118%";
				if ($(window).width() < 768) {
					incomingPos = "140%";
					initialState = "50%";
				} else if ($(window).width() < 968) {
					incomingPos = "140%";
					stagePos = "60%";
					initialState = "60%";
				} else if ($(window).width() < 1200) {
					incomingPos = "140%";
					stagePos = "50%";
				} else if ($(window).width() < 1500) {
					incomingPos = "127%";
					stagePos = "55%";
					initialState = "55%";
				}
			}
			thisCarousel.find("[data-slide='" + newSlide + "']").addClass("incoming").css("left",initialState);
			thisCarousel.find(".copybox").children(".copy").html(newCopy);
			numSlides = thisCarousel.find(".slide").length;
			if (thisCarousel.hasClass("wentworth")){
				slidecounterCopy = nextSlide+"/"+numSlides;
			}
			if (thisCarousel.hasClass("grain")){
				slidecounterCopy = "0"+nextSlide;
			}
			if (thisCarousel.hasClass("inert")){
				slidecounterCopy = '';
			}
			if (thisCarousel.hasClass("wellpet")){
				slidecounterCopy = nextSlide+"/"+numSlides;
			}
			thisCarousel.find(".copybox").children(".counter").text(slidecounterCopy);
			thisCarousel.find(".bg.bggo").animate({
				opacity: "1"
			}, 250, function() {
				thisCarousel.find(".bg.bggo").addClass("on").removeClass("fade").removeClass("bggo");
				thisCarousel.find(".bg.bgoff").attr("style","").addClass("fade").removeClass("on").removeClass("bgoff");
			});
			thisCarousel.find(".outgoing").animate({
				left: outgoingPos
			}, 250, function() {
				thisCarousel.find(".offstage").removeClass("offstage");
				thisCarousel.find(".outgoing").css("left",farPos+"%").removeClass("active").removeClass("outgoing").addClass("offstage");
			});
			thisCarousel.find(".stage").children("img").css("right",0);
			thisCarousel.find(".stage").animate({
				left: stagePos
			}, 200, function() {
				thisCarousel.find(".stage").addClass("active").removeClass("nextslide").removeClass("stage");
			});
			setTimeout(function(){ 
				thisCarousel.find(".incoming").animate({
					left: incomingPos
				}, 500, function() {
					thisCarousel.find(".incoming").addClass("nextslide").removeClass("incoming");
					carouselPlay = true;
				});
			}, 100);
			if (thisCarousel.hasClass("inert")){
				thisCarousel.find(".offstage").animate({
					left: "-260%"
				}, 250, function() {
					
				});
			}
		}
	});
	$("body").on("click",".carousel.links.inert .back",function(e) {
		e.preventDefault();
		if (carouselPlay == true){
			carouselPlay = false;
			thisCarousel = $(this).parent(".carousel");
			activeSlide = thisCarousel.find(".slide.active").data("slide");
			nextSlide = thisCarousel.find(".slide.offstage").data("slide");
			newSlide = nextSlide - 1;
			if (newSlide < 1){
				moreSlides = false;
				$(".carousel.links.inert").find(".back").show();
			}
			$(".carousel.links.inert").find(".next").show();
			newBg = thisCarousel.find(".slide.nextslide").data("bg");
			newCopy = thisCarousel.find(".slide.nextslide").find(".copy").html();
			thisCarousel.find(".bg.fade").addClass("bggo").css("background-image","url("+newBg+")").show();
			thisCarousel.find(".bg.on").addClass("bgoff");
			thisCarousel.find(".active").addClass("outgoing");
			thisCarousel.find(".nextslide").addClass("stage");
			farPos = 150;
			if (thisCarousel.hasClass("inert")){
				initialState = "143%";
				outgoingPos = "-165%";
				stagePos = "0%";
				incomingPos = "93%";
				farPos = "165%";
				if ($(window).width() <= 768) {
					incomingPos = "140%";
				} else if ($(window).width() < 968) {
					incomingPos = "140%";
				}
			}
			if (newSlide > 0){
				thisCarousel.find("[data-slide='" + newSlide + "']").addClass("incoming").css("left","-260%");
			} else {
				thisCarousel.find(".back").hide();
			}
			thisCarousel.find(".copybox").children(".copy").html(newCopy);
			numSlides = thisCarousel.find(".slide").length;
			if (thisCarousel.hasClass("wentworth")){
				slidecounterCopy = nextSlide+"/"+numSlides;
			}
			if (thisCarousel.hasClass("grain")){
				slidecounterCopy = "0"+nextSlide;
			}
			if (thisCarousel.hasClass("inert")){
				slidecounterCopy = '';
			}
			thisCarousel.find(".copybox").children(".counter").text(slidecounterCopy);
			thisCarousel.find(".bg.bggo").animate({
				opacity: "1"
			}, 250, function() {
				thisCarousel.find(".bg.bggo").addClass("on").removeClass("fade").removeClass("bggo");
				thisCarousel.find(".bg.bgoff").attr("style","").addClass("fade").removeClass("on").removeClass("bgoff");
			});
			thisCarousel.find(".outgoing").animate({
				left: incomingPos
			}, 250, function() {
				thisCarousel.find(".outgoing").css("left",incomingPos+"%").removeClass("active").removeClass("nextslide").removeClass("offstage").addClass("nextslide").removeClass("outgoing");
				carouselPlay = true;
			});
			thisCarousel.find(".offstage").children("img").css("right",0);
			thisCarousel.find(".stage").animate({
				left: farPos
			}, 200, function() {
				thisCarousel.find(".stage").css("left",farPos+"%").removeClass("active").removeClass("nextslide").removeClass("offstage").removeClass("stage");
			});
			thisCarousel.find(".offstage").animate({
				left: 0
			}, 200, function() {
				thisCarousel.find(".offstage").css("left",0).removeClass("active").removeClass("nextslide").removeClass("stage").addClass("active").removeClass("offstage");
			});
			if (newSlide > 0){
				setTimeout(function(){ 
					thisCarousel.find("[data-slide='" + newSlide + "']").animate({
						left: outgoingPos
					}, 500, function() {
						thisCarousel.find(".offstage").removeClass("offstage");
						thisCarousel.find(".incoming").addClass("offstage").removeClass("active").removeClass("nextslide").css("left",outgoingPos).removeClass("incoming");
					});
				}, 100);
			}
		}
	});
	$("body").on("click",".carousel.triple.wentworth.withcopy .btn",function(e) {
		e.preventDefault();
		thisCarousel = $(this).parent(".carousel");
		activeSlide = thisCarousel.find(".slide.active").data("slide");
		maxSlides = thisCarousel.find(".slide").length;
		if ($(this).hasClass("next")){
			incomingDefault = "74%";
			leavingDefault = "-74%";
			standbyDefault = "138%";
			offstageDefault = "-220%";
			thisCarousel.find("[data-slide='1']").addClass("scale").animate({
				left: "-74%"
			}, 200, function() {
				thisCarousel.find("[data-slide='1']").addClass("nextslide").removeClass("active");
			});
			thisCarousel.find("[data-slide='2']").removeClass("scale").animate({
				left: "0"
			}, 200, function() {
				thisCarousel.find("[data-slide='2']").addClass("active").removeClass("nextslide");
			});
			thisCarousel.find(".back").show();
			thisCarousel.find(".next").hide();
		}
		if ($(this).hasClass("back")){
			thisCarousel.find("[data-slide='1']").removeClass("scale").animate({
				left: "0"
			}, 200, function() {
				thisCarousel.find("[data-slide='1']").addClass("active").removeClass("nextslide");
			});
			thisCarousel.find("[data-slide='2']").addClass("scale").animate({
				left: "74%"
			}, 200, function() {
				thisCarousel.find("[data-slide='2']").addClass("nextslide").removeClass("active");
			});
			thisCarousel.find(".back").hide();
			thisCarousel.find(".next").show();
		}
// 		alert("Next is "+nextSlide+".  Previous is "+prevSlide+".  Slide After is "+slideAfter+".");

	});
	$("body").on("click",".carousel.triple.goodwill .btn",function(e) {
		e.preventDefault();
		if (carouselPlay == true){
			carouselPlay = false;
			thisCarousel = $(this).parent(".carousel");
			activeSlide = thisCarousel.find(".slide.active").data("slide");
			maxSlides = thisCarousel.find(".slide").length;
			if ($(this).hasClass("next")){
				thisCarousel.find(".btn.back").show();
				nextSlide = activeSlide + 1;
				prevSlide = activeSlide - 1;
				outgoingPos = "-70%";
				incomingPos = "70%";
				leavingPos = "-150%";
				if ($(window).width() < 768) {
					incomingPos = "92%";
					outgoingPos = "-92%";
				} else if ($(window).width() < 968) {
					incomingPos = "80%";
					outgoingPos = "-80%";
				} else if ($(window).width() < 1200) {
					incomingPos = "70%";
					outgoingPos = "-70%";
				} else {
					incomingPos = "60%";
					outgoingPos = "-60%";
				}
				if (activeSlide == 1){
					nextSlide = 2;
					prevSlide = 3;
					leavingPos = "70%";
					if ($(window).width() < 768) {
						leavingPos = "92%";
					} else if ($(window).width() < 968) {
						leavingPos = "80%";
					} else if ($(window).width() < 1200) {
						leavingPos = "70%";
					} else {
						leavingPos = "60%";
					}
				}
				if (nextSlide > maxSlides){
					return false;
				}
				if (nextSlide == maxSlides){
					thisCarousel.find(".btn.next").hide();
				} else {
					thisCarousel.find(".btn.next").show();
				}
			}
			if ($(this).hasClass("back")){
				thisCarousel.find(".btn.next").show();
				nextSlide = activeSlide - 1;
				prevSlide = activeSlide + 1;
				if ($(window).width() < 768) {
					outgoingPos = "92%";
				} else {
					outgoingPos = "70%";
				}
				incomingPos = "-70%";
				leavingPos = "100%";
				if ($(window).width() < 768) {
					outgoingPos = "92%";
					incomingPos = "-92%";
				} else if ($(window).width() < 968) {
					outgoingPos = "80%";
					incomingPos = "-80%";
				} else if ($(window).width() < 1200) {
					outgoingPos = "70%";
					incomingPos = "-70%";
				} else {
					outgoingPos = "60%";
					incomingPos = "-60%";
				}
				if (activeSlide == maxSlides){
					nextSlide = 2;
					prevSlide = 1;
					leavingPos = "-70%";
					if ($(window).width() < 768) {
						leavingPos = "-92%";
					} else if ($(window).width() < 968) {
						leavingPos = "-80%";
					} else if ($(window).width() < 1200) {
						leavingPos = "-70%";
					} else {
						leavingPos = "-60%";
					}
				}
				if (nextSlide < 1){
					return false;
				}
				if (nextSlide == 1){
					thisCarousel.find(".btn.back").hide();
				} else {
					thisCarousel.find(".btn.back").show();
				}
			}
			if ($(window).width() < 768) {
				centerPos = 19;
			} else {
				centerPos = 0;
			}
			thisCarousel.find("img").css("left","0");
			thisCarousel.find(".slide.active").addClass("outgoing");
			thisCarousel.find("[data-slide='" + nextSlide + "']").addClass("incoming");
			thisCarousel.find("[data-slide='" + prevSlide + "']").addClass("leaving");
			thisCarousel.find(".outgoing").addClass("scale").animate({
				left: outgoingPos
			}, 200, function() {
				thisCarousel.find(".outgoing").css("left",outgoingPos).removeClass("active").removeClass("outgoing");
				carouselPlay = true;
			});
			thisCarousel.find(".incoming").removeClass("scale").animate({
				left: centerPos+"%"
			}, 200, function() {
				thisCarousel.find(".incoming").addClass("active").removeClass("incoming");
				carouselPlay = true;
			});
			thisCarousel.find(".leaving").animate({
				left: leavingPos
			}, 200, function() {
				thisCarousel.find(".leaving").removeClass("leaving");
				carouselPlay = true;
			});
		}
	});
	
	//WELLPET CAROUSEL
	$("body").on("click",".carousel.triple.wellpet .btn",function(e) {
		e.preventDefault();
		if (carouselPlay == true){
			carouselPlay = false;
			thisCarousel = $(this).parent(".carousel");
			activeSlide = thisCarousel.find(".slide.active").data("slide");
			maxSlides = thisCarousel.find(".slide").length;
			if ($(this).hasClass("next")){
				thisCarousel.find(".btn.back").show();
				nextSlide = activeSlide + 1;
				prevSlide = activeSlide - 1;
				outgoingPos = "-70%";
				incomingPos = "70%";
				leavingPos = "-150%";
				if ($(window).width() < 768) {
					incomingPos = "92%";
					outgoingPos = "-57%";
				} else if ($(window).width() < 968) {
					incomingPos = "88%";
					outgoingPos = "-55%";
				} else if ($(window).width() < 1200) {
					incomingPos = "84%";
					outgoingPos = "-55%";
				} else if ($(window).width() < 1500) {
					incomingPos = "90%";
					outgoingPos = "-56%";
				} else {
					incomingPos = "87%";
					outgoingPos = "-50%";
				}
				if (activeSlide == 1){
					nextSlide = 2;
					prevSlide = 3;
					leavingPos = "70%";
					if ($(window).width() < 768) {
						leavingPos = "92%";
					} else if ($(window).width() < 968) {
						leavingPos = "88%";
					} else if ($(window).width() < 1200) {
						leavingPos = "84%";
					} else if ($(window).width() < 1500) {
						leavingPos = "90%";
					} else {
						leavingPos = "87%";
					}
				}
				if (nextSlide > maxSlides){
					return false;
				}
				if (nextSlide == maxSlides){
					thisCarousel.find(".btn.next").hide();
				} else {
					thisCarousel.find(".btn.next").show();
				}
			}
			if ($(this).hasClass("back")){ 
				thisCarousel.find(".btn.next").show();
				nextSlide = activeSlide - 1;
				prevSlide = activeSlide + 1;
				if ($(window).width() < 768) {
					outgoingPos = "92%";
				} else {
					outgoingPos = "70%";
				}
				incomingPos = "-70%";
				leavingPos = "100%";
				if ($(window).width() < 768) {
					outgoingPos = "92%";
					incomingPos = "-57%";
				} else if ($(window).width() < 968) {
					outgoingPos = "88%";
					incomingPos = "-55%";
				} else if ($(window).width() < 1200) {
					outgoingPos = "84%";
					incomingPos = "-55%";
				} else if ($(window).width() < 1500) {
					outgoingPos = "90%";
					incomingPos = "-56%";
				} else {
					outgoingPos = "87%";
					incomingPos = "-50%";
				}
				if (activeSlide == maxSlides){
					nextSlide = 2;
					prevSlide = 1;
					leavingPos = "-70%";
					if ($(window).width() < 768) {
						leavingPos = "-57%";
					} else if ($(window).width() < 968) {
						leavingPos = "-55%";
					} else if ($(window).width() < 1200) {
						leavingPos = "-55%";
					} else if ($(window).width() < 1200) {
						leavingPos = "-56%";
					} else {
						leavingPos = "-50%";
					}
				}
				if (nextSlide < 1){
					return false;
				}
				if (nextSlide == 1){
					thisCarousel.find(".btn.back").hide();
				} else {
					thisCarousel.find(".btn.back").show();
				}
			}
			if ($(window).width() < 768) {
				centerPos = 19;
			} else if ($(window).width() < 968) {
				centerPos = 23;
			} else if ($(window).width() < 1200) {
				centerPos = 23;
			} else {
				centerPos = 23;
			}
			thisCarousel.find("img").css("left","0");
			thisCarousel.find(".slide.active").addClass("outgoing");
			thisCarousel.find("[data-slide='" + nextSlide + "']").addClass("incoming");
			thisCarousel.find("[data-slide='" + prevSlide + "']").addClass("leaving");
			thisCarousel.find(".outgoing").addClass("scale").animate({
				left: outgoingPos
			}, 200, function() {
				thisCarousel.find(".outgoing").css("left",outgoingPos).removeClass("active").removeClass("outgoing");
				carouselPlay = true;
			});
			thisCarousel.find(".incoming").removeClass("scale").animate({
				left: centerPos+"%"
			}, 200, function() {
				thisCarousel.find(".incoming").addClass("active").removeClass("incoming");
				carouselPlay = true;
			});
			thisCarousel.find(".leaving").animate({
				left: leavingPos
			}, 200, function() {
				thisCarousel.find(".leaving").removeClass("leaving");
				carouselPlay = true;
			});
		}
	});
	
	$("body").on("click",".carousel.project.single .btn",function(e) {
		e.preventDefault();
		if (carouselPlay == true){
			carouselPlay = false;
			thisCarousel = $(this).parent(".carousel");
			activeSlide = thisCarousel.find(".slide.active").data("slide");
			maxSlides = thisCarousel.find(".slide").length;
			if ($(this).hasClass("next")){
				nextSlide = activeSlide + 1;
				if (nextSlide > maxSlides){
					nextSlide = 1;
				}
				initialState = "100%";
				outgoingPos = "-100%";
				incomingPos = "0%";
			}
			if ($(this).hasClass("back")){
				nextSlide = activeSlide - 1;
				if (nextSlide < 1){
					nextSlide = maxSlides;
				}
				initialState = "-100%";
				outgoingPos = "100%";
				incomingPos = "0%";
			}
			thisCarousel.find(".slide.active").addClass("outgoing");
			thisCarousel.find("[data-slide='" + nextSlide + "']").addClass("incoming").css("left",initialState);
			thisCarousel.find(".outgoing").animate({
				left: outgoingPos
			}, 250, function() {
				thisCarousel.find(".outgoing").css("left","100%").removeClass("active").removeClass("outgoing");
				carouselPlay = true;
			});
			thisCarousel.find(".incoming").animate({
				left: incomingPos
			}, 200, function() {
				thisCarousel.find(".incoming").addClass("active").removeClass("incoming");
			});
		}
	});
	//IMAGE SCROLL
	var outerContent = $('.img_scroll.wentworth');
    var innerContent = $('.img_scroll.wentworth img');
    outerContent.scrollLeft((innerContent.width() - outerContent.width()) / 2); 
	var b = null;
	if ($(".img_scroll.wentworth")[0]){
		$(".img_scroll.wentworth").parent(".fl-html").addClass("scrollmarker");
	}
	$( '.img_scroll.wentworth img' ).on( 'mousemove', function(e) {
	    var container = $(this).parent();
	    if ((e.pageX - container.offset().left) < container.width() / 2) {
	        var direction = function() {
	            container.animate( {scrollLeft: '-=600'}, 750, 'linear', direction );
	        }
	        if ((b == false) || (b == null)) {
	            b = true;
	            container.stop( true ).animate( {scrollLeft: '-=600'}, 750, 'linear', direction );
	        }
	    } else {
	        var direction = function() {
	            container.animate( {scrollLeft: '+=600'}, 750, 'linear', direction );
	        }
	        if ((b == true) || (b == null)) {
	            b = false;
	            container.stop( true ).animate( {scrollLeft: '+=600'}, 750, 'linear', direction );
	        }
	    }
	}).on ( 'mouseleave', function() {
	    var container = $(this).parent();
	    container.stop( true );
	    b = null;
	});
	//IMAGE SCROLL
	//ABOUT CAROUSEL//
	
	function Timer(fn, t) {
	    var timerObj = setInterval(fn, t);
	
	    this.stop = function() {
	        if (timerObj) {
	            clearInterval(timerObj);
	            timerObj = null;
	        }
	        return this;
	    }
	
	    // start timer using current settings (if it's not already running)
	    this.start = function() {
	        if (!timerObj) {
	            this.stop();
	            timerObj = setInterval(fn, t);
	        }
	        return this;
	    }
	
	    // start with new interval, stop current interval
	    this.reset = function(newT) {
	        t = newT;
	        return this.stop().start();
	    }
	}
	
	if ($("#about_carousel")[0]){
		var timer = new Timer(function() {
		    autoMove();
		}, 6000);
		timer.start();
		function autoMove(){
			currentSlide = $("#about_carousel ul li.active").index();
			nextSlide = currentSlide + 1;
			maxSlides = $("#about_carousel").find(".slide").length;
			if (nextSlide == maxSlides){
				nextSlide = 0;
			}
			$("#about_carousel ul li").removeClass("active");
			$("#about_carousel ul li").eq(nextSlide).addClass("active");
			moveCarousel(nextSlide + 1);
		}
		$("body").on("click","#about_carousel ul li a",function(e) {
			e.preventDefault();
// 			clearInterval(automoveVar);
// 			automoveVar = null;
// 			var automoveVar = setInterval(autoMove, 6000);
			timer.reset(6000);
			nextSlide = $(this).parent("li").index() + 1;
			$("#about_carousel ul li").removeClass("active");
			$(this).parent("li").addClass("active");
			moveCarousel(nextSlide);
// 			var automoveVar = setInterval(autoMove, 6000);
		});
		function moveCarousel(nextSlide){
 			currentSlide = $("#about_carousel").find("slide.active").data("id");
			maxSlides = $("#about_carousel").find(".slide").length;
			if (nextSlide > maxSlides){
				nextSlide = 1;
			}
// 			clearInterval(automoveVar);
			$("#about_carousel").find(".slide.active").addClass("outgoing").css("left","0%");
			$("#about_carousel").find("[data-id='" + nextSlide + "']").addClass("incoming").css("left","100%");
			$("#about_carousel").find(".outgoing").animate({
				left: "-100%"
			}, 250, function() {
				$("#about_carousel").find(".outgoing").css("left","100%").removeClass("active").removeClass("outgoing");
// 				var automoveVar = setInterval(autoMove, 6000);
			});
			$("#about_carousel").find(".incoming").animate({
				left: "0%"
			}, 200, function() {
				$("#about_carousel").find(".incoming").addClass("active").removeClass("incoming");
			});
		}
	}
	//ABOUT CAROUSEL//
	
	//BLOG FILTER//
	function getParameterByName(name, url) {
	    if (!url) url = window.location.href;
	    name = name.replace(/[\[\]]/g, '\\$&');
	    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
	        results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, ' '));
	}
	if ($("body#page_contact")[0]){
		var successVal = getParameterByName('success');
		if (successVal == "true"){
			$(".fl-module-widget.form").remove();
			$(".thanks").show();
		}
	}
	if ($("body#page_rfp")[0]){
		var successVal = getParameterByName('success');
		if (successVal == "true"){
			$(".fl-module-widget.form").remove();
			$(".thanks").show();
		}
	}
	if ($("body#page_blog")[0]){
		var categoryName = getParameterByName('category');
		if (categoryName){
			categoryName = categoryName.replace("%20"," ");
			searchVal = $(this).find("input[type='text']").val().toLowerCase();
			$("#category option[value='"+categoryName+"']").attr('selected', 'selected');
			selectedVal = $( "#categories #category" ).find("option:selected").val().replace(/ /g,'').toLowerCase();
			blogSearch(searchVal,selectedVal);
		}
	}
	$('#page_blog .posts').isotope({
		itemSelector: '.post',
		percentPosition: true
	});
	$( "#search_posts" ).submit(function( event ) {
		event.preventDefault();
		searchVal = $(this).find("input[type='text']").val().toLowerCase();
		selectedVal = $( "#categories #category" ).find("option:selected").val().replace(/ /g,'').toLowerCase();
		blogSearch(searchVal,selectedVal);
/*
		$( "#page_blog .posts .post" ).each(function( index ) {
			thisTitle = $(this).find("p.title").text().replace(/ /g,'').toLowerCase();
// 			var result = thisTitle.indexOf(searchVal) > -1;
			if(thisTitle.indexOf(searchVal) != -1){
					$(this).addClass("active_tile");
					$(this).removeClass("inactive_tile");
				} else {
				  	$(this).removeClass("active_tile");
				  	$(this).addClass("inactive_tile");
			
			}
		});
		$('#page_blog .posts').isotope({ filter: '.active_tile'});
*/
	});
	$("body").on("keyup","#search_posts input[type='text']",function(e) {
		searchVal = $(this).val();
		selectedVal = $( "#categories #category" ).find("option:selected").val().replace(/ /g,'').toLowerCase();
		if (searchVal.length < 1){
			blogSearch(searchVal,selectedVal);
		}
	});
	$( "#categories #category" ).change(function(e) {
		e.preventDefault();
		searchVal = $("#search_posts").find("input[type='text']").val().toLowerCase();
		selectedVal = $( "#categories #category" ).find("option:selected").val().replace(/ /g,'').toLowerCase();
		blogSearch(searchVal,selectedVal);
/*
		if (selectedVal !== 0){
			$( "#page_blog .posts .post" ).each(function( index ) {
				if (!$(this).hasClass("inactive_tile")){
					thisCat = $(this).find("span.cats").text().replace(/ /g,'').toLowerCase();
		// 			var result = thisTitle.indexOf(searchVal) > -1;
					if(thisCat.indexOf(selectedVal) != -1){
						$(this).addClass("active_tile");
						$(this).removeClass("inactive_tile");
					} else {
						$(this).removeClass("active_tile");
						$(this).addClass("inactive_tile");
					}
				}
			});
		} else {
			$( "#page_blog .posts .post" ).each(function( index ) {
				if (!$(this).hasClass("inactive_tile")){
					$(this).removeClass("active_tile");
					$(this).addClass("inactive_tile");
				}
			});
		}
		$('#page_blog .posts').isotope({ filter: '.active_tile'});
*/
	});
	function blogSearch(searchVal, selectedVal){
		$( "#page_blog .posts .post" ).each(function( index ) {
			thisTitle = $(this).find("p.title").text().replace(/ /g,'').toLowerCase();
			thisCat = $(this).find("span.cats").text().replace(/ /g,'').toLowerCase();
			if (searchVal.length>0 && selectedVal !== 0){
				if(thisCat.indexOf(selectedVal) != -1 && thisTitle.indexOf(searchVal) != -1){
					$(this).addClass("active_tile");
					$(this).removeClass("inactive_tile");
				} else {
					$(this).removeClass("active_tile");
					$(this).addClass("inactive_tile");
				}
			}
			if (searchVal.length == 0 && selectedVal !== 0){
				if(thisCat.indexOf(selectedVal) != -1){
					$(this).addClass("active_tile");
					$(this).removeClass("inactive_tile");
				} else {
					$(this).removeClass("active_tile");
					$(this).addClass("inactive_tile");
				}
			}
			if (searchVal.length>0 && selectedVal == 0){
				if(thisTitle.indexOf(searchVal) != -1){
					$(this).addClass("active_tile");
					$(this).removeClass("inactive_tile");
				} else {
					$(this).removeClass("active_tile");
					$(this).addClass("inactive_tile");
				}
			} 
			if (searchVal.length == 0 && selectedVal == 0){
				$(this).addClass("active_tile");
				$(this).removeClass("inactive_tile");
			}
		});
		$('#page_blog .posts').isotope({ filter: '.active_tile'});
	}
/*
	stickyMenu();
	function stickyMenu(){
		headerHeight = $("header").outerHeight();
		$("header").attr("data-height",headerHeight);
		$(".trigger").css("top",headerHeight);
	};
	currentPos = 0;
	unstick = true;
	$(window).scroll(function(){
		var posTop = $(window).scrollTop() - $('.trigger').offset().top;
		if (posTop < currentPos && posTop > $("header").data("height")){
			if (!$("header").hasClass("sticky")){
				unstick = true;
				$("header").addClass("sticky").css("top","-100px");
				$("header").animate({
					top: "0"
				}, 250, function() {
				});
				$(".tostick").addClass("sticky").css("margin-top",$("header").data("height"));
			}
			currentPos = posTop;
		} else {
			if ($("header").hasClass("sticky") && unstick == true){
				$("header").animate({
					top: "-"+$("header").data("height")
				}, 250, function() {
					$("header").css("top",0).removeClass("sticky");
					$(".tostick").css("margin-top",0).removeClass("sticky");
				});
				unstick = false;
			}
			currentPos = posTop;
		}
	});
*/
	// grab an element
	var myElement = document.querySelector("header");
	// construct an instance of Headroom, passing the element
        if(myElement){
	var headroom  = new Headroom(myElement);
	// initialise
	headroom.init(); 
    }
	//BLOG FILTER STOP//
	var d = new Date();
	var n = d.getDay();
	var daysQuotes = ["It’s Sunday Funday—what are you doing here?!","Farewell, sweet weekend. Hello, Monday.","Every day is a gift. Even Tuesday.","Wenzday: We spell it the way it sounds.","Feeling Thursday? Drink it in! Dad-joke fail.","Friday: The original F-word.","Hey. Workweek’s done. But not you."];
	$(".dayofweek h3").text(daysQuotes[n]);
});