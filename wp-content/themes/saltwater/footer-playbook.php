<footer>
	<a class="scroll"><span>SCROLL</span> <img src="/wp-content/themes/saltwater/img/playbook/scrollsign.png" class="main" alt="Scroll" ><img src="/wp-content/themes/saltwater/img/playbook/scrollsign_dark.png" class="alt" alt="Scroll" ></a>
</footer>

<?php wp_footer(); ?>
<script src="https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.1/dist/jquery.fancybox.min.js"></script>
<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/headroom.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/playbook.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/animations.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenLite.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.js"></script>

<script src="https://cdn.jsdelivr.net/parallax.js/1.4.2/parallax.min.js"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/rellax.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js"></script>

</body>
</html>