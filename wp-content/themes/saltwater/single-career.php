<?php
get_header(); 
$currentid = $post->ID;
$prfx_stored_meta = get_post_meta( $post->ID );
while ( have_posts() ) : the_post();
if (!get_avatar_url(get_the_author_meta('ID'))){
	$authorurl = get_stylesheet_directory_uri().'/img/default_person.png';
} else {
// 	$authorurl = get_avatar_url(get_the_author_meta('ID'));
	$authorurl = get_stylesheet_directory_uri().'/img/default_person.png';
}
?>

<div class="tostick">
<div id="hero" class="blog" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'full') ?>)">
	<div class="container">
		<div class="info">
			<span class="date">CURRENT OPENING</span>
<!-- 			<span class="sep">&bull;</span> -->
		</div>
		<h1><?php echo the_title(); ?></h1>
		<div class="careerbyline">
			<p class="desc"><?php echo $prfx_stored_meta['desc'][0]; ?></p>
		</div>
	</div>
</div>
</div>
        <div class="entry-content-post career">
	        
	        <div class="social_bar">
		        <p class="share">Share:</p>
		        <ul class="share">
			        <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" class="fb" target="_blank"><i class="fab fa-facebook-square"></i></a></li>
			        <li><a href="https://twitter.com/home?status=<?php echo get_the_title(); ?>%20-%20Via%20%40SaltwaterCo%20<?php echo get_permalink(); ?>" class="tw" target="_blank"><i class="fab fa-twitter-square"></i></a></li>
			        <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_permalink(); ?>&title=<?php echo get_the_title(); ?>&summary=&source=" class="li" target="_blank"><i class="fab fa-linkedin"></i></a></li>
			        <li><a href="mailto:?&subject=<?php echo get_the_title(); ?> - From Saltwater Co.&body=Check%20this%20out%3A%20<?php echo get_permalink(); ?>" class="em" target="_blank"><i class="far fa-envelope"></i></a></li>
		        </ul>
	        </div>
	        <div class="content">
            	<?php the_content(); ?> <!-- Page Content -->
            	<hr>
	       </div>
	       <div class="social_bar">
		        <p class="share">Share:</p>
		        <ul class="share">
			        <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" class="fb" target="_blank"><i class="fab fa-facebook-square"></i></a></li>
			        <li><a href="https://twitter.com/home?status=<?php echo get_the_title(); ?>%20-%20Via%20%40SaltwaterCo%20<?php echo get_permalink(); ?>" class="tw" target="_blank"><i class="fab fa-twitter-square"></i></a></li>
			        <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_permalink(); ?>&title=<?php echo get_the_title(); ?>&summary=&source=" class="li" target="_blank"><i class="fab fa-linkedin"></i></a></li>
			        <li><a href="mailto:?&subject=<?php echo get_the_title(); ?> - From Saltwater Co.&body=Check%20this%20out%3A%20<?php echo get_permalink(); ?>" class="em" target="_blank"><i class="far fa-envelope"></i></a></li>
		        </ul>
	        </div>
<!-- 	        <?php previous_post_link('%link', 'Previous in category', TRUE, '13'); ?>  -->
        </div><!-- .entry-content-page -->
        
        <div class="row">
        	<hr>
        	
        </div>
        
        <div class="careers_list">
	    <h4>MORE CURRENT OPENINGS</h4>
		   	<ul>
			   	<?php
				  
				$query = new WP_Query(array(
				  'post_type' => 'career',
				  'post_status' => 'publish',
				  'numberposts' => -1
				  // 'order'    => 'ASC'
				));
				while ($query->have_posts()) {
				    $query->the_post();
				    if (get_the_ID() !== $currentid){
				?>
			   	<li><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?><span class="link">See Details</span></a></li>
			    <?php } } wp_reset_query(); ?>
		    </ul>
	    </div>
        
    <?php
    endwhile; //resetting the page loop
    wp_reset_query(); //resetting the page query
    ?>

<?php get_footer(); ?>