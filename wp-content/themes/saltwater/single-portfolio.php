<?php get_header(); ?>
	<?php
		$currentID = get_the_ID();
    while ( have_posts() ) : the_post(); ?>
        <div class="entry-content-page tostick">
            <?php the_content(); ?>
        </div>
        <?php
	        $query = new WP_Query(array(
				'post_type' => 'portfolio',
				'post_status' => 'publish',
				'numberposts' => -1
				// 'order'    => 'ASC'
			));
			if ( $query->have_posts() ){
		?>
		<div class="carousel double twoside nobtn nobg projects">
			<a href="#" class="btn next">
		        <svg version="1.0" xmlns="http://www.w3.org/2000/svg" width="56px" height="72px" viewBox="0 0 560 720" preserveAspectRatio="xMidYMid meet">
					<g id="layer101" fill="#53b4e8" stroke="none">
					<path d="M87 637 c-48 -45 -88 -86 -87 -92 0 -5 42 -49 92 -98 l93 -87 -94 -89 c-58 -55 -92 -95 -88 -103 6 -18 171 -168 183 -168 5 0 92 81 194 180 l185 180 -185 180 c-102 98 -189 179 -195 179 -5 0 -50 -37 -98 -82z"></path>
					</g>
				</svg>
		    </a>
		    <a href="#" class="btn back">
		        <svg version="1.0" xmlns="http://www.w3.org/2000/svg" width="56px" height="72px" viewBox="0 0 560 720" preserveAspectRatio="xMidYMid meet">
					<g id="layer101" fill="#53b4e8" stroke="none">
					<path d="M177 547 c-98 -95 -178 -179 -177 -189 0 -14 338 -348 361 -356 11 -4 178 145 186 166 4 8 -30 48 -88 103 l-94 89 93 87 c50 49 92 93 92 99 0 13 -173 174 -186 174 -5 -1 -89 -78 -187 -173z"></path>
					</g>
				</svg>
		    </a>
		    <?php } $startcount = 1;
			    while ($query->have_posts()) {
				$query->the_post();
				$classslide = "";
				if ($startcount == 1){
					$classslide = "active";
				}
				if ($startcount == 2){
					$classslide = "nextslide";
				}
				$term_list = wp_get_post_terms(get_the_ID(), 'services', array("fields" => "names"));
				if ($currentID !== get_the_ID()){
					echo '<a href="'.get_the_permalink(get_the_ID()).'" class="slide '.$classslide.'" data-slide="'.$startcount.'"><img src="'.get_the_post_thumbnail_url(get_the_ID(),"full").'"><div class="content"><p class="title">'.get_the_title(get_the_ID()).'</p><p class="desc">'.implode(', ', $term_list).'</p></div></a>';
					$startcount++;
				}
		    ?>
		    <?php } ?>
		</div>
		<div class="entry-content-page fl-rich-text">
        <div class="waves projects">
	        <div class="content">
				<h4 style="text-align: center;">Tell us about your project.</h4>
				<p style="text-align: center;">Specifics, hopes, dreams... anything will do. Let's start the conversation and see where we land!</p>
				<div class="button" style="text-align: center;">
					<p><a href="/contact">Let's talk</a></p>
				</div>
		    </div>
	    </div>
    </div>
    <?php
    endwhile;
    wp_reset_query();
    ?>
<?php  get_footer(); ?>