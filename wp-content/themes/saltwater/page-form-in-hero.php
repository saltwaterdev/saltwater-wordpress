<?php
	/*
* Template Name: form in hero
*/
require ("header-other.php");
$currentid = $post->ID;
?>
<div class="tostick">
<div id="hero" class="shorter">
	<h1>Interested in working with us?</h1>
</div>
</div>

	<?php
    while ( have_posts() ) : the_post(); ?>
        <div class="entry-content-page">
            <?php the_content(); ?>
        </div>

    <?php
    endwhile;
    wp_reset_query();
    ?>
<?php  get_footer(); ?>