<?php

class HeaderIconClass extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Header with Icon', 'fl-builder' ),
            'description'     => __( 'Build a block with a header and icon side by side', 'fl-builder' ),
            'group'           => __( 'Content', 'fl-builder' ),
            'category'        => __( 'Content Blocks', 'fl-builder' ),
            'dir'             => SALTWATER_DIR . 'header-icon/',
            'url'             => SALTWATER_URL . 'header-icon/',
            'icon'            => 'button.png',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
}

FLBuilder::register_module( 'HeaderIconClass', array(
    'slide-1'      => array(
        'title'         => __( 'General', 'fl-builder' ),
        'sections'      => array(
            'my-section-1'  => array(
                'title'         => __( 'Contents', 'fl-builder' ),
                'fields'        => array(
					'title_1' => array(
					    'type'          => 'text',
					    'label'         => __( 'Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'This is the H1 heading of the page. Do not add an H1 through a content block.', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
                    'icon' => array(
					    'type'          => 'photo',
					    'label'         => __('Icon', 'fl-builder'),
					    'show_remove'   => false,
					),
					'alignment' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select Alignment', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'Left', 'fl-builder' ),
					        '2'      => __( 'Center', 'fl-builder' ),
					        '3'      => __( 'Right', 'fl-builder' )
					    )
					),
					'uniqueid' => array(
					    'type'          => 'text',
					    'label'         => __( 'Optional unique ID for anchors', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					)
                )
            )
        )
    ),
) );