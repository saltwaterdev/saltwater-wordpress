.fl-node-<?php echo $id; ?> h3 {
	display: flex;
	flex-direction: row;
	flex-wrap: wrap;
	align-items: center;
	font-family: "freight-text-pro" !important;
    font-weight: 300 !important;
    font-size: 2.7em !important;
    margin: 0 !important;
    color: #222222 !important;
    letter-spacing: 0px !important;
    margin-bottom: 0.25em !important;
    margin-top: 0.25em !important;
    <?php if($settings->alignment == 2){ echo 'justify-content: center;'; } ?>
    <?php if($settings->alignment == 3){ echo 'justify-content: flex-end;'; } ?>
}
.fl-node-<?php echo $id; ?> h3 img{
	margin-right: 10px;
	margin-bottom: 1em;
	max-width: 150px;
}
@media only screen and (min-width: 768px){
	.fl-node-<?php echo $id; ?> h3 img{
		margin-bottom: 0;	
	}
}