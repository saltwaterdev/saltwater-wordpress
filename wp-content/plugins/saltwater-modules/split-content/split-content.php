<?php

class SplitContentClass extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Split Content', 'fl-builder' ),
            'description'     => __( 'Build a split content block', 'fl-builder' ),
            'group'           => __( 'Content', 'fl-builder' ),
            'category'        => __( 'Content Blocks', 'fl-builder' ),
            'dir'             => SALTWATER_DIR . 'split-content/',
            'url'             => SALTWATER_URL . 'split-content/',
            'icon'            => 'button.png',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
}

FLBuilder::register_module( 'SplitContentClass', array(
    'slide-1'      => array(
        'title'         => __( 'Left Side', 'fl-builder' ),
        'sections'      => array(
            'my-section-1'  => array(
                'title'         => __( 'Slide Contents', 'fl-builder' ),
                'fields'        => array(
	                'subtitle_1' => array(
					    'type'          => 'text',
					    'label'         => __( 'Subtitle (optional)', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'This appears above the title of the hero', 'fl-builder' ),
					    'help'          => __( 'If left blank, this will not show', 'fl-builder' )
					),
					'title_1' => array(
					    'type'          => 'text',
					    'label'         => __( 'Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'This is the H1 heading of the page. Do not add an H1 through a content block.', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
                    'hero_bg_1' => array(
					    'type'          => 'photo',
					    'label'         => __('Background', 'fl-builder'),
					    'show_remove'   => false,
					),
					'bg_color_1' => array(
					  'type'          => 'color',
					  'label'         => __( 'Background Color', 'fl-builder' ),
					  'default'       => 'eeeff0',
					  'show_reset'    => true,
					  'show_alpha'    => false
					),
					'hero_content_1' => array(
					    'type'          => 'editor',
					    'media_buttons' => true,
					    'wpautop'       => true
					),
					'content_color_1' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select Content Color Type', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'Normal', 'fl-builder' ),
					        '2'      => __( 'Light', 'fl-builder' ),
					        '3'      => __( 'Dark', 'fl-builder' )
					    )
					),
					'slide1_cta1_type' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select CTA 1 Type', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'None', 'fl-builder' ),
					        '2'      => __( 'Button', 'fl-builder' ),
					        '3'      => __( 'Ghost Button', 'fl-builder' ),
					        '4'      => __( 'Link', 'fl-builder' ),
					    )
					),
					'slide1_cta1_text' => array(
					    'type'          => 'text',
					    'label'         => __( 'CTA 1 Text', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( 'Learn More', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'Will default to "Learn More" if nothing is entered', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'slide1_cta1' => array(
					    'type'          => 'link',
					    'label'         => __('CTA 1', 'fl-builder')
					),
					'slide1_cta2_type' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select CTA 2 Type', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'None', 'fl-builder' ),
					        '2'      => __( 'Button', 'fl-builder' ),
					        '3'      => __( 'Ghost Button', 'fl-builder' ),
					        '4'      => __( 'Link', 'fl-builder' ),
					    )
					),
					'slide1_cta2_text' => array(
					    'type'          => 'text',
					    'label'         => __( 'CTA 2 Text', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( 'Learn More', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'Will default to "Learn More" if nothing is entered', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'slide1_cta2' => array(
					    'type'          => 'link',
					    'label'         => __('CTA 2', 'fl-builder')
					),
					'slide1_cta3_type' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select CTA 3 Type', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'None', 'fl-builder' ),
					        '2'      => __( 'Button', 'fl-builder' ),
					        '3'      => __( 'Ghost Button', 'fl-builder' ),
					        '4'      => __( 'Link', 'fl-builder' ),
					    )
					),
					'slide1_cta3_text' => array(
					    'type'          => 'text',
					    'label'         => __( 'CTA 3 Text', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( 'Learn More', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'Will default to "Learn More" if nothing is entered', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'slide1_cta3' => array(
					    'type'          => 'link',
					    'label'         => __('CTA 3', 'fl-builder')
					),
                )
            )
        )
    ),
    'side-2'      => array(
        'title'         => __( 'Right Side', 'fl-builder' ),
        'sections'      => array(
            'my-section-2'  => array(
                'title'         => __( 'Slide Contents', 'fl-builder' ),
                'fields'        => array(
	                'subtitle_2' => array(
					    'type'          => 'text',
					    'label'         => __( 'Subtitle (optional)', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'This appears above the title of the hero', 'fl-builder' ),
					    'help'          => __( 'If left blank, this will not show', 'fl-builder' )
					),
					'title_2' => array(
					    'type'          => 'text',
					    'label'         => __( 'Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'This is the H1 heading of the page. Do not add an H1 through a content block.', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
                    'hero_bg_2' => array(
					    'type'          => 'photo',
					    'label'         => __('Background', 'fl-builder'),
					    'show_remove'   => false,
					),
					'bg_color_2' => array(
					  'type'          => 'color',
					  'label'         => __( 'Background Color', 'fl-builder' ),
					  'default'       => 'eeeff0',
					  'show_reset'    => true,
					  'show_alpha'    => false
					),
					'content_color_2' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select Content Color Type', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'Normal', 'fl-builder' ),
					        '2'      => __( 'Light', 'fl-builder' ),
					        '3'      => __( 'Dark', 'fl-builder' )
					    )
					),
					'hero_content_2' => array(
					    'type'          => 'editor',
					    'media_buttons' => true,
					    'wpautop'       => true
					),
					'slide2_cta1_type' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select CTA 1 Type', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'None', 'fl-builder' ),
					        '2'      => __( 'Button', 'fl-builder' ),
					        '3'      => __( 'Ghost Button', 'fl-builder' ),
					        '4'      => __( 'Link', 'fl-builder' ),
					    )
					),
					'slide2_cta1_text' => array(
					    'type'          => 'text',
					    'label'         => __( 'CTA 1 Text', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( 'Learn More', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'Will default to "Learn More" if nothing is entered', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'slide2_cta1' => array(
					    'type'          => 'link',
					    'label'         => __('CTA 1', 'fl-builder')
					),
					'slide2_cta2_type' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select CTA 2 Type', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'None', 'fl-builder' ),
					        '2'      => __( 'Button', 'fl-builder' ),
					        '3'      => __( 'Ghost Button', 'fl-builder' ),
					        '4'      => __( 'Link', 'fl-builder' ),
					    )
					),
					'slide2_cta2_text' => array(
					    'type'          => 'text',
					    'label'         => __( 'CTA 2 Text', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( 'Learn More', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'Will default to "Learn More" if nothing is entered', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'slide2_cta2' => array(
					    'type'          => 'link',
					    'label'         => __('CTA 2', 'fl-builder')
					),
					'slide2_cta3_type' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select CTA 3 Type', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'None', 'fl-builder' ),
					        '2'      => __( 'Button', 'fl-builder' ),
					        '3'      => __( 'Ghost Button', 'fl-builder' ),
					        '4'      => __( 'Link', 'fl-builder' ),
					    )
					),
					'slide2_cta3_text' => array(
					    'type'          => 'text',
					    'label'         => __( 'CTA 3 Text', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( 'Learn More', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'Will default to "Learn More" if nothing is entered', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'slide2_cta3' => array(
					    'type'          => 'link',
					    'label'         => __('CTA 3', 'fl-builder')
					),
                )
            )
        )
    ),
    'slide-general'      => array(
        'title'         => __( 'General Settings', 'fl-builder' ),
        'sections'      => array(
            'my-section-3'  => array(
                'title'         => __( 'General Settings', 'fl-builder' ),
                'fields'        => array(
	                'logobar_title' => array(
					    'type'          => 'text',
					    'label'         => __( 'Logo Bar Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'logobar_img_1' => array(
					    'type'          => 'photo',
					    'label'         => __('Logo Bar Image 1', 'fl-builder'),
					    'show_remove'   => false,
					),
					'logobar_img_2' => array(
					    'type'          => 'photo',
					    'label'         => __('Logo Bar Image 2', 'fl-builder'),
					    'show_remove'   => false,
					),
					'logobar_img_3' => array(
					    'type'          => 'photo',
					    'label'         => __('Logo Bar Image 3', 'fl-builder'),
					    'show_remove'   => false,
					),
					'logobar_img_4' => array(
					    'type'          => 'photo',
					    'label'         => __('Logo Bar Image 4', 'fl-builder'),
					    'show_remove'   => false,
					),
                )
            )
        )
    ),
) );