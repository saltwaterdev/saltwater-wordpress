<div class="split_content">
	<div class="columns">
		<div class="col-sm-15">
			<div class="container">
				<div class="contents">
					<?php if ($settings->{"subtitle_1"} !== ""){ echo '<h4>'.$settings->{"subtitle_1"}.'</h4>'; } ?>
					<?php if ($settings->{"title_1"} !== ""){ echo '<h3>'.$settings->{"title_1"}.'</h4>'; } ?>
					<?php if ($settings->{"hero_content_1"} !== ""){ echo $settings->{"hero_content_1"}; } ?>
					<?php if($settings->slide1_cta1_type !== 1 || $settings->slide1_cta2_type !== 1 && $settings->slide1_cta3_type !== 1){ ?>
						<div class="ctas">
							<?php
								for($y=1;$y<=3;$y++){
									if($settings->{"slide1_cta".$y."_type"} !== 1){
										if($settings->{"slide1_cta".$y."_text"} == ""){
											$ctaText = "Learn More";
										} else {
											$ctaText = $settings->{"slide1_cta".$y."_text"};
										}
										if($settings->{"slide1_cta".$y} !== ""){
											echo '<a href="'.$settings->{"slide1_cta".$y}.'" class="type'.$settings->{"slide1_cta".$y."_type"}.'">'.$ctaText.'</a>';
										}
									}
								}	
							?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="col-sm-15">
			<div class="container">
				<div class="contents">
					<?php if ($settings->{"subtitle_2"} !== ""){ echo '<h4>'.$settings->{"subtitle_2"}.'</h4>'; } ?>
					<?php if ($settings->{"title_2"} !== ""){ echo '<h3>'.$settings->{"title_2"}.'</h4>'; } ?>
					<?php if ($settings->{"hero_content_2"} !== ""){ echo $settings->{"hero_content_2"}; } ?>
					<?php if($settings->slide2_cta1_type !== 1 || $settings->slide2_cta3_type !== 1 || $settings->slide2_cta3_type !== 1){ ?>
						<div class="ctas">
							<?php
								for($y=1;$y<=3;$y++){
									if($settings->{"slide2_cta".$y."_type"} !== 1){
										if($settings->{"slide2_cta".$y."_text"} == ""){
											$ctaText = "Learn More";
										} else {
											$ctaText = $settings->{"slide2_cta".$y."_text"};
										}
										if($settings->{"slide2_cta".$y} !== ""){
											echo '<a href="'.$settings->{"slide2_cta".$y}.'" class="type'.$settings->{"slide2_cta".$y."_type"}.'">'.$ctaText.'</a>';
										}
									}
								}	
								?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($settings->logobar_img_1 || $settings->logobar_img_2 || $settings->logobar_img_3 || $settings->logobar_img_4){ ?>
		<div class="col-sm-30 logobar">
			<div class="container">
				<?php if($settings->logobar_title){
					echo '<p>'.$settings->logobar_title.'</p>';
				} ?>
				<div class="logos">
					<?php
						for($x=1;$x<=4;$x++){
							if($settings->{'logobar_img_'.$x}){
								echo '<img src="'.wp_get_attachment_url($settings->{'logobar_img_'.$x}).'">';
							}
						}
					?>
				</div>
			</div>
		</div>
	<?php } ?>
</div>