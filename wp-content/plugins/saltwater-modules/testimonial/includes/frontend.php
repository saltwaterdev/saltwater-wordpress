<div class="testimonial">
	<?php $realCount = 0; for($x=1;$x<=5;$x++){ 
		$activeClass = ($x == 1) ? "active" : false;
		if ($settings->{"content_".$x} !== ""){
			$realCount++;
	?>
		<div class="slide <?php echo $activeClass; ?>" data-slide="<?php echo $realCount; ?>">
			<div class="container">
				<div class="contents">
					<?php if ($settings->{"subtitle_".$x} !== ""){ echo '<h4>'.$settings->{"subtitle_".$x}.'</h4>'; } ?>
					<?php if ($settings->{"content_".$x} !== ""){ echo $settings->{"content_".$x}; } ?>
					<div class="author">
						<div class="thumbnail"></div>
						<div class="info">
							<?php if ($settings->{"author_".$x} !== ""){ echo '<p class="name">'.$settings->{"author_".$x}.'</p>'; } ?>
							<?php if ($settings->{"author_title_".$x} !== "" || $settings->{"author_company_".$x} !== ""){ 
								$authorTitle = "";
								$authorCompany = "";
								$sep = "";
								if($settings->{"author_title_".$x} !== ""){
									$authorTitle = '<span>'.$settings->{"author_title_".$x}.'</span>';
								}
								if($settings->{"author_company_".$x} !== ""){
									$authorCompany = '<span>'.$settings->{"author_company_".$x}.'</span>';
								}
								if ($authorTitle !== "" && $authorCompany !== ""){
									$sep = " | ";
								}
								echo '<p class="desc">'.$authorTitle.''.$sep.''.$authorCompany.'</p>'; 
							} ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php } 
	}?>
	<?php if($realCount > 1){ ?>
		<ul class="indicators">
			<?php for($x=1;$x<=$realCount;$x++){
				$activeClass = ($x == 1) ? "active" : false;
				echo '<li><a class="'.$activeClass.'" data-slide="'.$x.'"></a></li>';
			} ?>
		</ul>
		<a class="prev"></a>
		<a class="next"></a>
	<?php }
	?>
</div>