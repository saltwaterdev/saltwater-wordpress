<?php

class TestimonialSliderClass extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Testimonial Slider', 'fl-builder' ),
            'description'     => __( 'Build a testimonial block', 'fl-builder' ),
            'group'           => __( 'Content', 'fl-builder' ),
            'category'        => __( 'Content Blocks', 'fl-builder' ),
            'dir'             => SALTWATER_DIR . 'testimonial/',
            'url'             => SALTWATER_URL . 'testimonial/',
            'icon'            => 'button.png',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
}

FLBuilder::register_module( 'TestimonialSliderClass', array(
    'slide-1'      => array(
        'title'         => __( 'Slide 1', 'fl-builder' ),
        'sections'      => array(
            'my-section-1'  => array(
                'title'         => __( 'Slide Contents', 'fl-builder' ),
                'fields'        => array(
					'subtitle_1' => array(
					    'type'          => 'text',
					    'label'         => __( 'Sub Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
                    'bg_1' => array(
					    'type'          => 'photo',
					    'label'         => __('Background', 'fl-builder'),
					    'show_remove'   => false,
					),
					'content_1' => array(
					    'type'          => 'editor',
					    'label'         => __('Content', 'fl-builder'),
					    'media_buttons' => true,
					    'wpautop'       => true
					),
					'thumb_1' => array(
					    'type'          => 'photo',
					    'label'         => __('Author Thumbnail', 'fl-builder'),
					    'show_remove'   => false,
					),
					'author_1' => array(
					    'type'          => 'text',
					    'label'         => __( 'Author Name', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'author_title_1' => array(
					    'type'          => 'text',
					    'label'         => __( 'Author Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'author_company_1' => array(
					    'type'          => 'text',
					    'label'         => __( 'Author Company', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
                )
            )
        )
    ),
    'slide-2'      => array(
        'title'         => __( 'Slide 2', 'fl-builder' ),
        'sections'      => array(
            'my-section-2'  => array(
                'title'         => __( 'Slide Contents', 'fl-builder' ),
                'fields'        => array(
					'subtitle_2' => array(
					    'type'          => 'text',
					    'label'         => __( 'Sub Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
                    'bg_2' => array(
					    'type'          => 'photo',
					    'label'         => __('Background', 'fl-builder'),
					    'show_remove'   => false,
					),
					'content_2' => array(
					    'type'          => 'editor',
					    'label'         => __('Content', 'fl-builder'),
					    'media_buttons' => true,
					    'wpautop'       => true
					),
					'thumb_2' => array(
					    'type'          => 'photo',
					    'label'         => __('Author Thumbnail', 'fl-builder'),
					    'show_remove'   => false,
					),
					'author_2' => array(
					    'type'          => 'text',
					    'label'         => __( 'Author Name', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'author_title_2' => array(
					    'type'          => 'text',
					    'label'         => __( 'Author Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'author_company_2' => array(
					    'type'          => 'text',
					    'label'         => __( 'Author Company', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
                )
            )
        )
    ),
    'slide-3'      => array(
        'title'         => __( 'Slide 3', 'fl-builder' ),
        'sections'      => array(
            'my-section-3'  => array(
                'title'         => __( 'Slide Contents', 'fl-builder' ),
                'fields'        => array(
					'subtitle_3' => array(
					    'type'          => 'text',
					    'label'         => __( 'Sub Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
                    'bg_3' => array(
					    'type'          => 'photo',
					    'label'         => __('Background', 'fl-builder'),
					    'show_remove'   => false,
					),
					'content_3' => array(
					    'type'          => 'editor',
					    'label'         => __('Content', 'fl-builder'),
					    'media_buttons' => true,
					    'wpautop'       => true
					),
					'thumb_3' => array(
					    'type'          => 'photo',
					    'label'         => __('Author Thumbnail', 'fl-builder'),
					    'show_remove'   => false,
					),
					'author_3' => array(
					    'type'          => 'text',
					    'label'         => __( 'Author Name', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'author_title_3' => array(
					    'type'          => 'text',
					    'label'         => __( 'Author Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'author_company_3' => array(
					    'type'          => 'text',
					    'label'         => __( 'Author Company', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
                )
            )
        )
    ),
    'slide-4'      => array(
        'title'         => __( 'Slide 4', 'fl-builder' ),
        'sections'      => array(
            'my-section-4'  => array(
                'title'         => __( 'Slide Contents', 'fl-builder' ),
                'fields'        => array(
					'subtitle_4' => array(
					    'type'          => 'text',
					    'label'         => __( 'Sub Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
                    'bg_4' => array(
					    'type'          => 'photo',
					    'label'         => __('Background', 'fl-builder'),
					    'show_remove'   => false,
					),
					'content_4' => array(
					    'type'          => 'editor',
					    'label'         => __('Content', 'fl-builder'),
					    'media_buttons' => true,
					    'wpautop'       => true
					),
					'thumb_4' => array(
					    'type'          => 'photo',
					    'label'         => __('Author Thumbnail', 'fl-builder'),
					    'show_remove'   => false,
					),
					'author_4' => array(
					    'type'          => 'text',
					    'label'         => __( 'Author Name', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'author_title_4' => array(
					    'type'          => 'text',
					    'label'         => __( 'Author Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'author_company_4' => array(
					    'type'          => 'text',
					    'label'         => __( 'Author Company', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
                )
            )
        )
    ),
    'slide-5'      => array(
        'title'         => __( 'Slide 5', 'fl-builder' ),
        'sections'      => array(
            'my-section-5'  => array(
                'title'         => __( 'Slide Contents', 'fl-builder' ),
                'fields'        => array(
					'subtitle_5' => array(
					    'type'          => 'text',
					    'label'         => __( 'Sub Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
                    'bg_5' => array(
					    'type'          => 'photo',
					    'label'         => __('Background', 'fl-builder'),
					    'show_remove'   => false,
					),
					'content_5' => array(
					    'type'          => 'editor',
					    'label'         => __('Content', 'fl-builder'),
					    'media_buttons' => true,
					    'wpautop'       => true
					),
					'thumb_5' => array(
					    'type'          => 'photo',
					    'label'         => __('Author Thumbnail', 'fl-builder'),
					    'show_remove'   => false,
					),
					'author_5' => array(
					    'type'          => 'text',
					    'label'         => __( 'Author Name', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'author_title_5' => array(
					    'type'          => 'text',
					    'label'         => __( 'Author Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'author_company_5' => array(
					    'type'          => 'text',
					    'label'         => __( 'Author Company', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
                )
            )
        )
    ),
    'slide-6'      => array(
        'title'         => __( 'Slide 6', 'fl-builder' ),
        'sections'      => array(
            'my-section-6'  => array(
                'title'         => __( 'Slide Contents', 'fl-builder' ),
                'fields'        => array(
					'subtitle_6' => array(
					    'type'          => 'text',
					    'label'         => __( 'Sub Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
                    'bg_6' => array(
					    'type'          => 'photo',
					    'label'         => __('Background', 'fl-builder'),
					    'show_remove'   => false,
					),
					'content_6' => array(
					    'type'          => 'editor',
					    'label'         => __('Content', 'fl-builder'),
					    'media_buttons' => true,
					    'wpautop'       => true
					),
					'thumb_6' => array(
					    'type'          => 'photo',
					    'label'         => __('Author Thumbnail', 'fl-builder'),
					    'show_remove'   => false,
					),
					'author_6' => array(
					    'type'          => 'text',
					    'label'         => __( 'Author Name', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'author_title_6' => array(
					    'type'          => 'text',
					    'label'         => __( 'Author Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'author_company_6' => array(
					    'type'          => 'text',
					    'label'         => __( 'Author Company', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
                )
            )
        )
    ),
    'slide-general'      => array(
        'title'         => __( 'General Settings', 'fl-builder' ),
        'sections'      => array(
            'my-section-7'  => array(
                'title'         => __( 'General Settings', 'fl-builder' ),
                'fields'        => array(
	                'bg_color' => array(
					  'type'          => 'color',
					  'label'         => __( 'Background Color', 'fl-builder' ),
					  'default'       => '5c6267',
					  'show_reset'    => true,
					  'show_alpha'    => false
					),
                )
            )
        )
    ),
) );