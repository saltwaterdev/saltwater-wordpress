<?php

class InteriorHeroClass extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Interior Hero', 'fl-builder' ),
            'description'     => __( 'Build an interior hero', 'fl-builder' ),
            'group'           => __( 'Content', 'fl-builder' ),
            'category'        => __( 'Content Blocks', 'fl-builder' ),
            'dir'             => SALTWATER_DIR . 'interior-hero/',
            'url'             => SALTWATER_URL . 'interior-hero/',
            'icon'            => 'button.png',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
}

FLBuilder::register_module( 'InteriorHeroClass', array(
    'slide-1'      => array(
        'title'         => __( 'Slide 1', 'fl-builder' ),
        'sections'      => array(
            'my-section-1'  => array(
                'title'         => __( 'Slide Contents', 'fl-builder' ),
                'fields'        => array(
	                'subtitle_1' => array(
					    'type'          => 'text',
					    'label'         => __( 'Subtitle (optional)', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'This appears above the title of the hero', 'fl-builder' ),
					    'help'          => __( 'If left blank, this will not show', 'fl-builder' )
					),
					'title_1' => array(
					    'type'          => 'text',
					    'label'         => __( 'Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'This is the H1 heading of the page. Do not add an H1 through a content block.', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
                    'hero_bg_1' => array(
					    'type'          => 'photo',
					    'label'         => __('Background', 'fl-builder'),
					    'show_remove'   => false,
					),
					'hero_content_1' => array(
					    'type'          => 'editor',
					    'media_buttons' => true,
					    'wpautop'       => true
					),
					'slide1_cta1_type' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select CTA 1 Type', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'None', 'fl-builder' ),
					        '2'      => __( 'Button', 'fl-builder' ),
					        '3'      => __( 'Ghost Button', 'fl-builder' ),
					        '4'      => __( 'Link', 'fl-builder' ),
					    )
					),
					'slide1_cta1_text' => array(
					    'type'          => 'text',
					    'label'         => __( 'CTA 1 Text', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( 'Learn More', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'Will default to "Learn More" if nothing is entered', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'slide1_cta1' => array(
					    'type'          => 'link',
					    'label'         => __('CTA 1', 'fl-builder')
					),
					'slide1_cta2_type' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select CTA 2 Type', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'None', 'fl-builder' ),
					        '2'      => __( 'Button', 'fl-builder' ),
					        '3'      => __( 'Ghost Button', 'fl-builder' ),
					        '4'      => __( 'Link', 'fl-builder' ),
					    )
					),
					'slide1_cta2_text' => array(
					    'type'          => 'text',
					    'label'         => __( 'CTA 2 Text', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( 'Learn More', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'Will default to "Learn More" if nothing is entered', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'slide1_cta2' => array(
					    'type'          => 'link',
					    'label'         => __('CTA 2', 'fl-builder')
					),
					'slide1_cta3_type' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select CTA 3 Type', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'None', 'fl-builder' ),
					        '2'      => __( 'Button', 'fl-builder' ),
					        '3'      => __( 'Ghost Button', 'fl-builder' ),
					        '4'      => __( 'Link', 'fl-builder' ),
					    )
					),
					'slide1_cta3_text' => array(
					    'type'          => 'text',
					    'label'         => __( 'CTA 3 Text', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( 'Learn More', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'Will default to "Learn More" if nothing is entered', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'slide1_cta3' => array(
					    'type'          => 'link',
					    'label'         => __('CTA 3', 'fl-builder')
					),
                )
            )
        )
    ),
    'slide-2'      => array(
        'title'         => __( 'Slide 2', 'fl-builder' ),
        'sections'      => array(
            'my-section-2'  => array(
                'title'         => __( 'Slide Contents', 'fl-builder' ),
                'fields'        => array(
	                'subtitle_2' => array(
					    'type'          => 'text',
					    'label'         => __( 'Subtitle (optional)', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'This appears above the title of the hero', 'fl-builder' ),
					    'help'          => __( 'If left blank, this will not show', 'fl-builder' )
					),
					'title_2' => array(
					    'type'          => 'text',
					    'label'         => __( 'Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'This is the H1 heading of the page. Do not add an H1 through a content block.', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
                    'hero_bg_2' => array(
					    'type'          => 'photo',
					    'label'         => __('Background', 'fl-builder'),
					    'show_remove'   => false,
					),
					'hero_content_2' => array(
					    'type'          => 'editor',
					    'media_buttons' => true,
					    'wpautop'       => true
					),
					'slide2_cta1_type' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select CTA 1 Type', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'None', 'fl-builder' ),
					        '2'      => __( 'Button', 'fl-builder' ),
					        '3'      => __( 'Ghost Button', 'fl-builder' ),
					        '4'      => __( 'Link', 'fl-builder' ),
					    )
					),
					'slide2_cta1_text' => array(
					    'type'          => 'text',
					    'label'         => __( 'CTA 1 Text', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( 'Learn More', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'Will default to "Learn More" if nothing is entered', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'slide2_cta1' => array(
					    'type'          => 'link',
					    'label'         => __('CTA 1', 'fl-builder')
					),
					'slide2_cta2_type' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select CTA 2 Type', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'None', 'fl-builder' ),
					        '2'      => __( 'Button', 'fl-builder' ),
					        '3'      => __( 'Ghost Button', 'fl-builder' ),
					        '4'      => __( 'Link', 'fl-builder' ),
					    )
					),
					'slide2_cta2_text' => array(
					    'type'          => 'text',
					    'label'         => __( 'CTA 2 Text', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( 'Learn More', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'Will default to "Learn More" if nothing is entered', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'slide2_cta2' => array(
					    'type'          => 'link',
					    'label'         => __('CTA 2', 'fl-builder')
					),
					'slide2_cta3_type' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select CTA 3 Type', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'None', 'fl-builder' ),
					        '2'      => __( 'Button', 'fl-builder' ),
					        '3'      => __( 'Ghost Button', 'fl-builder' ),
					        '4'      => __( 'Link', 'fl-builder' ),
					    )
					),
					'slide2_cta3_text' => array(
					    'type'          => 'text',
					    'label'         => __( 'CTA 3 Text', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( 'Learn More', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'Will default to "Learn More" if nothing is entered', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'slide2_cta3' => array(
					    'type'          => 'link',
					    'label'         => __('CTA 3', 'fl-builder')
					),
                )
            )
        )
    ),
    'slide-3'      => array(
        'title'         => __( 'Slide 3', 'fl-builder' ),
        'sections'      => array(
            'my-section-3'  => array(
                'title'         => __( 'Slide Contents', 'fl-builder' ),
                'fields'        => array(
	                'subtitle_3' => array(
					    'type'          => 'text',
					    'label'         => __( 'Subtitle (optional)', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'This appears above the title of the hero', 'fl-builder' ),
					    'help'          => __( 'If left blank, this will not show', 'fl-builder' )
					),
					'title_3' => array(
					    'type'          => 'text',
					    'label'         => __( 'Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'This is the H1 heading of the page. Do not add an H1 through a content block.', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
                    'hero_bg_3' => array(
					    'type'          => 'photo',
					    'label'         => __('Background', 'fl-builder'),
					    'show_remove'   => false,
					),
					'hero_content_3' => array(
					    'type'          => 'editor',
					    'media_buttons' => true,
					    'wpautop'       => true
					),
					'slide3_cta1_type' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select CTA 1 Type', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'None', 'fl-builder' ),
					        '2'      => __( 'Button', 'fl-builder' ),
					        '3'      => __( 'Ghost Button', 'fl-builder' ),
					        '4'      => __( 'Link', 'fl-builder' ),
					    )
					),
					'slide3_cta1_text' => array(
					    'type'          => 'text',
					    'label'         => __( 'CTA 1 Text', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( 'Learn More', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'Will default to "Learn More" if nothing is entered', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'slide3_cta1' => array(
					    'type'          => 'link',
					    'label'         => __('CTA 1', 'fl-builder')
					),
					'slide3_cta2_type' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select CTA 2 Type', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'None', 'fl-builder' ),
					        '2'      => __( 'Button', 'fl-builder' ),
					        '3'      => __( 'Ghost Button', 'fl-builder' ),
					        '4'      => __( 'Link', 'fl-builder' ),
					    )
					),
					'slide3_cta2_text' => array(
					    'type'          => 'text',
					    'label'         => __( 'CTA 2 Text', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( 'Learn More', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'Will default to "Learn More" if nothing is entered', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'slide3_cta2' => array(
					    'type'          => 'link',
					    'label'         => __('CTA 2', 'fl-builder')
					),
					'slide3_cta3_type' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select CTA 3 Type', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'None', 'fl-builder' ),
					        '2'      => __( 'Button', 'fl-builder' ),
					        '3'      => __( 'Ghost Button', 'fl-builder' ),
					        '4'      => __( 'Link', 'fl-builder' ),
					    )
					),
					'slide3_cta3_text' => array(
					    'type'          => 'text',
					    'label'         => __( 'CTA 3 Text', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( 'Learn More', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'Will default to "Learn More" if nothing is entered', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'slide3_cta3' => array(
					    'type'          => 'link',
					    'label'         => __('CTA 3', 'fl-builder')
					),
                )
            )
        )
    ),
    'slide-4'      => array(
        'title'         => __( 'Slide 4', 'fl-builder' ),
        'sections'      => array(
            'my-section-4'  => array(
                'title'         => __( 'Slide Contents', 'fl-builder' ),
                'fields'        => array(
	                'subtitle_4' => array(
					    'type'          => 'text',
					    'label'         => __( 'Subtitle (optional)', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'This appears above the title of the hero', 'fl-builder' ),
					    'help'          => __( 'If left blank, this will not show', 'fl-builder' )
					),
					'title_4' => array(
					    'type'          => 'text',
					    'label'         => __( 'Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'This is the H1 heading of the page. Do not add an H1 through a content block.', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
                    'hero_bg_4' => array(
					    'type'          => 'photo',
					    'label'         => __('Background', 'fl-builder'),
					    'show_remove'   => false,
					),
					'hero_content_4' => array(
					    'type'          => 'editor',
					    'media_buttons' => true,
					    'wpautop'       => true
					),
					'slide4_cta1_type' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select CTA 1 Type', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'None', 'fl-builder' ),
					        '2'      => __( 'Button', 'fl-builder' ),
					        '3'      => __( 'Ghost Button', 'fl-builder' ),
					        '4'      => __( 'Link', 'fl-builder' ),
					    )
					),
					'slide4_cta1_text' => array(
					    'type'          => 'text',
					    'label'         => __( 'CTA 1 Text', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( 'Learn More', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'Will default to "Learn More" if nothing is entered', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'slide4_cta1' => array(
					    'type'          => 'link',
					    'label'         => __('CTA 1', 'fl-builder')
					),
					'slide4_cta2_type' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select CTA 2 Type', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'None', 'fl-builder' ),
					        '2'      => __( 'Button', 'fl-builder' ),
					        '3'      => __( 'Ghost Button', 'fl-builder' ),
					        '4'      => __( 'Link', 'fl-builder' ),
					    )
					),
					'slide4_cta2_text' => array(
					    'type'          => 'text',
					    'label'         => __( 'CTA 2 Text', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( 'Learn More', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'Will default to "Learn More" if nothing is entered', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'slide4_cta2' => array(
					    'type'          => 'link',
					    'label'         => __('CTA 2', 'fl-builder')
					),
					'slide4_cta3_type' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select CTA 3 Type', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'None', 'fl-builder' ),
					        '2'      => __( 'Button', 'fl-builder' ),
					        '3'      => __( 'Ghost Button', 'fl-builder' ),
					        '4'      => __( 'Link', 'fl-builder' ),
					    )
					),
					'slide4_cta3_text' => array(
					    'type'          => 'text',
					    'label'         => __( 'CTA 3 Text', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( 'Learn More', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'Will default to "Learn More" if nothing is entered', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'slide4_cta3' => array(
					    'type'          => 'link',
					    'label'         => __('CTA 3', 'fl-builder')
					),
                )
            )
        )
    ),
    'slide-5'      => array(
        'title'         => __( 'Slide 5', 'fl-builder' ),
        'sections'      => array(
            'my-section-5'  => array(
                'title'         => __( 'Slide Contents', 'fl-builder' ),
                'fields'        => array(
	                'subtitle_5' => array(
					    'type'          => 'text',
					    'label'         => __( 'Subtitle (optional)', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'This appears above the title of the hero', 'fl-builder' ),
					    'help'          => __( 'If left blank, this will not show', 'fl-builder' )
					),
					'title_5' => array(
					    'type'          => 'text',
					    'label'         => __( 'Title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'This is the H1 heading of the page. Do not add an H1 through a content block.', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
                    'hero_bg_5' => array(
					    'type'          => 'photo',
					    'label'         => __('Background', 'fl-builder'),
					    'show_remove'   => false,
					),
					'hero_content_5' => array(
					    'type'          => 'editor',
					    'media_buttons' => true,
					    'wpautop'       => true
					),
					'slide5_cta1_type' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select CTA 1 Type', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'None', 'fl-builder' ),
					        '2'      => __( 'Button', 'fl-builder' ),
					        '3'      => __( 'Ghost Button', 'fl-builder' ),
					        '4'      => __( 'Link', 'fl-builder' ),
					    )
					),
					'slide5_cta1_text' => array(
					    'type'          => 'text',
					    'label'         => __( 'CTA 1 Text', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( 'Learn More', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'Will default to "Learn More" if nothing is entered', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'slide5_cta1' => array(
					    'type'          => 'link',
					    'label'         => __('CTA 1', 'fl-builder')
					),
					'slide5_cta2_type' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select CTA 2 Type', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'None', 'fl-builder' ),
					        '2'      => __( 'Button', 'fl-builder' ),
					        '3'      => __( 'Ghost Button', 'fl-builder' ),
					        '4'      => __( 'Link', 'fl-builder' ),
					    )
					),
					'slide5_cta2_text' => array(
					    'type'          => 'text',
					    'label'         => __( 'CTA 2 Text', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( 'Learn More', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'Will default to "Learn More" if nothing is entered', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'slide5_cta2' => array(
					    'type'          => 'link',
					    'label'         => __('CTA 2', 'fl-builder')
					),
					'slide5_cta3_type' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select CTA 3 Type', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'None', 'fl-builder' ),
					        '2'      => __( 'Button', 'fl-builder' ),
					        '3'      => __( 'Ghost Button', 'fl-builder' ),
					        '4'      => __( 'Link', 'fl-builder' ),
					    )
					),
					'slide5_cta3_text' => array(
					    'type'          => 'text',
					    'label'         => __( 'CTA 3 Text', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( 'Learn More', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( 'Will default to "Learn More" if nothing is entered', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'slide5_cta3' => array(
					    'type'          => 'link',
					    'label'         => __('CTA 3', 'fl-builder')
					),
                )
            )
        )
    ),
    'slide-general'      => array(
        'title'         => __( 'General Settings', 'fl-builder' ),
        'sections'      => array(
            'my-section-1'  => array(
                'title'         => __( 'Slide Contents', 'fl-builder' ),
                'fields'        => array(
	                'hero_size' => array(
					    'type'          => 'select',
					    'label'         => __( 'Hero Size', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'Full Screen', 'fl-builder' ),
					        '2'      => __( 'Half Screen', 'fl-builder' ),
					    )
					),
					'bg_color' => array(
					  'type'          => 'color',
					  'label'         => __( 'Background Color', 'fl-builder' ),
					  'default'       => '333333',
					  'show_reset'    => true,
					  'show_alpha'    => false
					),
					'hyperlink_target' => array(
					    'type'          => 'select',
					    'label'         => __( 'Should hyperlinks open in same or new window/tab?', 'fl-builder' ),
					    'default'       => '_self',
					    'options'       => array(
					        '_self'      => __( 'Same Window/Tab', 'fl-builder' ),
					        '_blank'      => __( 'New Window/Tab', 'fl-builder' ),
					    )
					),
                )
            )
        )
    ),
) );