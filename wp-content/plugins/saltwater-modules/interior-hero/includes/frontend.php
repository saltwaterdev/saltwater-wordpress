<div class="int_hero">
	<?php $realCount = 0; for($x=1;$x<=5;$x++){ 
		$activeClass = ($x == 1) ? "active" : false;
		if ($settings->{"title_".$x} !== ""){
			$realCount++;
	?>
		<div class="slide <?php echo $activeClass; ?>" data-slide="<?php echo $realCount; ?>">
			<div class="container">
				<div class="contents">
					<?php if ($settings->{"subtitle_".$x} !== ""){ echo '<h4>'.$settings->{"subtitle_".$x}.'</h4>'; } ?>
					<?php if ($settings->{"title_".$x} !== ""){ echo '<h3>'.$settings->{"title_".$x}.'</h4>'; } ?>
					<?php if ($settings->{"hero_content_".$x} !== ""){ echo $settings->{"hero_content_".$x}; } ?>
					<div class="ctas">
						<?php
							for($y=1;$y<=3;$y++){
								if($settings->{"slide".$x."_cta".$y."_type"} !== 1){
									if($settings->{"slide".$x."_cta".$y."_text"} == ""){
										$ctaText = "Learn More";
									} else {
										$ctaText = $settings->{"slide".$x."_cta".$y."_text"};
									}
									if($settings->{"slide".$x."_cta".$y} !== ""){
										echo '<a href="'.$settings->{"slide".$x."_cta".$y}.'" class="type'.$settings->{"slide".$x."_cta".$y."_type"}.'" target="'.$settings->hyperlink_target.'">'.$ctaText.'</a>';
									}
								}
							}	
						?>
					</div>
				</div>
			</div>
		</div>
	<?php } 
	}?>
	<?php if($realCount > 1){ ?>
		<ul class="indicators">
			<?php for($x=1;$x<=$realCount;$x++){
				$activeClass = ($x == 1) ? "active" : false;
				echo '<li><a class="'.$activeClass.'" data-slide="'.$x.'"></a></li>';
			} ?>
		</ul>
	<?php }
	?>
</div>