.fl-node-<?php echo $id; ?> {
	
}

<?php if($settings->hero_size == 1){ ?>
.fl-node-<?php echo $id; ?> .int_hero{
	height: calc(100vh - 100px);
	max-height: 1000px;
	background-color: #<?php echo $settings->bg_color; ?>;
}
<?php } ?>
<?php if($settings->hero_size == 2){ ?>
.fl-node-<?php echo $id; ?> .int_hero{
	height: calc(50vh - 100px);
	max-height: 500px;
}
<?php } ?>
<?php for($x=1;$x<=5;$x++){ ?>
.fl-node-<?php echo $id; ?> .int_hero .slide[data-slide="<?php echo $x; ?>"]{
	 <?php if ($settings->{"hero_bg_".$x}){ ?>background-image: url(<?php echo wp_get_attachment_url($settings->{"hero_bg_".$x}); ?>);<?php } ?>
}
<?php } ?>