<?php

/**
 * Plugin Name: Saltwater Modules
 * Plugin URI: http://www.saltwaterco.com
 * Description: Custom modules for the Saltwater Site
 * Version: 1.0
 * Author: Alex Davis
 * Author URI: https://www.saltwaterco.com
 */
define( 'SALTWATER_DIR', plugin_dir_path( __FILE__ ) );
define( 'SALTWATER_URL', plugins_url( '/', __FILE__ ) );

function saltwater_modules_examples() {
    if ( class_exists( 'FLBuilder' ) ) {
        require_once 'content-box/content-box.php';
        require_once 'team-member/team-member.php';
        require_once 'hero/hero.php';
        require_once 'interior-hero/interior-hero.php';
        require_once 'split-content/split-content.php';
        require_once 'featured-text/featured-text.php';
        require_once 'testimonial/testimonial.php';
        require_once 'header-icon/header-icon.php';
        require_once 'color-box/color-box.php';
        require_once 'logo-bar/logo-bar.php';
    }
}
add_action( 'init', 'saltwater_modules_examples' );