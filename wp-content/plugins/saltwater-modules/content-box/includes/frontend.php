<?php
	require_once('TwitterAPIExchange.php');
	
	$path = $_SERVER['DOCUMENT_ROOT'];
	
	include_once $path . '/wp-config.php';
	include_once $path . '/wp-load.php';
	include_once $path . '/wp-includes/wp-db.php';
	include_once $path . '/wp-includes/pluggable.php';
	
	function igSendoff($apicall, $data, $method){
		if ($method == "POST"){
			$ch = curl_init( $apicall );
			$payload = http_build_query($data);
			curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			$result = curl_exec($ch);
			curl_close($ch);
			$result = json_decode($result);
		}
		if ($method == "GET"){
			$ch = curl_init();
			
			curl_setopt($ch, CURLOPT_URL, $apicall);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			
			$result = curl_exec($ch);
			if (curl_errno($ch)) {
			    echo 'Error:' . curl_error($ch);
			}
			curl_close($ch);
			$result = json_decode($result);
		}
		return $result;
	}
	
	function instagram_id_to_url($instagram_id){
		$url_prefix = "https://www.instagram.com/p/";
		$url_suffix = "";
	    if(!empty(strpos($instagram_id, '_'))){
	        $parts = explode('_', $instagram_id);
	        $instagram_id = $parts[0];
	        $userid = $parts[1];
	    }
	    $alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_';
	    while($instagram_id > 0){
	        $remainder = $instagram_id % 64;
	        $instagram_id = ($instagram_id-$remainder) / 64;
	        $url_suffix = $alphabet{$remainder} . $url_suffix;
	    };
	    return $url_prefix.$url_suffix;
	}
	
	global $wpdb;
	
	$igToken = $wpdb->get_results("SELECT * FROM instagram WHERE id = 1");
	foreach($igToken as $tokenRow){
		$theToken = $tokenRow->token;
		$theTime = $tokenRow->time;
	}
 
	$settings = array(
	    'oauth_access_token' => "132998183-QpaULtCah7b9VaCuYv44NwpdqD328vxIai5R26J3",
	    'oauth_access_token_secret' => "V72oIzM25EH8Tz1tGfMXpZN7KiweJFUhPAmixeVOE",
	    'consumer_key' => "gepdB8baKeQIgivVyIYeg",
	    'consumer_secret' => "eizgSBjpHTSCTIAMxUs2Sx1oJDDogSFUKoGEQPYo3c"
	);
	
	$url = "https://api.twitter.com/1.1/statuses/user_timeline.json";
	$requestMethod = "GET";
	$getfield = '?screen_name=saltwaterco&count=2';
	
	$twitter = new TwitterAPIExchange($settings);
	$string = json_decode($twitter->setGetfield($getfield)
	->buildOauth($url, $requestMethod)
	->performRequest(),$assoc = TRUE);
	if(array_key_exists("errors", $string)) {echo "<h3>Sorry, there was a problem.</h3><p>Twitter returned the following error message:</p><p><em>".$string[errors][0]["message"]."</em></p>";exit();}

	
	function rudr_instagram_api_curl_connect( $api_url ){
		$connection_c = curl_init(); // initializing
		curl_setopt( $connection_c, CURLOPT_URL, $api_url ); // API URL to connect
		curl_setopt( $connection_c, CURLOPT_RETURNTRANSFER, 1 ); // return the result, do not print
		curl_setopt( $connection_c, CURLOPT_TIMEOUT, 20 );
		$json_return = curl_exec( $connection_c ); // connect and get json data
		curl_close( $connection_c ); // close connection
		return json_decode( $json_return ); // decode and return
	}
	
	$mediaInfo = igSendoff(
		'https://graph.instagram.com/me/media?fields=id,caption&access_token='.$theToken,
		array(),
		'GET'
	);
	
// 	echo $mediaInfo->data[0]->id;

	$fileInfoA = igSendoff(
		'https://graph.instagram.com/'.$mediaInfo->data[0]->id.'?fields=id,media_type,media_url,username,timestamp&access_token='.$theToken,
		array(),
		'GET'
	);
	$filePhotoA = $fileInfoA->media_url;
	$fileInfoB = igSendoff(
		'https://graph.instagram.com/'.$mediaInfo->data[1]->id.'?fields=id,media_type,media_url,username,timestamp&access_token='.$theToken,
		array(),
		'GET'
	);
	$filePhotoB = $fileInfoB->media_url;
	if($fileInfoB->media_type == "VIDEO"){
		$fileInfoC = igSendoff(
			'https://graph.instagram.com/'.$mediaInfo->data[2]->id.'?fields=id,media_type,media_url,username,timestamp&access_token='.$theToken,
			array(),
			'GET'
		);
		$filePhotoB = $fileInfoC->media_url;
		if($fileInfoC->media_type == "VIDEO"){
			$fileInfoD = igSendoff(
				'https://graph.instagram.com/'.$mediaInfo->data[3]->id.'?fields=id,media_type,media_url,username,timestamp&access_token='.$theToken,
				array(),
				'GET'
			);
			$filePhotoB = $fileInfoD->media_url;
		}
	}
	
// 	print_r($fileInfo);

	
	$return = array();

		$args = array(
		 	'posts_per_page'   => 4,
		    'post_type'        => 'post',
		);
    	$allposts = new WP_Query($args);  
    	if( $allposts->have_posts()): 

		while( $allposts->have_posts()): $allposts->the_post();

		{ 
			$term_list = wp_get_post_terms(get_the_ID(), 'category', array("fields" => "names"));
			$output[] = array("date"=>get_the_time('n.j.Y'), "categories"=>implode(', ', $term_list), "name"=>get_the_title(), "time"=>prefix_estimated_reading_time( get_the_content()), "link"=>get_the_permalink());
		}
		
		endwhile; 
	 	else:
	 	endif;
	 	
	 	function make_urls_into_links($plain_text) {
		    return preg_replace(
		        '@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-~]*(\?\S+)?)?)?)@',
		        '<a href="$1">$1</a>', $plain_text);
		}

?>

<div class="content fl-rich-text preview_rows">
	<div class="preview_holder animation8 post"><div class="preview_box single content">
		<p class="info"><span class="date"><?php echo $output[0]['date']; ?></span>  • <span class="type"><?php echo strlen($output[0]['categories']) > 22 ? substr($output[0]['categories'],22)."..." : $output[0]['categories']; ?></span></p>
		<h4><?php echo strlen($output[0]['name']) > 50 ? substr($output[0]['name'],0,50)."..." : $output[0]['name']; ?></h4>
		<p class="date"><?php echo $output[0]['time']; ?> min</p>
		<div class="right-arrow"><a href="<?php echo $output[0]['link']; ?>">Read</a></div>
	</div></div>
	<div class="preview_holder animation9"><div class="preview_box single instagram" style="background-image: url(<?php echo $filePhotoA; ?>);">
		<a href="https://www.instagram.com/saltwater_collective/" target="_blank"></a>
	</div></div>
	<div class="preview_holder animation10 post"><div class="preview_box single content">
		<p class="info"><span class="date"><?php echo $output[1]['date']; ?></span> • <span class="type"><?php echo strlen($output[1]['categories']) > 22 ? substr($output[1]['categories'],22)."..." : $output[1]['categories']; ?></span></p>
		<h4><?php echo strlen($output[1]['name']) > 50 ? substr($output[1]['name'],0,50)."..." : $output[1]['name']; ?></h4>
		<p class="date"><?php echo $output[0]['time']; ?> min</p>
		<div class="right-arrow"><a href="<?php echo $output[1]['link']; ?>">Read</a></div>
	</div></div>
	<div class="preview_holder animation11"><div class="preview_box single instagram" style="background-image: url(<?php echo $filePhotoB; ?>);">
		<a href="https://www.instagram.com/saltwater_collective/" target="_blank"></a>
	</div></div>
	<div class="preview_holder animation12"><div class="preview_box single twitter">
		<?php if ( $string[0]['entities']['urls']) { $url = $string[0]['entities']['urls'][0]['url'];} else {$url = "https://twitter.com/saltwaterco";} ?>
		<a href="<?php echo $url; ?>" target="_blank" class="twitter_link"><i class="fab fa-twitter"></i></a>
		<h4><?php echo make_urls_into_links($string[0]['text']); ?></h4>
	</div></div>
	<div class="preview_holder animation13 post"><div class="preview_box single content">
		<p class="info"><span class="date"><?php echo $output[2]['date']; ?></span> • <span class="type"><?php echo strlen($output[2]['categories']) > 22 ? substr($output[2]['categories'],22)."..." : $output[2]['categories']; ?></span></p>
		<h4><?php echo strlen($output[2]['name']) > 50 ? substr($output[2]['name'],0,50)."..." : $output[2]['name']; ?></h4>
		<p class="date"><?php echo $output[2]['time']; ?> min</p>
		<div class="right-arrow"><a href="<?php echo $output[2]['link']; ?>">Read</a></div>
	</div></div>
	<div class="preview_holder animation14"><div class="preview_box single twitter">
		<?php if ( $string[1]['entities']['urls']) { $url = $string[1]['entities']['urls'][0]['url'];} else {$url = "https://twitter.com/saltwaterco";} ?>
		<a href="<?php echo $url; ?>" target="_blank" class="twitter_link"><i class="fab fa-twitter"></i></a>
		<h4><?php echo make_urls_into_links($string[1]['text']); ?></h4>
	</div></div>
	<div class="preview_holder animation15 post"><div class="preview_box single content">
		<p class="info"><span class="date"><?php echo $output[3]['date']; ?></span> • <span class="type"><?php echo strlen($output[3]['categories']) > 22 ? substr($output[3]['categories'],0,22)."..." : $output[3]['categories']; ?></span></p>
		<h4><?php echo strlen($output[3]['name']) > 50 ? substr($output[3]['name'],0,50)."..." : $output[3]['name']; ?></h4>
		<p class="date"><?php echo $output[3]['time']; ?> min</p>
		<div class="right-arrow"><a href="<?php echo $output[3]['link']; ?>">Read</a></div>
	</div></div>
</div>