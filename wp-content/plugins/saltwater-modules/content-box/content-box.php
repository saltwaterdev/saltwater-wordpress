<?php

class ContentBoxClass extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Content Box', 'fl-builder' ),
            'description'     => __( 'Build a content box', 'fl-builder' ),
            'group'           => __( 'Content', 'fl-builder' ),
            'category'        => __( 'Content Blocks', 'fl-builder' ),
            'dir'             => SALTWATER_DIR . 'content-box/',
            'url'             => SALTWATER_URL . 'content-box/',
            'icon'            => 'button.png',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
}

FLBuilder::register_module( 'ContentBoxClass', array(
    'my-tab-1'      => array(
        'title'         => __( 'Content Box Content', 'fl-builder' ),
        'sections'      => array(
            'my-section-1'  => array(
                'title'         => __( 'Section 1', 'fl-builder' ),
                'fields'        => array(
                    'url' => array(
					    'type'          => 'text',
					    'label'         => __( 'Enter URL of Content', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '255',
					    'size'          => '40',
					    'placeholder'   => __( 'Enter URL Here', 'fl-builder' ),
					    'class'         => 'url',
					    'description'   => __( 'The box will adjust to what the URL is, whether it be Blog, Tweet, or Instagram.', 'fl-builder' ),
					    'help'          => __( 'Will form fit to URL', 'fl-builder' )
					),
                )
            )
        )
    )
) );