<?php

class HeroClass extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Hero', 'fl-builder' ),
            'description'     => __( 'Build a hero', 'fl-builder' ),
            'group'           => __( 'Content', 'fl-builder' ),
            'category'        => __( 'Content Blocks', 'fl-builder' ),
            'dir'             => SALTWATER_DIR . 'hero/',
            'url'             => SALTWATER_URL . 'hero/',
            'icon'            => 'button.png',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
}

FLBuilder::register_module( 'HeroClass', array(
    'my-tab-1'      => array(
        'title'         => __( 'Hero Content', 'fl-builder' ),
        'sections'      => array(
            'my-section-1'  => array(
                'title'         => __( 'Section 1', 'fl-builder' ),
                'fields'        => array(
                    'hero_bg' => array(
					    'type'          => 'photo',
					    'label'         => __('Background Photo', 'fl-builder'),
					    'show_remove'   => false,
					),
					'hero_content' => array(
					    'type'          => 'editor',
					    'media_buttons' => true,
					    'wpautop'       => true
					),
                )
            )
        )
    )
) );