<div id="hero" class="module" style="background-image: url(<?php echo wp_get_attachment_url($settings->hero_bg); ?>);">
	<div class="container">
		<?php echo $settings->hero_content; ?>
	</div>
</div>