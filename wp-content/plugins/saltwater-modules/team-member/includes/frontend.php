<a href="#" class="person">
    <?php if($settings->active_thumb){?><div class="img" style="background-image: url(<?php echo wp_get_attachment_url($settings->active_thumb); ?>)"></div><?php } ?>
    <?php if($settings->hover_thumb){?><div class="imghover" style="background-image: url(<?php echo wp_get_attachment_url($settings->hover_thumb); ?>)"></div><?php } ?>
    <?php if($settings->persontitle){?><p class="name"><?php echo strtoupper($settings->personname); ?></p><?php } ?>
    <?php if($settings->persontitle){?><p class="title"><?php echo $settings->persontitle; ?></p><?php } ?>
</a>