<?php

class TeamMemberClass extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Team Member', 'fl-builder' ),
            'description'     => __( 'Build a team member box', 'fl-builder' ),
            'group'           => __( 'Content', 'fl-builder' ),
            'category'        => __( 'Content Blocks', 'fl-builder' ),
            'dir'             => SALTWATER_DIR . 'team-member/',
            'url'             => SALTWATER_URL . 'team-member/',
            'icon'            => 'button.png',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
}

FLBuilder::register_module( 'TeamMemberClass', array(
    'my-tab-1'      => array(
        'title'         => __( 'Team Member Content', 'fl-builder' ),
        'sections'      => array(
            'my-section-1'  => array(
                'title'         => __( 'Section 1', 'fl-builder' ),
                'fields'        => array(
                    'personname' => array(
					    'type'          => 'text',
					    'label'         => __( 'Enter name', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '255',
					    'size'          => '40',
					    'placeholder'   => __( 'Name', 'fl-builder' ),
					    'class'         => 'name',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'persontitle' => array(
					    'type'          => 'text',
					    'label'         => __( 'Enter job title', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '255',
					    'size'          => '40',
					    'placeholder'   => __( 'Title', 'fl-builder' ),
					    'class'         => 'title',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'active_thumb' => array(
					    'type'          => 'photo',
					    'label'         => __('Enter headshot', 'fl-builder'),
					    'show_remove'   => false,
					),
					'hover_thumb' => array(
					    'type'          => 'photo',
					    'label'         => __('Enter roll-over headshot', 'fl-builder'),
					    'show_remove'   => false,
					),
                )
            )
        )
    )
) );