<?php

class ColorBoxClass extends FLBuilderModule {

    public function __construct()
    {
        parent::__construct(array(
            'name'            => __( 'Colored Text Box', 'fl-builder' ),
            'description'     => __( 'Build a colored text box', 'fl-builder' ),
            'group'           => __( 'Content', 'fl-builder' ),
            'category'        => __( 'Content Blocks', 'fl-builder' ),
            'dir'             => SALTWATER_DIR . 'color-box/',
            'url'             => SALTWATER_URL . 'color-box/',
            'icon'            => 'button.png',
            'editor_export'   => true, // Defaults to true and can be omitted.
            'enabled'         => true, // Defaults to true and can be omitted.
            'partial_refresh' => false, // Defaults to false and can be omitted.
        ));
    }
}

FLBuilder::register_module( 'ColorBoxClass', array(
    'slide-1'      => array(
        'title'         => __( 'General', 'fl-builder' ),
        'sections'      => array(
            'my-section-1'  => array(
                'title'         => __( 'Contents', 'fl-builder' ),
                'fields'        => array(
	                'content' => array(
					    'type'          => 'editor',
					    'label'         => __( 'Content', 'fl-builder' ),
					    'media_buttons' => true,
					    'wpautop'       => true
					),
	                'bgcolor' => array(
					  'type'          => 'color',
					  'label'         => __( 'Background Color', 'fl-builder' ),
					  'default'       => 'eeeff0',
					  'show_reset'    => true,
					  'show_alpha'    => false
					),
					'byline' => array(
					    'type'          => 'text',
					    'label'         => __( 'Byline', 'fl-builder' ),
					    'default'       => '',
					    'maxlength'     => '',
					    'size'          => '',
					    'placeholder'   => __( '', 'fl-builder' ),
					    'class'         => '',
					    'description'   => __( '', 'fl-builder' ),
					    'help'          => __( '', 'fl-builder' )
					),
					'color' => array(
					    'type'          => 'select',
					    'label'         => __( 'Select Content Color', 'fl-builder' ),
					    'default'       => '1',
					    'options'       => array(
					        '1'      => __( 'Light (White)', 'fl-builder' ),
					        '2'      => __( 'Dark (Dark Gray)', 'fl-builder' ),
					    )
					),
                )
            )
        )
    ),
) );