.fl-node-<?php echo $id; ?> .color-box  {
	background-color: #<?php echo $settings->bgcolor; ?>;
	margin: 1em auto;
	max-width: 1100px;
}
.fl-node-<?php echo $id; ?> .color-box .content{
	padding: 0.5em 2em;
}
.fl-node-<?php echo $id; ?> .color-box h1, .fl-node-<?php echo $id; ?> .color-box h2, .fl-node-<?php echo $id; ?> .color-box h3, .fl-node-<?php echo $id; ?> .color-box h4, .fl-node-<?php echo $id; ?> .color-box h5, .fl-node-<?php echo $id; ?> .color-box h6, .fl-node-<?php echo $id; ?> .color-box p, .fl-node-<?php echo $id; ?> .color-box ul, .fl-node-<?php echo $id; ?> .color-box ol, .fl-node-<?php echo $id; ?> .color-box li, .fl-node-<?php echo $id; ?> .color-box a, .fl-node-<?php echo $id; ?> .color-box span{
	font-size: 1.25em;
	line-height: 1.5em;
	font-weight: 400;
	<?php if ($settings->color == 2){ ?>
	color: #4b4848
	<?php } else { ?>
	color: white;
	<?php } ?>
}

.fl-node-<?php echo $id; ?> .color-box p.byline{
    font-family: 'Barlow Condensed', sans-serif;
    text-transform: uppercase;
    letter-spacing: 1px;
    font-weight: 700;
    text-align: right;
    font-size: 1.1em;
}